# Hardware
Hardware interface is asynchronous UART @115200 (8bit, 1start, 1stop, no parity), TTL levels (5V logic), full duplex mode.

## Communication protocol
Protocol is asci nonblocking master-slave protocol without crc/checksum.
Communication between main system (master) and controller (slave) is initiated by request from master side, slave side is passive.

Each request is immediately followed with response from slave - time-out intervals on master side can be very short (~100ms).

Some actions are asynchronous and master must periodically read motion status to check action finished.
Request and Response

There are three types of requests:

1. commands - motion control, reset etc.
2. set value - adjusting params (speed, accel position)
3. get value - reading status, params, inputs, temperature etc.

Request contain module prefix (0-3chars), request type char ('!' or '?'), command/value identifier (0-4 chars) and data (0-23 chars) and is terminated with LF ('\n' char).
Request cannot be longer than 32 chars (with LF).

When success, response contain valid data (if requested) and is terminated with "ok\n". In case of error, response is any of following:

        "e1\n"   error 1 - unspecified failure
        "e2\n"   error 2 - busy
        "e3\n"   error 3 - syntax error
        "e4\n"   error 4 - parameter out of range
        "e5\n"   error 5 - operation not permitted

## Module prefix
Module prefix is 0-3char string containing list of target modules. Each module has single char identifier. Multiple modules can be accessed in one request.

SLA firmware does not support modules (only module main).

        ""    - (empty) main module
        "X"   - x-axis
        "Y"   - y-axis
        "Z"   - z-axis
        "XYZ" - x,y,z axes
        "*"   - x,y,z axes (same as "xyz")
        "T"   - module t, temperature
        "I"   - module i, inputs
        "O"   - module o, outputs
 
## Generic Commands
### main / Get Version
    req: "?ver"
    res: "SLA-control 0.9.0-70 ok" or "er"
    latest version is ‘0.9.8-313’

### main / Get Serial Number
    req: "?ser"
    res: "xxxxxxxxxxxx ok" or "er"
    this command returns 32char string (16 bytes from signature row)

### main / Reset
    req: "!rst"
    res: "RESET"

### main / Enable/disable steppers
    req: "!ena %d"
    res: "ok"
     %d - bitmask

### main / Get stepper enable/disable status
    req: "?ena"
    res: "%d ok"
     %d - bitmask

### main / Enable/disable endstops
    req: "!end %d"
    res: "ok"
     %d - bitmask

### main / Get endstops enabled/disabled
    req: "?end"
    res: "%d ok"
     %d - bitmask
     
### main / Enable/disable leds
    req: "!led %d"
    res: "ok"
     %d - bitmask
 
### main / Get led enable/disable status
    req: "?led"
    res: "%d ok"
     %d - bitmask

### main / Get status
    req: "?"
    res: "%d ok"
     %d - bitmask
       bit0 - tower is moving
       bit1 - tilt is moving
       bit6 - start button
       bit7 - cover switch
       bit12 - 
       bit13 - any mcusr reset flag is set (use "?rst" to read and reset mcusr)
       bit14 - fan error (no feedback from any fan)
       bit15 - fatal error (controller is in error mode)

### main / Get Info (**NOT IMPLEMENTED**)
    req: "?inf"
    res: "2018/11/14 20:30 4.9.2 0.2 ok" or "er"
    this command returns build informations (date, time, gcc version, board revision)

### sla / motorsMovementMask - "!mot"
    req: "!mot %d"
    res: "ok" or "er"
    tower: bit 0
    tilt: bit 1
    If the bit is cleared, axis is stopped. If the bit is set, axis remains in its previous state
     %d - axis mask (range 0..3)

## SLA Commands
### sla / beep - "!beep"
    req: "!beep %d %d"
    res: "ok" or "er"
     %d - frequency [Hz] (range 16..2000)
     %d - length [ms] (range 0..10000)

### sla / powerLed - "!pled"
    req: "!pled %d"
    res: "ok" or "er"
     %d - power led state (1/0)

### sla / read powerLed - "?pled"
    req: "?pled"
    res: "%d ok" or "er"
     %d - power led state (1/0)

### sla / shutdown - "!shdn"
    req: "!shdn %hhd"
    res: "ok" or "er"
     %hhd - delay to shutdown (range 0..255 [s])

### sla / uvLed - "!uled"
    req: "!uled %d [%d]"
    res: "ok" or "er"
     %d - UV led state (1/0)
     %d - Exposure time [ms] (1..65535)

### sla / read uvLed - "?uled"
    req: "?uled"
    res: "%d [%d] ok" or "er"
     %d - UV led state (1/0)
     %d - Remaining exposure time [ms] (1..65535)

### sla / cameraLed - "!cled"
    req: "!cled %d"
    res: "ok" or "er"
     %d - camera led state (1/0)

### sla / read cameraLed - "?cled"
    req: "?cled"
    res: "%d ok" or "er"
     %d - camera led state (1/0)

### sla / getFanState - "?fans"
    req: "?fans"
    res: "%d ok" or "er"
     %d - fan state (bit 0..3 - enabled)

### sla / setFanState - "!fans"
    req: "!fans %d"
    res: "ok" or "er"
     %d - fan state (bit 0..3 - enabled)

### sla / getTemperatures - "?temp"

For printers SL1

    req: "?temp"
    res: "%d %d %d %d ok" or "er"
     %d - TEMP-LED [0.1 °C]
     %d - TEMP-1 [0.1 °C]
     %d - TEMP-2 [0.1 °C]
     %d - TEMP-3 [0.1 °C]

For printers SL1S

    req: "?temp"
    res: "%d %d %d %d ok" or "er"
     %d - TEMP-2 [0.1 °C]
     %d - TEMP-1 [0.1 °C]
     %d - TEMP-LED [0.1 °C]
     %d - TEMP-3 [0.1 °C]

for board revision 04 and newer this command returns four numbers, for older revisions two numbers (system and uv-led temperature)

### sla / motorsRelease - "!motr"
    req: "!motr"
    res: "ok" or "er"
    disable motor power, homing status and current positions are lost.
    sla / towerUpMicroSteps - "!twup"
    req: "!twup %d"
    res: "ok" or "er"
     %d - microsteps (range 0..65535)
    action is asynchronous, “?mot” must be executed to get motion status

### sla / towerDownMicroSteps - "!twdn"
    req: "!twdn %d"
    res: "ok" or "er"
     %d - microsteps (range 0..65535)
    action is asynchronous, “?mot” must be executed to get motion status

### sla / getTowerCurr - "?twcu"
    req: "?twcu"
    res: "%d ok" or "er"
     %d - current (range 0..63 ~ 0..1A)

### sla / setTowerCurr - "!twcu"
    req: "!twcu %d"
    res: "ok" or "er"
     %d - current (range 0..63 ~ 0..1A)

### sla / getTiltCurr - "?ticu"
    req: "?ticu"
    res: "%d ok" or "er"
     %d - current (range 0..63 ~ 0..1A)

### sla / setTiltCurr - "!ticu"
    req: "!ticu %d"
    res: "ok" or "er"
     %d - current (range 0..63 ~ 0..1A)

### sla / getTowerSgth - "?twsg" **DEPRECATED**
    req: "?twsg"
    res: "%d ok" or "er"
     %d - stallguard threshold (range -128..127)

### sla / setTowerSgth - "!twsg" **DEPRECATED**
    req: "!twsg %d"
    res: "ok" or "er"
     %d - stallguard threshold (range -128..127)
 
### sla / getTiltSgth - "?tisg" **DEPRECATED**
    req: "?tisg"
    res: "%d ok" or "er"
     %d - stallguard threshold (range -128..127)
 
### sla / setTiltSgth - "!tisg" **DEPRECATED**
    req: "!tisg %d"
    res: "ok" or "er"
     %d - stallguard threshold (range -128..127)
 
### sla / getTowerHomeState - "?twho"
    req: "?twho"
    res: "ok" or "er"
     %d - current tower homing state:
      -3 blocked axis
      -2 endstop not reached
      -1 not initialized, unknown
       0 initialized, idle
       1 started
       2 going to minimum
       3 going back
       4 going to minimum 2 (slower)

### sla / towerHome - "!twho"
    req: "!twho"
    res: "ok" or "er"
    action is asynchronous, “?twho” must be executed to determine actual state

### sla / getTiltHomeState - "?tiho"
    req: "?tiho"
    res: "%d ok" or "er"
     %d - current tilt homing state:
      -3 blocked axis
      -2 endstop not reached
      -1 not initialized, unknown
       0 initialized, idle
       1 started
       2 going to minimum
       3 going back
       4 going to minimum 2 (slower)

### sla / tiltHome - "!tiho"
    req: "!tiho"
    res: "ok" or "er"
    action is asynchronous, “?tiho” must be executed to determine actual state

### sla / towerMoveAbsolute - "!twma"
    req: "!twma %d"
    res: "ok" or "er"
     %d - absolute coordinate (32-bit integer)
    action is asynchronous, “?mot” must be executed to determine actual state

### sla / tiltMoveAbsolute - "!tima"
    req: "!tima %d"
    res: "ok" or "er"
     %d - absolute coordinate (16-bit integer)
    action is asynchronous, “?mot” must be executed to determine actual state


### sla / getTowerPos - "?twpo"
    req: "?twpo"
    res: "%ld ok" or "er"
     %ld - tower position (32bit int)

### sla / setTowerPos - "!twpo"
    req: "!twpo %d"
    res: "ok" or "er"
     %d - tower position (32bit int)

### sla / getTiltPos - "?tipo"
    req: "?tipo"
    res: "%ld ok" or "er"
     %ld - tilt position (32bit int)

### sla / setTiltPos - "!tipo"
    req: "!tipo %d"
    res: "ok" or "er"
     %d - tilt position (32bit int)

### sla / getFansRPM - "?frpm"
    req: "?frpm"
    res: "%d %d %d ok" or "er"
     %d - fan0 rpm
     %d - fan1 rpm
     %d - fan2 rpm

gets actual RPM of fans. If the fan error occures frpm stores last RPM value and stops refreshing.

### sla / setFansRPM - "!frpm"
    req: "!frpm %d %d %d"
    res: "ok" or "er"
     %d - fan0 rpm
     %d - fan1 rpm
     %d - fan2 rpm

sets target RPM for every fan.

### sla / getTowerConfig - "?twcf"
    req: "?twcf"
    res: "%d %d %d %d %d %d %d ok" or "er"
     %d - sr0 (starting steprate 0..22000 [steps/s])
     %d - srm (maximum steprate 0..22000 [steps/s])
     %d - acc (acceleration 0..800 [256xsteps/s^2])
     %d - dec (deceleration 0..800 [256xsteps/s^2])
     %d - cur (current 0..63 [aprox. 1/64A])
     %d - sgt (stallguard threshold -128..127)
     %d - cst (coolstep threshold 0..10000 [T])

### sla / getTiltConfig - "?ticf"
    req: "?ticf"
    res: "%d %d %d %d %d %d %d ok" or "er"
     %d - sr0 (starting steprate 0..22000 [steps/s])
     %d - srm (maximum steprate 0..22000 [steps/s])
     %d - acc (acceleration 0..800 [256xsteps/s^2])
     %d - dec (deceleration 0..800 [256xsteps/s^2])
     %d - cur (current 0..63 [aprox. 1/64A])
     %d - sgt (stallguard threshold -128..127)
     %d - cst (coolstep threshold 0..10000 [T])

### sla / setTowerConfig - "!twcf"
    req: "!twcf %d %d %d %d %d %d %d"
    res: "ok" or "er"
    parameters are same as values returned from “?twcf”
    if selected tower config index >0, parameters are stored in eprom

### sla / setTiltConfig - "!ticf"
    req: "?twcf %d %d %d %d %d %d %d"
    res: "ok" or "er"
    parameters are same as values returned from “?ticf”
    if selected tilt config index >0, parameters are stored in eprom

### sla / getTowerConfigSelected - "?twcs"
    req: "?twcs"
    res: "%d ok" or "er"
     %d - selected tower config index (-1..7)
    value -1 means “not selected”

### sla / selectTowerConfig - "!twcs"
    req: "!twcs %d"
    res: "ok" or "er"
     %d - tower config index to select (-1..7)
    for index values 0..7 will be the axis configuration readed from eeprom and applied, value -1 keeps current axis settings and just unselect current profile (changes will be not stored in eeprom)

### sla / getTiltConfigSelected - "?tics"
    req: "?tics"
    res: "%d ok" or "er"

### sla / selectTiltConfig - "!tics"
    req: "!tics %d"
    res: "ok" or "er"

### sla / eepromClear - "!eecl"
    req: "!eecl"
    res: "ok" or "er"

### sla / eepromReadHex - "!eerx" (**NOT IMPLEMENTED**)
    req: "!eerx%d %d"
    res: "%02x %02x %02x … ok" or "er"
     %d - eeprom address (range 0..1023)
     %d - byte count (range 1..16)

### sla / eepromWriteHex - "!eewx" (**NOT IMPLEMENTED**)
    req: "!eewx %d %02x %02x ..."
    res: "%02x %02x %02x … ok" or "er"
     %d - eeprom address (range 512..1023 - first 512 bytes reserved for system)
     %02x - data (range 00..ff, max 16 bytes)




### sla / getEcho - "?echo"
    req: "?echo"
    res: "%d ok" or "er"
     %d - 0: echo disabled, 1: echo enabled
    for debugging purposes

### sla / setEcho - "!echo"
    req: "!echo %d"
    res: "ok" or "er"
     %d - 0: echo disabled, 1: echo enabled
    for debugging purposes

### sla / getResinSensorEnabled - "?rsen"
    req: "?rsen"
    res: "%d ok" or "er"
     %d - 0: sensor disabled, 1: sensor enabled

### sla / setResinSensorEnabled - "!rsen"
    req: "!rsen %d"
    res: "ok" or "er"
     %d - 0: sensor disabled, 1: sensor enabled
    sensor must be disabled/enabled at reference tower position to get calibrated

### sla / getResinSensorState - "?rsst"
    req: "?rsst"
    res: "%d ok" or "er"
     %d - 0: sensor not active, 1: sensor active
    sensor must be enabled and calibrated

### sla / resinSensorMeassure - "!rsme"
    req: "!rsme %d"
    res: "ok" or "er"
     %d - microsteps, distance for tower relative move down
    similar to !twdn, but also resin sensor is used as endstop



## Order of commands for resin measurement
    !ena 3                        // enable motors 
    !twcs 0                        // select fast profile
    !twho                        // tower home               
    !twma 17000                // move over the container        
    !rsen 0                        // sensor disabled
    delay(100);                // delay 100ms        
    !rsen 1                        // sensor enabled
    delay(100);                // delay 100ms 
    !twcs 1                        // select slower profile        
    !rsme 30000                // start measurement
    ?twpo                        // read actual tower position


### sla / gotoBootloader - "!boot"
    req: "!boot"
    res: "ok" or "er"
    switch to bootloader with 5s timeout (stay in bootloader just 5s)
     ### sla / getVoltages - "?volt"
    req: "?volt"
    res: "%d %d %d ok" or "er"
     %d - UVLED-1 [mV]
     %d - UVLED-2 [mV]
     %d - UVLED-3 [mV]
     %d - POWER [mV] (**NOT IMPLEMENTED**)
    for board revision 04 this commad return constant value 24000mV for power voltage.


### sla / getFanError - "?fane"
    req: "?fane"
    res: "%d ok" or "er"
     %d - fan error (bit 0..3 - fan0..3 error - no rotation)

### sla / getPowerLedPWM - "?ppwm"
    req: "?ppwm"
    res: "%d ok" or "er"
     %d - power led pwm (value 0..20 = 0..100%)

### sla / setPowerLedPWM - "!ppwm"
    req: "!ppwm %d"
    res: "ok" or "er"
     %d - power led pwm (value 0..20 = 0..100%)

### sla / getUVledPWM - "?upwm"
    req: "?upwm"
    res: "%d ok" or "er"
     %d - uvled pwm (value 0..250 = 0..100%)

### sla / setUVledPWM - "!upwm"
    req: "!upwm %d"
    res: "ok" or "er"
     %d - uvled pwm (value 0..250 = 0..100%)


### sla / getPowerLedSpeed - "?pspd"
    req: "?pspd"
    res: "%d ok" or "er"
     %d - power led blinking/pulsing speed factor (value 1..64 = 1/16..4)

### sla / setPowerLedSpeed- "!pspd"
    req: "!pspd %d"
    res: "ok" or "er"
     %d - power led blinking/pulsing speed factor (value 1..64 = 1/16..4)

### sla / sgBufferCount - "?sgbc"
    req: "?sgbc"
    res: "%d ok" or "er"
     %d - stallguard buffer sample count (value 0..255)
    returns number of samples in stall-guard buffer

### sla / sgBufferData - "?sgbd"
    req: "?sgbf"
    res: "%02x %02x %02x %02x %02x … %02x ok" or "er"
     %d - stallguard result values (00..ff - original sg result divided by 4)
    returns next 10 samples from stall-guard buffer in hexadecimal format


### sla / waitIdle - "!widl"
    req: "!widl"
    res: "ok" or "er"

Debugging command, ‘ok’ response is delayed until movements not finished.
Master should not use this command!

### sla / waitDelay - "!wdel"
    req: "!wdel %d"
    res: "ok" or "er"
     %d - timeout in milliseconds

Debugging command, ‘ok’ response is delayed by %d milliseconds.
Master should not use this command!

### sla / setPowerPanicStatus - "!ppst"
    req: "!ppst %d"
    res: "ok" or "er"
     %d - 1/0 - enabled/disabled


Activate delay measurement. Delay will be stored in eeprom.
Debugging command, for power-panic test.
Master should not use this command!
changed (build 302): Interrupt is by default disabled

### sla / powerPanicStatus - "?ppst"
    req: "?ppst"
    res: "%d ok" or "er"
     %d - last measured delay from power-panic to CPU dead [ms]

Read last measured delay from eeprom.
Debugging command, for power-panic test.
Master should not use this command!

### sla / getTowerHomeCal - "?twhc"
    req: "?twhc"
    res: "%d ok" or "er"
     %d - tower home phase (8bit int, -1..63, -1 means disabled)

### sla / setTowerHomeCal - "!twhc"
    req: "!twhc %d"
    res: "ok" or "er"
     %d - tower home phase (8bit int, -1..63, -1 means disabled)

### sla / towerHomeCalibrate - "!twhc"
    req: "!twhc"
    res: "ok" or "er"
    action is asynchronous, “?twho” must be executed to determine actual state

### sla / getTiltHomeCal - "?tihc"
    req: "?tihc"
    res: "%d ok" or "er"
     %d - tower home phase (8bit int, -1..63, -1 means disabled)

### sla / setTiltHomeCal - "!tihc"
    req: "!tihc %d"
    res: "ok" or "er"
     %d - tower home phase (8bit int, -1..63, -1 means disabled)

### sla / tiltHomeCalibrate - "!twhc"
    req: "!twhc"
    res: "ok" or "er"
    action is asynchronous, “?tiho” must be executed to determine actual state

### sla / towerStep - "!twst"
    req: "!twst %d"
    res: "ok" or "er"
     %d - number of steps to do (8bit int, -128..+127)
    action is synchronous, ok response can be delayed by %d milliseconds (steprate is 1kHz)

### sla / tiltStep - "!tist"
    req: "!tist %d"
    res: "ok" or "er"
     %d - number of steps to do (8bit int, -128..+127)
    action is synchronous, ok response can be delayed by %d milliseconds (steprate is 1kHz)

### sla / setTowerPhase - "!twph"
    req: "!twph %d"
    res: "ok" or "er"
     %d - new motor phase (16bit uint, 0..1023)
    action is synchronous, ok response can be delayed by 32 milliseconds (steprate is 1kHz)
    tower motor is moved shortest way to desired phase, position is updated

### sla / setTiltPhase - "!tiph"
    req: "!tiph %d"
    res: "ok" or "er"
     %d - new motor phase (16bit uint, 0..1023)
    action is synchronous, ok response can be delayed by 32 milliseconds (steprate is 1kHz)
    tilt motor is moved shortest way to desired phase, position is updated


### sla / getFanCheckMask - "?fmsk" (unused)
    req: "?fmsk"
    res: "%d ok" or "er"
     %d - fan check mask (bit 0..3 - fan check enabled)

### sla / setFanCheckMask - "!fmsk" (unused)
    req: "!fmsk %d"
    res: "ok" or "er"
     %d - fan check mask (bit 0..3 - fan check enabled)




### sla / goServiceMode - "!gose"
    req: "!gose %s"
    res: "ok" or "er"
     %s - serial number (32chars)
    this command enables service mode. Service mode allows execution of some special commands (direct pin control etc.), these commands are not permitted in live mode (controller responds e5).


### sla / _getPORT() - "?_por"
    req: "?_por %c"
    res: "%d ok" or "er"
     %c - single character identifying the port (B, C, D, E, F)
     %d - 8bit value readed from PORT register

### sla / _setPORT - "!_por"
    req: "!_por %c %d"
    res: "ok" or "er"
     %c - single character identifying the port (B, C, D, E, F)
     %d - 8bit value to write to PORT register

### sla / _getDDR() - "?_ddr"
    req: "?_ddr %c"
    res: "%d ok" or "er"
     %c - single character identifying the port (B, C, D, E, F)
     %d - 8bit value readed from DDR register

### sla / _setDDR - "!_ddr"
    req: "!_ddr %c %d"
    res: "ok" or "er"
     %c - single character identifying the port (B, C, D, E, F)
     %d - 8bit value to write to DDR register

### sla / _getPIN() - "?_pin"
    req: "?_pin %c"
    res: "%d ok" or "er"
     %c - single character identifying the port (B, C, D, E, F)
     %d - 8bit value readed from PIN register

### sla / _setPIN - "!_pin"
    req: "!_pin %c %d"
    res: "ok" or "er"
     %c - single character identifying the port (B, C, D, E, F)
     %d - 8bit value to write to PIN register

### main / getResetFlags
    req: "?rst"
    res: "%d ok"
     %d - bitmask
       bit0 - power-on reset flag (always 0, should be ignored)
       bit1 - external reset flag (will be set after flash process from master)
       bit2 - brown-out reset flag
       bit3 - watchdog reset flag (will be set also after “!rst” or “!boot”)
       bit4 - jtag reset flag (always 0, should be ignored)
       bit7 - stack overflow reset flag (**NOT IMPLEMENTED**)

### sla / getDiagnostics (**NOT IMPLEMENTED**)
    req: "?diag"
    res: "%d ok"
     %d - self diagnostic result code (0 means OK)

### sla / startDiagnostics (**NOT IMPLEMENTED**)
    req: "!diag"
    res: "ok"

### sla / read/writeTmcRegisters
    req: "?tmcc %u %u %u %u"
    res: "%lu"
     %u - axis number (tilt 0, tower 1)
     %u - read/write flag (read 0, write 1)
     %u - TMC register address
     %u - value (ignored when reads)
     %lu - return value

### sla / move tower to fullstep
    req: "!twgf
    res: "ok"

Calculates nearest higher fullstep position (phase 0. Same current in both coils A and B) and moves axis to this position

### sla / move tilt to fullstep
    req: "!tigf
    res: "ok"


## Legend:
    red text - command not implemented
    blue text - command implemented, empty functionality/simulated
    green text - command implementation complete
    black text - command verified
    orange text - command will be removed

## Controller modes
SLA controller has five modes of operation:
BOOT - controller is starting up (~100ms), requests from serial line are ignored
DIAG - internal diagnostics (~3s after boot), limited command set
IDLE - idle mode, no status requests from master within 5s (live-time)
LIVE - live mode, communication with master is live
ERRO - fatal error mode, any fatal error detected


## Error codes (internal diagnostics result)

1. boot_flash checksum error
2. appl_flash checksum error
3. serial number not set
4. invalid fuse bit settings
5. bootsection not locked
6. mcp23s17 spi error
7. tmc2130 spi error
8. tmc2130 signal/wiring error
