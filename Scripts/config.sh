#!/bin/sh
#
# config.sh - configuration script (Linux/MinGW)
#  Definition of tool executables, absolute paths etc.
#  This file is evaluated in most other scripts.
#

# avr-gcc tools binary folder:
#export AVRBIN=C:/Progra~1/WinAVR-20100110/bin
#export AVRBIN=C:/arduino-1.6.0/hardware/tools/avr/bin
#export AVRBIN=C:/arduino-1.6.8/hardware/tools/avr/bin
#export AVRBIN=C:/arduino-1.8.5/hardware/tools/avr/bin
export AVRBIN=C:/arduino-1.8.6/hardware/tools/avr/bin
#export AVRBIN=/data/data/com.termux/files/home/local/bin

# avrdude path
export AVRDUDE=C:/Progra~1/WinAVR-20100110/bin/avrdude
#export AVRDUDE=$AVRBIN/avrdude

rm -f config.out
exec &> >(tee -a "config.out")
echo "config.sh started"

export CONFIG=0
export AVRGCC=$AVRBIN/avr-gcc
export AVRGPP=$AVRBIN/avr-g++
export OBJCOPY=$AVRBIN/avr-objcopy
export OBJDUMP=$AVRBIN/avr-objdump
export SCRDIR=$(cd ${0%[\\/]*}; pwd)
export PRJDIR=$(cd $SCRDIR/..; pwd)
export OUTDIR=$PRJDIR/output
export OUTELF=$OUTDIR/SLA-control-01.ino.elf
export OUTHEX=$OUTDIR/SLA-control-01.ino.hex

check_exists()
{
	echo -n "$2 "
	if [ -e $1 ]; then echo 'OK'; return 0; else echo 'not found!'; return 1; fi
}

if check_exists "$AVRBIN" " avr bin folder"; then
 check_exists "$AVRGCC" " avr-gcc executable"
 check_exists "$AVRGPP" " avr-g++ executable"
 check_exists "$OBJCOPY" " avr-objcopy executable"
 check_exists "$OBJDUMP" " avr-objdump executable"
 check_exists "$AVRDUDE" " avrdude executable"
else
 export 
fi

if [ $CONFIG -eq 0 ]; then
 echo "config.sh finished with success"
elif [ $CONFIG -gt 0 ]; then
 echo "config.sh finished with errors!"
else
 echo "config.sh finished with fatal errors!"
fi
exec >&-
