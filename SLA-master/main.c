// SLA-master
//#include <inttypes.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h> 
#include <sys/types.h>
#include <sys/fcntl.h>
//#include <netinet/in.h> 
#include <arpa/inet.h>
#include <sys/socket.h>
//#define _POSIX_C_SOURCE 199309L
#include <sys/time.h>
//#include <sys/systeminfo.h>

#include <time.h>
#include "../Common/sla_comm.h"
#include "sla_master.h"
#include "sla_master_cmd.h"



int tower_home_test(void);

int tilt_home_test(void);

int tower_test(void);

int tilt_test(void);


void print_vars(void);

int ssh_show_image(char* host, char* filename);


int main(int argc, char** argv)
{
	int ret = 0;
	int i;
	char target[255] = "";
	char choice[255] = "";
	FILE* file = 0;

	if (argc > 1)
	{
		strcpy(target, argv[1]);
	}
	else
	{
		printf("enter target (ip address / tty device):\n");
		gets(target);
	}
	if (strlen(target) == 0)
	{
		printf("target is empty!\n");
		return 1;
	}
	if (sscanf(target, "%hhu.%hhu.%hhu.%hhu%n", &i, &i, &i, &i, &i) == 4)
	{
		printf("target is ip addres\n");
		target[i] = 0;
		strcat(target, ":8192");
		printf(" connecting to %s\n", target);
		file = fopen_tcp_client(target);
	}
	else
	{
		printf("target is tty device\n");
		printf(" connecting to %s\n", target);
		file = fopen_tty(target);
	}
	//FILE* file = fopen_tcp_client("127.0.0.1:8888");
	//FILE* file = fopen_tty("COM3");
	if (file == 0)
	{
		perror("connect failed");
		return 1;
	}
	printf("connected\n", target);


	cmd_in = file;
	cmd_out = file;
	//cmd_log = stdout;

	ret = sla_init();
	if (ret < 0)
	{
		printf("ERROR: %d\n", ret);
		fclose(file);
		return 1;
	}
	print_vars();

	printf("\n");
	printf("1. calibrate tower\n");
	printf("2. calibrate tilt\n");
	printf("3. tower homing test\n");
	printf("4. tilt homing test\n");
	printf("enter choice: ");
	gets(choice);
	if ((sscanf(choice, "%d", &i) != 1) || (i < 1) || (i > 4))
	{
		printf("invalid choice!");
		fclose(file);
		return 1;
	}

	switch (i)
	{
	case 1:
		sla_autocal_start_tower();
		sla_autocal_profile_tower(0);
		sla_autocal_profile_tower(1);
		break;
	case 2:
		sla_autocal_start_tilt();
		sla_autocal_profile_tilt(0);
		sla_autocal_profile_tilt(1);
		break;
	case 3:
		tower_home_test();
		break;
	case 4:
		tilt_home_test();
		break;
	}


	sla_cmd_E_motr();

	printf("closing connection\n");
	fclose(file);

	return 0;
}








//#define _log3(...) printf(__VA_ARGS__)
#define _log2(...) printf(__VA_ARGS__)
#define _log3(...)
//#define _log2(...)

int tilt_autocal_profile_0(void)
{
	int ret = 0;
	unsigned char sgup = 0;
	unsigned char sgdn = 0;
	unsigned char sg_avg = 0;
	unsigned char sg_dif = 0;
	unsigned char cur_tab[] = {19, 20, 21, 22};
	unsigned char sgt_tab[] = {9, 8, 7, 6};
	unsigned char cur;
	unsigned char sgt;

	unsigned char desired_sg_avg = 75;
	unsigned char max_sg_dif = 10;

	unsigned char best_cur;
	unsigned char best_sgt;
	unsigned char best_sgup;
	unsigned char best_sgdn;
	unsigned char best_sg_avg;
	unsigned char best_sg_dif = 255;

	unsigned char not_best = 0;

	int i;
	int j;
	_log2("\ntilt_autocal_profile_0\n");

	for (i = 0; i < sizeof(sgt_tab); i++)
	{
		sgt = sgt_tab[i];
		for (j = 0; j < sizeof(cur_tab); j++)
		{
			cur = cur_tab[j];
			//select profile 0
			ret = sla_select_tilt_profile(0);
			_log3("sla_select_tilt_profile(0) ret=%d\n", ret);

			ret = sla_autocal_test(1, 5000, 0, cur, sgt, &sgup, &sgdn);
			if (ret == 2) break;
			if (ret) return 0;
			sg_avg = (sgup + sgdn) / 2;
			sg_dif = abs(desired_sg_avg - sg_avg);
			if (sg_dif > (desired_sg_avg / 2)) break;
			if (sg_dif < best_sg_dif)
			{
				not_best = 0;
				best_cur = cur;
				best_sgt = sgt;
				best_sgup = sgup;
				best_sgdn = sgdn;
				best_sg_avg = sg_avg;
				best_sg_dif = sg_dif;
				//printf("#best setting changed: cur=%d sgt=%d\n", best_cur, best_sgt);
			}
			else
				not_best++;
			if (not_best >= 2) break;
		}
		if (ret == 2) break;
		if (not_best >= 2) break;
	}
	printf("\nbest setting: cur=%d sgt=%d\n", best_cur, best_sgt);
	if (best_sg_dif <= max_sg_dif)
	{
		sla_ticf[0].cur = best_cur;
		sla_ticf[0].sgt = best_sgt;
		ret = sla_set_tilt_profile(0, 0);
		_log3("sla_set_tilt_profile(0, 0) ret=%d\n", ret);
		if (ret == 0)
			printf("profile 0 adjusted: cur=%d sgt=%d\n", best_cur, best_sgt);
	}
	return 0;
}

int tilt_autocal_profile_1(void)
{
	int ret = 0;
	unsigned char sgup = 0;
	unsigned char sgdn = 0;
	unsigned char sg_avg = 0;
	unsigned char sg_dif = 0;
	unsigned char cur_tab[] = {16, 17, 18, 19};
	unsigned char sgt_tab[] = {8, 7, 6, 5};
	unsigned char cur;
	unsigned char sgt;

	unsigned char desired_sg_avg = 50;
	unsigned char max_sg_dif = 10;

	unsigned char best_cur;
	unsigned char best_sgt;
	unsigned char best_sgup;
	unsigned char best_sgdn;
	unsigned char best_sg_avg;
	unsigned char best_sg_dif = 255;

	unsigned char not_best = 0;

	int i;
	int j;
	_log2("\ntilt_autocal_profile_1\n");

	for (i = 0; i < sizeof(sgt_tab); i++)
	{
		sgt = sgt_tab[i];
		for (j = 0; j < sizeof(cur_tab); j++)
		{
			cur = cur_tab[j];
			//select profile 0
			ret = sla_select_tilt_profile(1);
			_log3("sla_select_tilt_profile(1) ret=%d\n", ret);

			ret = sla_autocal_test(1, 2500, 0, cur, sgt, &sgup, &sgdn);
			if (ret == 2) break;
			if (ret) return 0;
			sg_avg = (sgup + sgdn) / 2;
			sg_dif = abs(desired_sg_avg - sg_avg);
			if (sg_dif > (desired_sg_avg / 2)) break;
			if (sg_dif < best_sg_dif)
			{
				not_best = 0;
				best_cur = cur;
				best_sgt = sgt;
				best_sgup = sgup;
				best_sgdn = sgdn;
				best_sg_avg = sg_avg;
				best_sg_dif = sg_dif;
				//printf("#best setting changed: cur=%d sgt=%d\n", best_cur, best_sgt);
			}
			else
				not_best++;
			if (not_best >= 2) break;
		}
		if (ret == 2) break;
		if (not_best >= 2) break;
	}
	printf("\nbest setting: cur=%d sgt=%d\n", best_cur, best_sgt);
	if (best_sg_dif <= max_sg_dif)
	{
		sla_ticf[1].cur = best_cur;
		sla_ticf[1].sgt = best_sgt;
		ret = sla_set_tilt_profile(1, 0);
		_log3("sla_set_tilt_profile(1, 0) ret=%d\n", ret);
		if (ret == 0)
			printf("profile 1 adjusted: cur=%d sgt=%d\n", best_cur, best_sgt);
	}
	return 0;
}



int tower_autocal_profile_0(void)
{
	int ret = 0;
	unsigned char sgup = 0;
	unsigned char sgdn = 0;
	unsigned char sg_avg = 0;
	unsigned char sg_dif = 0;
	unsigned char cur_tab[] = {22};
	unsigned char sgt_tab[] = {10, 9, 8, 7, 6, 5, 4};
	unsigned char cur;
	unsigned char sgt;

	unsigned char desired_sg_avg = 90;
	unsigned char max_sg_dif = 10;

	unsigned char best_cur;
	unsigned char best_sgt;
	unsigned char best_sgup;
	unsigned char best_sgdn;
	unsigned char best_sg_avg;
	unsigned char best_sg_dif = 255;

	unsigned char not_best = 0;

	int i;
	int j;
	_log2("\ntower_autocal_profile_0\n");

	for (i = 0; i < sizeof(sgt_tab); i++)
	{
		sgt = sgt_tab[i];
		for (j = 0; j < sizeof(cur_tab); j++)
		{
			cur = cur_tab[j];
			//select profile 0
			ret = sla_select_tower_profile(0);
			_log3("sla_select_tower_profile(0) ret=%d\n", ret);

			ret = sla_autocal_test(0, -20000, 0, cur, sgt, &sgup, &sgdn);
			if (ret == 2)
			{
				printf("#position not reached!\n", ret);
				break;
			}
			if (ret) return 0;
			sg_avg = (sgup + sgdn) / 2;
			sg_dif = abs(desired_sg_avg - sg_avg);
			if (sg_dif > (desired_sg_avg / 2)) 
			{
				printf("#sg_dif to big!\n", ret);
				break;
			}
			if (sg_dif < best_sg_dif)
			{
				not_best = 0;
				best_cur = cur;
				best_sgt = sgt;
				best_sgup = sgup;
				best_sgdn = sgdn;
				best_sg_avg = sg_avg;
				best_sg_dif = sg_dif;
				printf("#best setting changed: cur=%d sgt=%d\n", best_cur, best_sgt);
			}
			else
				not_best++;
			if (not_best >= 2) break;
		}
		if (ret == 2) break;
		if (not_best >= 2) break;
	}
	printf("\nbest setting: cur=%d sgt=%d\n", best_cur, best_sgt);
	if (best_sg_dif <= max_sg_dif)
	{
		sla_twcf[0].cur = best_cur;
		sla_twcf[0].sgt = best_sgt;
		ret = sla_set_tower_profile(0, 0);
		_log3("sla_set_tower_profile(0, 0) ret=%d\n", ret);
		if (ret == 0)
			printf("profile 0 adjusted: cur=%d sgt=%d\n", best_cur, best_sgt);
	}
	return 0;
}

int tower_autocal_profile_1(void)
{
	int ret = 0;
	unsigned char sgup = 0;
	unsigned char sgdn = 0;
	unsigned char sg_avg = 0;
	unsigned char sg_dif = 0;
	unsigned char cur_tab[] = {16, 17};
	unsigned char sgt_tab[] = {3, 2, 1, 0};
	unsigned char cur;
	unsigned char sgt;

	unsigned char desired_sg_avg = 55;
	unsigned char max_sg_dif = 10;

	unsigned char best_cur;
	unsigned char best_sgt;
	unsigned char best_sgup;
	unsigned char best_sgdn;
	unsigned char best_sg_avg;
	unsigned char best_sg_dif = 255;

	unsigned char not_best = 0;

	int i;
	int j;
	_log2("\ntower_autocal_profile_1\n");

	for (i = 0; i < sizeof(sgt_tab); i++)
	{
		sgt = sgt_tab[i];
		for (j = 0; j < sizeof(cur_tab); j++)
		{
			cur = cur_tab[j];
			//select profile 0
			ret = sla_select_tower_profile(1);
			_log3("sla_select_twlt_profile(1) ret=%d\n", ret);

			ret = sla_autocal_test(0, -5000, 0, cur, sgt, &sgup, &sgdn);
			if (ret == 2)
			{
				printf("#position not reached!\n", ret);
				break;
			}
			if (ret) return 0;
			sg_avg = (sgup + sgdn) / 2;
			sg_dif = abs(desired_sg_avg - sg_avg);
			if (sg_dif > (desired_sg_avg / 2)) 
			{
				printf("#sg_dif to big!\n", ret);
				break;
			}
			if (sg_dif < best_sg_dif)
			{
				not_best = 0;
				best_cur = cur;
				best_sgt = sgt;
				best_sgup = sgup;
				best_sgdn = sgdn;
				best_sg_avg = sg_avg;
				best_sg_dif = sg_dif;
				//printf("#best setting changed: cur=%d sgt=%d\n", best_cur, best_sgt);
			}
			else
				not_best++;
			if (not_best >= 2) break;
		}
		if (ret == 2) break;
		if (not_best >= 2) break;
	}
	printf("\nbest setting: cur=%d sgt=%d\n", best_cur, best_sgt);
	if (best_sg_dif <= max_sg_dif)
	{
		sla_twcf[1].cur = best_cur;
		sla_twcf[1].sgt = best_sgt;
		ret = sla_set_tower_profile(1, 0);
		_log3("sla_set_tower_profile(1, 0) ret=%d\n", ret);
		if (ret == 0)
			printf("profile 1 adjusted: cur=%d sgt=%d\n", best_cur, best_sgt);
	}
	return 0;
}

int tower_home_test(void)
{
	int ret = 0;
	int n = 0;
	int e = 0;
	_log2("\ntower_home_test\n");
	while (n < 10)
	{
		n++;
		ret = sla_home_tower();
		if (ret == 0)
		{
			printf(" %d. tower home OK\n", n);
		}
		else
		{
			e++;
			printf(" %d. tower home NG (%d)\n", n, e);
		}
	}
}

int tilt_home_test(void)
{
	int ret = 0;
	int n = 0;
	int e = 0;
	_log2("\ntilt_home_test\n");
	while (n < 10)
	{
		n++;
		ret = sla_home_tilt();
		if (ret == 0)
		{
			printf(" %d. tilt home OK\n", n);
		}
		else
		{
			e++;
			printf(" %d. tilt home NG (%d)\n", n, e);
		}
	}
}

int tower_test(void)
{
	int ret = 0;
	int i;
	ret = sla_home_tower();
	printf("sla_home_tower ret=%d\n", ret);
	if (ret < 0) return 1;

	ret = sla_set_tower_pos(0);
	printf("sla_set_tower_pos ret=%d\n", ret);
	if (ret < 0) return 1;

	while (1)
	{
		ret = sla_move_tower_abs(-2000);
		printf("sla_move_tower_abs ret=%d\n", ret);
		if (ret < 0) return 1;

		ret = sla_select_tower_profile(1);
		printf("sla_select_tower_profile ret=%d\n", ret);
		if (ret < 0) return 1;

		ret = sla_move_tower_abs(2000);
		printf("sla_move_tower_abs ret=%d\n", ret);

		printf(" twpo: %hd\n", sla_twpo);
		//if (ret < 0) return 1;
	}
}

int tilt_test(void)
{
	int ret = 0;
	int i;

	ret = sla_home_tilt();
	printf("sla_home_tilt ret=%d\n", ret);
	if (ret < 0) return 1;

	ret = sla_set_tilt_pos(0);
	printf("sla_set_tilt_pos ret=%d\n", ret);
	if (ret < 0) return 1;

	while (1)
	{
		ret = sla_move_tilt_abs(400);
		printf("sla_move_tilt_abs ret=%d\n", ret);
		if (ret < 0) return 1;

		ret = sla_select_tilt_profile(1);
		printf("sla_select_tilt_profile ret=%d\n", ret);
		if (ret < 0) return 1;

		ret = sla_move_tilt_abs(-400);
		printf("sla_move_tilt_abs ret=%d\n", ret);

		printf(" tipo: %hd\n", sla_tipo);
		//if (ret < 0) return 1;
	}
}




void print_vars(void)
{
	int i;

	printf(" version: %s\n", sla_version);
	printf(" serial: %s\n", sla_serial);
	printf(" twho: %hhd\n", sla_twho);
	printf(" twcs: %hhd\n", sla_twcs);
	printf(" twpo: %ld\n", sla_twpo);
	printf(" tiho: %hhd\n", sla_tiho);
	printf(" tics: %hhd\n", sla_tics);
	printf(" tipo: %hd\n", sla_tipo);
	printf(" uled: %hhd %hu\n", sla_uled, sla_uled_time);

	for (i = 0; i < 8; i++)
		printf("twcf%d: %hu %hu %hu %hu %hhu %hhd %hu\n",
			i,
			sla_twcf[i].sr0,
			sla_twcf[i].srm,
			sla_twcf[i].acc,
			sla_twcf[i].dec,
			sla_twcf[i].cur,
			sla_twcf[i].sgt,
			sla_twcf[i].cst
		);

	for (i = 0; i < 8; i++)
		printf("ticf%d: %hu %hu %hu %hu %hhu %hhd %hu\n",
			i,
			sla_ticf[i].sr0,
			sla_ticf[i].srm,
			sla_ticf[i].acc,
			sla_ticf[i].dec,
			sla_ticf[i].cur,
			sla_ticf[i].sgt,
			sla_ticf[i].cst
		);

}

int ssh_show_image(char* host, char* filename)
{
	int ret;
	char ssh[1024] = {0};
//	char host[256] = "root@10.24.230.140 ";
	char cmds[1024] = "\"cd /lib/python2.7/site-packages/sl1fw; python2 ./show_image.py ~/00.png\"";
	strcat(ssh, "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ");
	strcat(ssh, host);
	strcat(ssh, cmds);
	strcat(ssh, " &>/dev/null");
	ret = system(ssh);
	printf("system returned %d\n", ret);
	return 0;
}