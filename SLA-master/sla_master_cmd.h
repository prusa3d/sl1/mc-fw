// sla_master_cmd.h
#ifndef _SLA_MASTER_CMD_H
#define _SLA_MASTER_CMD_H
#include <stdio.h>

//maximum lengths
#define CMD_MAX_LINE 42 //command line
#define CMD_MAX_SER  32 //serial number string
#define CMD_MAX_VER  64 //version string

//result codes
#define CMD_OK         0 //ok - success
#define CMD_ER_UNK    -1 //error 1 - unknown/unspecified failure
#define CMD_ER_BSY    -2 //error 2 - busy
#define CMD_ER_SYN    -3 //error 3 - syntax error
#define CMD_ER_OOR    -4 //error 4 - parameter out of range
#define CMD_ER_ONP    -5 //error 5 - operation not permitted
#define CMD_ER_NUL    -6 //error 6 - null pointer
#define CMD_ER_CNF    -7 //error 7 - command not found
#define CMD_ER_PNR  -126 //error 126 - position not reached
#define CMD_ER_TO1  -127 //error 127 - operation timeout
#define CMD_ER_TMO  -128 //error 128 - command timeout

#define CMD_TMO_DEF   10000 //response timeout
#define CMD_TMO_RST   10000 //reset timeout


typedef struct
{
	unsigned short sr0;
	unsigned short srm;
	unsigned short acc;
	unsigned short dec;
	unsigned char cur;
	char sgt;
	unsigned short cst;
} sla_axis_cfg_t;

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

//FILE streams
extern FILE* cmd_in;
extern FILE* cmd_out;
extern FILE* cmd_log;


//"?" or "Q" (questionmark) commands

extern char sla_cmd_Q_(unsigned short* pstatus);

extern char sla_cmd_Q_ver(char* pver);

extern char sla_cmd_Q_ser(char* pser);

extern char sla_cmd_Q_tiho(char* ptiho);

extern char sla_cmd_Q_tics(char* ptics);

extern char sla_cmd_Q_ticf(char idx, sla_axis_cfg_t* pticf);

extern char sla_cmd_Q_tipo(short* ptipo);

extern char sla_cmd_Q_ticu(unsigned char* pticu);

extern char sla_cmd_Q_twho(char* ptwho);

extern char sla_cmd_Q_twcs(char* ptwcs);

extern char sla_cmd_Q_twcf(char idx, sla_axis_cfg_t* ptwcf);

extern char sla_cmd_Q_twpo(long* ptwpo);

extern char sla_cmd_Q_twcu(unsigned char* ptwcu);

extern char sla_cmd_Q_uled(char* puled, unsigned short* puled_time);

extern char sla_cmd_Q_sgbc(unsigned char* psgbc);

extern char sla_cmd_Q_sgbd(unsigned char* psgbd);



//"!" or "E" (exclamationmark) commands

extern char sla_cmd_E_rst(void);

extern char sla_cmd_E_shdn(unsigned short shdn);

extern char sla_cmd_E_motr(void);

extern char sla_cmd_E_tiho(void);

extern char sla_cmd_E_tipo(short tipo);

extern char sla_cmd_E_tima(short pos);

extern char sla_cmd_E_tics(char idx);

extern char sla_cmd_E_ticf(sla_axis_cfg_t* pcfg);

extern char sla_cmd_E_ticu(unsigned char ticu);

extern char sla_cmd_E_tisg(char tisg);

extern char sla_cmd_E_twho(void);

extern char sla_cmd_E_twpo(long twpo);

extern char sla_cmd_E_twma(long pos);

extern char sla_cmd_E_twcs(char idx);

extern char sla_cmd_E_twcf(sla_axis_cfg_t* pcfg);

extern char sla_cmd_E_twcu(unsigned char twcu);

extern char sla_cmd_E_twsg(char twsg);

extern char sla_cmd_E_uled(char uled, unsigned short uled_time);

extern char sla_cmd_E_sgbd(void);


#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_MASTER_CMD_H
