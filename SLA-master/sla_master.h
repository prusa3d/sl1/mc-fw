// sla_master.h
#ifndef _SLA_MASTER_H
#define _SLA_MASTER_H
#include "sla_master_cmd.h"

//timeouts
#define SLA_TIMEOUT_TWHO  20000 //tower homing timeout
#define SLA_TIMEOUT_TIHO  10000 //tilt homing timeout
#define SLA_TIMEOUT_TWMA  30000 //tower moving timeout
#define SLA_TIMEOUT_TIMA  15000 //tilt moving timeout

//wait flags
#define SLA_WAIT_TWMO 0x01 //wait for tower movement finished
#define SLA_WAIT_TIMO 0x02 //wait for tilt movement finished
#define SLA_WAIT_TWHO 0x04 //wait for tower homing finished
#define SLA_WAIT_TIHO 0x08 //wait for tilt homing finished
#define SLA_WAIT_ULED 0x80 //wait for uled exposure finished


#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)


extern char sla_version[CMD_MAX_VER];
extern char sla_serial[CMD_MAX_SER];
unsigned short sla_status;
extern char sla_twho;
extern char sla_twcs;
extern long sla_twpo;
extern sla_axis_cfg_t sla_twcf[8];
extern char sla_tiho;
extern char sla_tics;
extern short sla_tipo;
extern sla_axis_cfg_t sla_ticf[8];
extern int sla_uled;
extern unsigned short sla_uled_time;



extern int sla_wait(int wait_mask, int timeout);

extern int sla_init(void);

extern int sla_reset(void);

extern int sla_home_tower(void);

extern int sla_home_tilt(void);

extern int sla_move_tower_abs(long pos);

extern int sla_move_tilt_abs(short pos);

extern int sla_set_tower_pos(long pos);

extern int sla_set_tilt_pos(short pos);

extern int sla_get_tower_profile(int idx);

extern int sla_get_tilt_profile(int idx);

extern int sla_set_tower_profile(int idx, sla_axis_cfg_t* pcfg);

extern int sla_set_tilt_profile(int idx, sla_axis_cfg_t* pcfg);

extern int sla_select_tower_profile(int idx);

extern int sla_select_tilt_profile(int idx);

extern int sla_set_tower_cur(unsigned char cur);

extern int sla_set_tilt_cur(unsigned char cur);

extern int sla_set_uled(char uled, unsigned short uled_time);

extern int sla_clr_sgdata(void);

extern int sla_get_sgdata(unsigned char* pbuff, unsigned char* psize);

extern int sla_set_axis_cfg(sla_axis_cfg_t* pcfg, unsigned short sr0, unsigned short srm, unsigned short acc, unsigned short dec, unsigned char cur, char sgt, unsigned short cst);


extern int sla_set_axis_cur(int axis, unsigned char cur);
extern int sla_set_axis_sgt(int axis, char sgt);
extern int sla_set_axis_pos(int axis, long pos);
extern int sla_move_axis_abs(int axis, long pos);
extern int sla_select_axis_profile(int axis, int idx);


#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_MASTER_H
