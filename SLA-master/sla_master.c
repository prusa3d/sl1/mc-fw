// sla_master.c
#include "sla_master.h"


char sla_version[CMD_MAX_VER] = "";
char sla_serial[CMD_MAX_SER] = "";
unsigned short sla_status = 0;
char sla_twho = -1;
char sla_twcs = -1;
long sla_twpo = 0;
sla_axis_cfg_t sla_twcf[8];
char sla_tiho = -1;
char sla_tics = -1;
short sla_tipo = 0;
sla_axis_cfg_t sla_ticf[8];
int sla_uled = -1;
unsigned short sla_uled_time = 0;




int sla_wait(int wait_mask, int timeout)
{
	int ret;
	unsigned long ms = time_ms();
	unsigned long dt;
	int mask = wait_mask;
	while (timeout > 0)
	{
		if (wait_mask & (SLA_WAIT_TWMO | SLA_WAIT_TIMO))
		{
			if ((ret = sla_cmd_Q_(&sla_status)) < 0) return ret;
			if ((sla_status & 0x01) == 0) mask &= ~SLA_WAIT_TWMO;
			if ((sla_status & 0x02) == 0) mask &= ~SLA_WAIT_TIMO;
		}
		if (wait_mask & SLA_WAIT_TWHO)
		{
			if ((ret = sla_cmd_Q_twho(&sla_twho)) < 0) return ret;
			if (sla_twho <= 0) mask &= ~SLA_WAIT_TWHO;
		}
		if (wait_mask & SLA_WAIT_TIHO)
		{
			if ((ret = sla_cmd_Q_tiho(&sla_tiho)) < 0) return ret;
			if (sla_tiho <= 0) mask &= ~SLA_WAIT_TIHO;
		}
		if (wait_mask & SLA_WAIT_ULED)
		{
			if ((ret = sla_cmd_Q_uled(&sla_uled, &sla_uled_time)) < 0) return ret;
			if ((sla_uled == 0) || (sla_uled_time == 0)) mask &= ~SLA_WAIT_ULED;
		}
		if (mask == 0) return CMD_OK;
		usleep(100000);
		dt = (time_ms() - ms);
		ms += dt;
		timeout -= dt;
//		printf("timeout=%d\n", timeout);
	}
//	printf("timeout!!!\n");
	return CMD_ER_TO1;
}

int sla_init(void)
{
	int ret;
//	if ((ret = sla_reset()) < 0) return ret;
//	usleep(200000);
	if ((ret = sla_cmd_Q_ver(sla_version)) < 0) return ret;
	if ((ret = sla_cmd_Q_ser(sla_serial)) < 0) return ret;
	if ((ret = sla_cmd_Q_twho(&sla_twho)) < 0) return ret;
	if ((ret = sla_cmd_Q_twcs(&sla_twcs)) < 0) return ret;
	if ((ret = sla_cmd_Q_twpo(&sla_twpo)) < 0) return ret;
	if ((ret = sla_cmd_Q_tiho(&sla_tiho)) < 0) return ret;
	if ((ret = sla_cmd_Q_tics(&sla_tics)) < 0) return ret;
	if ((ret = sla_cmd_Q_tipo(&sla_tipo)) < 0) return ret;
	if ((ret = sla_get_tower_profile(-1)) < 0) return ret;
	if ((ret = sla_get_tilt_profile(-1)) < 0) return ret;
	if ((ret = sla_cmd_Q_uled(&sla_uled, &sla_uled_time)) < 0) return ret;
	if ((ret = sla_cmd_Q_(&sla_status)) < 0) return ret;
	return CMD_OK;
}

int sla_reset(void)
{
	int ret;
	if ((ret = sla_cmd_E_rst()) < 0) return ret;
	return CMD_OK;
}

int sla_home_tower(void)
{
	int ret;
	if ((ret = sla_cmd_E_twho()) < 0) return ret;
	return sla_wait(SLA_WAIT_TWHO, SLA_TIMEOUT_TWHO);
}

int sla_home_tilt(void)
{
	int ret;
	if ((ret = sla_cmd_E_tiho()) < 0) return ret;
	return sla_wait(SLA_WAIT_TIHO, SLA_TIMEOUT_TIHO);
}

int sla_move_tower_abs(long pos)
{
	int ret;
	if ((ret = sla_cmd_E_twma(pos)) < 0) return ret;
	if ((ret = sla_wait(SLA_WAIT_TWMO, SLA_TIMEOUT_TWMA)) < 0) return ret;
	if ((ret = sla_cmd_Q_twpo(&sla_twpo)) < 0) return ret;
	return (sla_twpo == pos)?CMD_OK:CMD_ER_PNR;
}

int sla_move_tilt_abs(short pos)
{
	int ret;
	if ((ret = sla_cmd_E_tima(pos)) < 0) return ret;
	if ((ret = sla_wait(SLA_WAIT_TIMO, SLA_TIMEOUT_TIMA)) < 0) return ret;
	if ((ret = sla_cmd_Q_tipo(&sla_tipo)) < 0) return ret;
	return (sla_tipo == pos)?CMD_OK:CMD_ER_PNR;
}

int sla_set_tower_pos(long pos)
{
	int ret;
	if ((ret = sla_cmd_E_twpo(pos)) < 0) return ret;
	sla_twpo = pos;
	return CMD_OK;
}

int sla_set_tilt_pos(short pos)
{
	int ret;
	if ((ret = sla_cmd_E_tipo(pos)) < 0) return ret;
	sla_tipo = pos;
	return CMD_OK;
}

int sla_get_tower_profile(int idx)
{
	int ret;
	if (idx < 0)
	{
		for (idx = 0; idx < 8; idx++)
			if ((ret = sla_cmd_Q_twcf(idx, sla_twcf + idx)) < 0) return ret;
	}
	else
		if ((ret = sla_cmd_Q_twcf(idx, sla_twcf + idx)) < 0) return ret;
	return CMD_OK;
}

int sla_get_tilt_profile(int idx)
{
	int ret;
	if (idx < 0)
	{
		for (idx = 0; idx < 8; idx++)
			if ((ret = sla_cmd_Q_ticf(idx, sla_ticf + idx)) < 0) return ret;
	}
	else
		if ((ret = sla_cmd_Q_ticf(idx, sla_ticf + idx)) < 0) return ret;
	return CMD_OK;
}

int sla_set_tower_profile(int idx, sla_axis_cfg_t* pcfg)
{
	int ret;
	if (idx < 0) idx = sla_twcs;
	if (idx < 0) return CMD_ER_OOR;
	if (pcfg) memcpy(sla_twcf + idx, pcfg, sizeof(sla_axis_cfg_t));
	if ((ret = sla_cmd_E_twcs(idx)) < 0) return ret;
	if ((ret = sla_cmd_E_twcf(sla_twcf + idx)) < 0) return ret;
	return CMD_OK;
}

int sla_set_tilt_profile(int idx, sla_axis_cfg_t* pcfg)
{
	int ret;
	if (idx < 0) idx = sla_tics;
	if (idx < 0) return CMD_ER_OOR;
	if (pcfg) memcpy(sla_ticf + idx, pcfg, sizeof(sla_axis_cfg_t));
	if ((ret = sla_cmd_E_tics(idx)) < 0) return ret;
	if ((ret = sla_cmd_E_ticf(sla_ticf + idx)) < 0) return ret;
	return CMD_OK;
}

int sla_select_tower_profile(int idx)
{
	int ret;
	if ((ret = sla_cmd_E_twcs(idx)) < 0) return ret;
	sla_twcs = idx;
	return CMD_OK;
}

int sla_select_tilt_profile(int idx)
{
	int ret;
	if ((ret = sla_cmd_E_tics(idx)) < 0) return ret;
	sla_tics = idx;
	return CMD_OK;
}

int sla_set_tower_cur(unsigned char cur)
{
	int ret;
	if ((ret = sla_cmd_E_twcu(cur)) < 0) return ret;
	return CMD_OK;
}

int sla_set_tilt_cur(unsigned char cur)
{
	int ret;
	if ((ret = sla_cmd_E_ticu(cur)) < 0) return ret;
	return CMD_OK;
}

int sla_set_tower_sgt(unsigned char sgt)
{
	int ret;
	if ((ret = sla_cmd_E_twsg(sgt)) < 0) return ret;
	return CMD_OK;
}

int sla_set_tilt_sgt(unsigned char sgt)
{
	int ret;
	if ((ret = sla_cmd_E_tisg(sgt)) < 0) return ret;
	return CMD_OK;
}

int sla_set_uled(char uled, unsigned short uled_time)
{
	int ret;
	int timeout = uled_time + (uled_time >> 4); //timeout = 1+1/16 
	if ((ret = sla_cmd_E_uled(uled, uled_time)) < 0) return ret;
	if (timeout)
		if ((ret = sla_wait(SLA_WAIT_ULED, timeout)) < 0) return ret;
	return CMD_OK;
}

int sla_clr_sgdata(void)
{
	int ret;
	if ((ret = sla_cmd_E_sgbd()) < 0) return ret;
	return CMD_OK;
}

int sla_get_sgdata(unsigned char* pbuff, unsigned char* psize)
{
	int ret;
	unsigned char size = 0;
	while ((ret = sla_cmd_Q_sgbd(pbuff + size)) >= 0)
	{
		size += ret;
		if (ret < 10)
		{
			if (psize) *psize = size;
			return CMD_OK;
		}
	}
	return ret;
}


int sla_set_axis_cfg(sla_axis_cfg_t* pcfg, unsigned short sr0, unsigned short srm, unsigned short acc, unsigned short dec, unsigned char cur, char sgt, unsigned short cst)
{
	pcfg->sr0 = sr0;
	pcfg->srm = srm;
	pcfg->acc = acc;
	pcfg->dec = dec;
	pcfg->cur = cur;
	pcfg->sgt = sgt;
	pcfg->cst = cst;
	return CMD_OK;
}



int sla_set_axis_cur(int axis, unsigned char cur)
{
	if (axis == 0) return sla_set_tower_cur(cur);
	if (axis == 1) return sla_set_tilt_cur(cur);
	return -1;
}

int sla_set_axis_sgt(int axis, char sgt)
{
	if (axis == 0) return sla_set_tower_sgt(sgt);
	if (axis == 1) return sla_set_tilt_sgt(sgt);
	return -1;
}

int sla_set_axis_pos(int axis, long pos)
{
	if (axis == 0) return sla_set_tower_pos(pos);
	if (axis == 1) return sla_set_tilt_pos(pos);
	return -1;
}

int sla_move_axis_abs(int axis, long pos)
{
	if (axis == 0) return sla_move_tower_abs(pos);
	if (axis == 1) return sla_move_tilt_abs(pos);
	return -1;
}

int sla_select_axis_profile(int axis, int idx)
{
	if (axis == 0) return sla_select_tower_profile(idx);
	if (axis == 1) return sla_select_tilt_profile(idx);
	return -1;
}
