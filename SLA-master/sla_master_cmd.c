// sla_master_cmd.c
#include "sla_master_cmd.h"
#include <stdarg.h>
#include <sys/time.h> 


FILE* cmd_in = 0;
FILE* cmd_out = 0;
FILE* cmd_log = 0;

#ifdef ARDUINO
#define time_ms millis
#else //ARDUINO
unsigned long time_ms(void)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}
#endif //ARDUINO


//receive single line, 
char sla_cmd_recv_line(int timeout, char* pline)
{
	char line[CMD_MAX_LINE];
	int count = 0;
	int c = -1;
	unsigned long ms = time_ms();
	unsigned long dt;
	while (timeout > 0)
	{
		while ((c = fgetc(cmd_in)) >= 0)
		{
//			printf(" c=%d count=%d\n", c, count);
			if (c == '\r') c = 0;
			if (c == '\n') c = 0;
			line[count] = c;
			if (c) count++;
			if ((c == 0) || (count >= CMD_MAX_LINE)) break;
		}
		if (c == 0) break;
//		if (c < 0) printf(" c=%d count=%d\n", c, count);
		usleep(1000);
		dt = (time_ms() - ms);
		ms += dt;
		timeout -= dt;
	}
	if (timeout <= 0)
	{
//		printf("#timeout\n");
		return CMD_ER_TMO;
	}
	if (pline) strcpy(pline, line);
/*	if (count == 2)
	{
		printf("#count == 2\n");
	}*/
	return count;
}


// send request - returns number of chars printed
char sla_cmd_send_req(char* fmt_str, ...)
{
	char reqstr[CMD_MAX_LINE];
	char reqlen;
	int ret;
	va_list va;
	va_start(va, fmt_str);
	vsprintf(reqstr, fmt_str, va);
//	ret = vfprintf(cmd_out, fmt_str, va);
//	fflush(cmd_out);
//	va_end(va);
//	va_start(va, fmt_str);
	va_end(va);
	if (cmd_log) fprintf(cmd_log, ">'%s'\n", reqstr);
//	printf("sla_cmd_send_req ret=%d\n", ret);
//	if (ret < 0)
//	{
//		perror("ERR");
//		return CMD_ER_UNK;
//	}
//	while (fgetc(cmd_in) >= 0)
//	{
//		fprintf(cmd_log, "cmdin\n");
//	}
	fprintf(cmd_out, "%s\n", reqstr);
//	fputc('\n', cmd_out);
	fflush(cmd_out);
	return (char)ret;
}

char sla_cmd_recv_res(char* fmt_str, ...)
{
	char ret = 0;
	char line[CMD_MAX_LINE];
	int count = 10;
	int timeout = CMD_TMO_DEF;
	unsigned long ms = time_ms();
	unsigned long dt;
	if (fmt_str && ((strlen(fmt_str) == 0) || (strcmp(fmt_str, "ok") == 0)))
		fmt_str = 0;
	while (timeout > 0)
	{
		ret = sla_cmd_recv_line(timeout, line);
		if (ret < 0) return ret;
		if (cmd_log) fprintf(cmd_log, "<'%s'\n", line);
		if (ret == 2)
		{
			if (/*(fmt_str == 0) && */(line[0] == 'o') && (line[1] == 'k'))
				return CMD_OK;
			if ((line[0] == 'e') && (line[1] >= '0') && (line[1] <= '9'))
				return -(line[1] - '0');
		}
		if (fmt_str && (ret >= 3))
		{
			if (strcmp(line + ret - 3, " ok") == 0)
			{
				va_list va;
				va_start(va, fmt_str);
				ret = (char)vsscanf(line, fmt_str, va);
				va_end(va);
				return ret;
			}
		}
		dt = (time_ms() - ms);
		ms += dt;
		timeout -= dt;
	}
	return CMD_ER_TMO;
}

char sla_cmd_Q_(unsigned short* pstatus)
{
	sla_cmd_send_req("?");
	return sla_cmd_recv_res("%hu", pstatus);
}


char sla_cmd_Q_ver(char* pver)
{
	char ret;
	char ver0[16] = {0};
	char ver1[32] = {0};
	sla_cmd_send_req("?ver");
	ret = sla_cmd_recv_res("%s %s", ver0, ver1);
	sprintf(pver, "%s %s ", ver0, ver1);
	return ret;
}

char sla_cmd_Q_ser(char* pser)
{
	sla_cmd_send_req("?ser");
	return sla_cmd_recv_res("%s", pser);
}

char sla_cmd_Q_tiho(char* ptiho)
{
	sla_cmd_send_req("?tiho");
	return sla_cmd_recv_res("%hhd", ptiho);
}

char sla_cmd_Q_tics(char* ptics)
{
	sla_cmd_send_req("?tics");
	return sla_cmd_recv_res("%hhd", ptics);
}

char sla_cmd_Q_ticf(char idx, sla_axis_cfg_t* pticf)
{
	if (idx < 0) sla_cmd_send_req("?ticf");
	else sla_cmd_send_req("?ticf %d", idx);
	return sla_cmd_recv_res("%hu %hu %hu %hu %hhu %hhd %hu",
		&pticf->sr0,
		&pticf->srm,
		&pticf->acc,
		&pticf->dec,
		&pticf->cur,
		&pticf->sgt,
		&pticf->cst
	);
}

char sla_cmd_Q_tipo(short* ptipo)
{
	sla_cmd_send_req("?tipo");
	return sla_cmd_recv_res("%hd", ptipo);
}

char sla_cmd_Q_ticu(unsigned char* pticu)
{
	sla_cmd_send_req("?ticu");
	return sla_cmd_recv_res("%hhu", pticu);
}

char sla_cmd_Q_twho(char* ptwho)
{
	sla_cmd_send_req("?twho");
	return sla_cmd_recv_res("%hhd", ptwho);
}

char sla_cmd_Q_twcs(char* ptwcs)
{
	sla_cmd_send_req("?twcs");
	return sla_cmd_recv_res("%hhd", ptwcs);
}

char sla_cmd_Q_twcf(char idx, sla_axis_cfg_t* ptwcf)
{
	if (idx < 0) sla_cmd_send_req("?twcf");
	else sla_cmd_send_req("?twcf %d", idx);
	return sla_cmd_recv_res("%hu %hu %hu %hu %hhu %hhd %hu",
		&ptwcf->sr0,
		&ptwcf->srm,
		&ptwcf->acc,
		&ptwcf->dec,
		&ptwcf->cur,
		&ptwcf->sgt,
		&ptwcf->cst
	);
}

char sla_cmd_Q_twpo(long* ptwpo)
{
	sla_cmd_send_req("?twpo");
	return sla_cmd_recv_res("%ld", ptwpo);
}

char sla_cmd_Q_twcu(unsigned char* ptwcu)
{
	sla_cmd_send_req("?twcu");
	return sla_cmd_recv_res("%hhu", ptwcu);
}

char sla_cmd_Q_uled(char* puled, unsigned short* puled_time)
{
	sla_cmd_send_req("?uled");
	return sla_cmd_recv_res("%hhd %hd", puled, puled_time);
}

char sla_cmd_Q_sgbc(unsigned char* psgbc)
{
	sla_cmd_send_req("?sgbc");
	return sla_cmd_recv_res("%hhd", psgbc);
}

char sla_cmd_Q_sgbd(unsigned char* psgbd)
{
	sla_cmd_send_req("?sgbd");
	return sla_cmd_recv_res("%hhx %hhx %hhx %hhx %hhx %hhx %hhx %hhx %hhx %hhx",
		psgbd + 0,
		psgbd + 1,
		psgbd + 2,
		psgbd + 3,
		psgbd + 4,
		psgbd + 5,
		psgbd + 6,
		psgbd + 7,
		psgbd + 8,
		psgbd + 9
	);
}


char sla_cmd_E_rst(void)
{
	char ret;
	char line[CMD_MAX_LINE];
	int timeout = CMD_TMO_RST;
	unsigned long ms = time_ms();
	unsigned long dt;
	sla_cmd_send_req("!rst");
	while (timeout > 0)
	{
		if ((ret = sla_cmd_recv_line(timeout, line)) < 0) return ret;
		if (cmd_log) fprintf(cmd_log, "<'%s'\n", line);
		if (strcmp(line, "start") == 0) return CMD_OK;
		usleep(1000);
		dt = (time_ms() - ms);
		ms += dt;
		timeout -= dt;
	}
	return CMD_ER_TMO;
}

char sla_cmd_E_shdn(unsigned short shdn)
{
	sla_cmd_send_req("!shdn %hu", shdn);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_motr(void)
{
	sla_cmd_send_req("!motr");
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_tiho(void)
{
	sla_cmd_send_req("!tiho");
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_tipo(short tipo)
{
	sla_cmd_send_req("!tipo %d", tipo);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_tima(short pos)
{
	sla_cmd_send_req("!tima %d", pos);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_tics(char idx)
{
	sla_cmd_send_req("!tics %hhd", idx);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_ticf(sla_axis_cfg_t* pcfg)
{
	sla_cmd_send_req("!ticf %hu %hu %hu %hu %hhu %hhd %hu",
		pcfg->sr0,
		pcfg->srm,
		pcfg->acc,
		pcfg->dec,
		pcfg->cur,
		pcfg->sgt,
		pcfg->cst
	);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_ticu(unsigned char ticu)
{
	sla_cmd_send_req("!ticu %hhu", ticu);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_tisg(char tisg)
{
	sla_cmd_send_req("!tisg %hu", tisg);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_twho(void)
{
	sla_cmd_send_req("!twho");
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_twpo(long twpo)
{
	sla_cmd_send_req("!twpo %ld", twpo);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_twma(long pos)
{
	sla_cmd_send_req("!twma %ld", pos);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_twcs(char idx)
{
	sla_cmd_send_req("!twcs %hhd", idx);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_twcf(sla_axis_cfg_t* pcfg)
{
	sla_cmd_send_req("!twcf %hu %hu %hu %hu %hhu %hhd %hu",
		pcfg->sr0,
		pcfg->srm,
		pcfg->acc,
		pcfg->dec,
		pcfg->cur,
		pcfg->sgt,
		pcfg->cst
	);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_twcu(unsigned char twcu)
{
	sla_cmd_send_req("!twcu %hhu", twcu);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_twsg(char twsg)
{
	sla_cmd_send_req("!twsg %hhu", twsg);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_uled(char uled, unsigned short uled_time)
{
	sla_cmd_send_req("!uled %hhd %hu", uled, uled_time);
	return sla_cmd_recv_res(0);
}

char sla_cmd_E_sgbd(void)
{
	sla_cmd_send_req("!sgbd");
	return sla_cmd_recv_res(0);
}
