#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#pragma warning(disable:4996)

#define EEPROM_SIZE 1024
uint8_t __eeprom[EEPROM_SIZE];
uint8_t __eeprom_loaded = 0;


void __eeprom_save(void)
{
	FILE* pf = fopen("EEPROM.dat", "w");
	if (pf)
	{
		fwrite(__eeprom, 1, EEPROM_SIZE, pf);
		fclose(pf);
	}
	__eeprom_loaded = 1;
}

void __eeprom_load(void)
{
	FILE* pf = fopen("EEPROM.dat", "r");
	if (pf)
	{
		fread(__eeprom, 1, EEPROM_SIZE, pf);
		fclose(pf);
	}
	else
		memset(__eeprom, 0xff, EEPROM_SIZE);
	__eeprom_loaded = 1;
}


uint8_t eeprom_read_byte (const uint8_t *__p)
{
	uint16_t addr = (uint16_t)__p;
	if (addr >= EEPROM_SIZE) return 0xff;
	if (!__eeprom_loaded) __eeprom_load();
	return __eeprom[addr];
}

void eeprom_write_byte (const uint8_t *__p, uint8_t value)
{
	uint16_t addr = (uint16_t)__p;
	if (addr >= EEPROM_SIZE) return;
	if (__eeprom[addr] == value) return;
	__eeprom[addr] = value;
	__eeprom_save();
}

void eeprom_update_byte (const uint8_t *__p, uint8_t value)
{
	uint16_t addr = (uint16_t)__p;
	if (addr >= EEPROM_SIZE) return;
	if (__eeprom[addr] == value) return;
	__eeprom[addr] = value;
	__eeprom_save();
}


uint16_t eeprom_read_word(const uint16_t *__p)
{
	uint16_t addr = (uint16_t)__p;
	if (addr >= EEPROM_SIZE) return 0xffff;
	if (!__eeprom_loaded) __eeprom_load();
	return ((uint16_t)__eeprom[addr + 1] << 8) | __eeprom[addr];
}

void eeprom_write_word (const uint16_t *__p, uint16_t value)
{
	uint16_t addr = (uint16_t)__p;
	if (addr >= EEPROM_SIZE) return;
	if (__eeprom[addr] == value) return;
	__eeprom[addr] = (value & 0xff);
	__eeprom[addr + 1] = (value >> 8);
	__eeprom_save();
}

void eeprom_update_word (const uint16_t *__p, uint16_t value)
{
	uint16_t addr = (uint16_t)__p;
	if (addr >= EEPROM_SIZE) return;
	if (__eeprom[addr] == value) return;
	__eeprom[addr] = (value & 0xff);
	__eeprom[addr + 1] = (value >> 8);
	__eeprom_save();
}
