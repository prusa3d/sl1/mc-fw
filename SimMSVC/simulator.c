#include "simulator.h"
#include <stdio.h>
#include <avr/io.h>
#ifdef __unix__
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#define Sleep usleep
#endif
#include <config.h>

#ifdef _SIM_LCD
#include "sim_lcd.h"
#endif //_SIM_LCD

unsigned long __data_load_end = 0x11223344;
unsigned short __sim_sp_min = 0xffff;

unsigned char UDR1 = 0;

unsigned char SREG = 0;


unsigned char TCCR0A = 0;
unsigned char TCCR0B = 0;
unsigned char TCNT0 = 0;

unsigned char EICRB = 0;
unsigned char EIMSK = 0;

unsigned char OCR0A = 0;
unsigned char OCR0B = 0;
unsigned char TIMSK0 = 0;
unsigned char TIMSK1 = 0;
unsigned short OCR1A = 0;
unsigned short OCR1B = 0;
unsigned short OCR1C = 0;
unsigned short TCNT1 = 0;
unsigned char TCCR1A = 0;
unsigned char TCCR1B = 0;

unsigned char TIMSK3 = 0;
unsigned short TCNT3 = 0;
unsigned short OCR3A = 0;
unsigned short OCR3B = 0;
unsigned short OCR3C = 0;
unsigned char TCCR3A = 0;
unsigned char TCCR3B = 0;


unsigned char DDRA = 0;
unsigned char DDRB = 0;
unsigned char DDRC = 0;
unsigned char DDRD = 0;
unsigned char DDRE = 0;
unsigned char DDRF = 0;
unsigned char DDRG = 0;
unsigned char DDRH = 0;
unsigned char DDRI = 0;
unsigned char DDRJ = 0;
unsigned char DDRK = 0;
unsigned char DDRL = 0;

unsigned char PORTA = 0;
unsigned char PORTB = 0;
unsigned char PORTC = 0;
unsigned char PORTD = 0;
unsigned char PORTE = 0;
unsigned char PORTF = 0;
unsigned char PORTG = 0;
unsigned char PORTH = 0;
unsigned char PORTI = 0;
unsigned char PORTJ = 0;
unsigned char PORTK = 0;
unsigned char PORTL = 0;

unsigned char PINA = 0;
unsigned char PINB = 0;
unsigned char PINC = 0;
unsigned char PIND = 0;
unsigned char PINE = 0;
unsigned char PINF = 0;
unsigned char PING = 0;
unsigned char PINH = 0;
unsigned char PINI = 0;
unsigned char PINJ = 0;
unsigned char PINK = 0;
unsigned char PINL = 0;

unsigned char SPCR = 0;
unsigned char SPSR = 0;
unsigned char SPDR = 0;

unsigned char ADCSRA = 0;
unsigned char ADCSRB = 0;
unsigned char ADMUX = 0;
unsigned char DIDR0 = 0;
unsigned char DIDR2 = 0;
unsigned short ADC = 550; // Provides error-less temperatures

unsigned char MCUSR = 0;
unsigned char WDTCSR = 0;
unsigned char CLKSEL0 = 0;
unsigned char CLKSEL1 = 0;
unsigned char CLKPR = 0;
unsigned char UHWCON = 0;
unsigned char USBCON = 0;


unsigned char UCSR1A = 0;
unsigned char UBRR1L = 0;
unsigned char UCSR1B = 0;

unsigned short SP = 0x0a00;


#ifdef __unix__
#include <pthread.h>
#else
#include <windows.h>
#endif
#include <io.h>

//extern FILE _uart0io;
extern FILE _uart1io;
extern FILE _lcdio;
//extern FILE* uart0io;
extern FILE* uart1io;

uint8_t sim_reset_req = 0;

#ifdef __unix__
pthread_t sim_timer0_thread;
//DWORD sim_timer0_threadId = 0;
pthread_t sim_timer1_thread;
//DWORD sim_timer1_threadId = 0;
#else
HANDLE sim_timer0_thread = 0;
DWORD sim_timer0_threadId = 0;
HANDLE sim_timer1_thread = 0;
DWORD sim_timer1_threadId = 0;
#endif

void sim_cycle(void);
void sim_init(void);
void sim_done(void);

void sim_uart0io_init(void);
void sim_uart0io_done(void);
void sim_uart1io_init(void);
void sim_uart1io_done(void);

#ifdef __unix__
void *sim_timer0_threadProc(void *lpParameter);
#else
DWORD WINAPI sim_timer0_threadProc(LPVOID lpParameter);
#endif

extern void TIMER0_OVF_vect(void);
extern void TIMER0_COMPA_vect(void);
extern void TIMER0_COMPB_vect(void);
extern void TIMER1_COMPA_vect(void);

int threadStarted = 0;

void sim_cycle(void)
{
#ifdef __unix__
	// TODO: Implement unix codepath
#else
	MSG sMsg;
	int iResult = 0;
	if (PeekMessage(&sMsg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&sMsg); 
		DispatchMessage(&sMsg);
	}
#endif
#ifdef _SIM_LCD
	fflush(sim_lcd_outF[1]);
#endif //_SIM_LCD
	Sleep(10);
}

void sim_init(void)
{
	// Reset ports
	PORTB = 0; // Necessary for SPI after !rst

	sim_reset_req = 0;
#ifdef _SIM_LCD
	sim_lcd_init();
#endif //_SIM_LCD
	if(!threadStarted) {
		threadStarted = 1;
#ifdef __unix__
		if(pthread_create( &sim_timer0_thread, NULL, sim_timer0_threadProc, NULL)) {
			printf("error starting timer0 thread\n");
		}
#else
		sim_timer0_thread = CreateThread(0, 0, sim_timer0_threadProc, 0, 0, &sim_timer0_threadId);
#endif
	}
}

void sim_done(void)
{
#ifdef _SIM_LCD
	sim_window_done();
#endif //_SIM_LCD
}

void sim_uart0io_init(void)
{
//	char buff[256];
//	int sz;
//	HWND wnd;

/*
	int nPipe = 0;
	HANDLE hPipe = CreateNamedPipe("\\\\.\\Pipe\\AVRsim_uart0",
		PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED, PIPE_TYPE_BYTE | PIPE_WAIT,
		PIPE_UNLIMITED_INSTANCES, MAX_PATH, MAX_PATH, NMPWAIT_USE_DEFAULT_WAIT, NULL);
	if (hPipe == INVALID_HANDLE_VALUE)
	{
		GetLastError();
	}
	nPipe = _open_osfhandle(hPipe, 0);
	if (nPipe < 0)
	{
		GetLastError();
	}
	uart0io = fdopen(nPipe, "w+");
*/
/*	if (!CreatePipe(huart0i, huart0i + 1, NULL, 512)) return;
	if (!CreatePipe(huart0o, huart0o + 1, NULL, 512))
	{
		CloseHandle(huart0i[0]);
		CloseHandle(huart0i[1]);
		return;
	}
	int nfd_ir = _open_osfhandle(huart0i[0], 0);
	int nfd_ow = _open_osfhandle(huart0o[1], 0);
	uart0io = fdopen(nPort, "w+");
	uart0io = fdopen(nPort, "w+");
//	HANDLE hPort = 0;
	int nPort = 0;
	char cText[256];
	int i;*/
/*	HANDLE hPort = 0;
	int nPort = 0;
//	if ((hPort = CreateFile( "\\\\.\\COM40", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
	if ((hPort = CreateFile( "\\\\.\\COM40", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL)) == INVALID_HANDLE_VALUE)
		return;
	nPort = _open_osfhandle(hPort, 0);
	uart0io = fdopen(nPort, "r+");
//	uart0io = fopen("\\\\.\\COM40", "w+");

	fprintf(uart0io, "Test\n");
	fflush(uart0io);

	sz = 1;
	while (sz)
	{
	sz = fread(buff, 1, 256, uart0io);
	if (sz > 0)
	{
		printf("256 ok\n");
	}
	}

	if (uart0io == 0)
	{
//		CloseHandle(hPort);
		return;
	}
//	i = fgetc(uart0io);
	//fgets(cText, 256, uart0io);

	sim_init_window();
//	uart0io = fopen("\\\\.\\COM40", "rb");*/
}

void sim_uart0io_done(void)
{
}

void sim_uart1io_init(void)
{
}

void sim_uart1io_done(void)
{
}


void _delay_us(double __us)
{
}

void _delay_ms(double __ms)
{
}

void cli(void)
{
}

void sei(void)
{
}


void wdt_enable(uint8_t wdto)
{
	if (wdto == 0)
		sim_reset_req = 1;
}

void wdt_disable(void)
{
}

void wdt_reset(void)
{
}

void fdev_setup_stream(void* stream, void* put, void* get, int rwflag)
{
/*	if (stream == &_uart0io) sim_uart0io_init();
	else */if (stream == &_uart1io) sim_uart1io_init();
#ifdef _SIM_LCD
	else if (stream == &_lcdio) sim_lcdio_init();
#endif //_SIM_LCD

/*#define fdev_setup_stream(stream, p, g, f) \
	do { \
		(stream)->put = p; \
		(stream)->get = g; \
		(stream)->flags = f; \
		(stream)->udata = 0; \
	} while(0) */
}

extern uint16_t sla_beep_ms;

#ifdef __unix__
void *sim_timer0_threadProc(void *lpParameter)
#else
DWORD WINAPI sim_timer0_threadProc(LPVOID lpParameter)
#endif
{
	while (1)
	{
		for(int i = 0; i < 2; i++) {
			if (TIMSK0 & (1 << TOIE0))
				TIMER0_OVF_vect();
			if (TIMSK0 & (1 << OCIE0A))
				TIMER0_COMPA_vect();
			Sleep(10);
		}
#ifdef _SIM_TIMER1
		TIMER1_COMPA_vect();
#endif
	}
	return 0;
}


#ifdef _SIM_SPI

uint8_t _sim_spi_mcp23s17_reg_iodira = 0;
uint8_t _sim_spi_mcp23s17_reg_iodirb = 0;
uint8_t _sim_spi_mcp23s17_reg_gpioa = 0;
uint8_t _sim_spi_mcp23s17_reg_gpiob = 0;
uint8_t _sim_spi_mcp23s17_address = 0;
uint8_t _sim_spi_mcp23s17_register = 0;
uint8_t _sim_spi_mcp23s17_state = 0;

void _sim_spi_mcp23s17(void)
{
	if (PORTB & (1<<4))
	{
		_sim_spi_mcp23s17_state = 0;
		return;
	}
	switch (_sim_spi_mcp23s17_state)
	{
	case 0: //address
		_sim_spi_mcp23s17_state++;
		_sim_spi_mcp23s17_address = SPDR;
		SPDR = 0xff;
		break;
	case 1: //register
		_sim_spi_mcp23s17_state++;
		_sim_spi_mcp23s17_register = SPDR;
		SPDR = 0xff;
		break;
	case 2: //value
		_sim_spi_mcp23s17_state = 0;
		if (_sim_spi_mcp23s17_address == 0x41)
		{ //read
			switch (_sim_spi_mcp23s17_register)
			{
			case 0x00://_REG_IODIRA
				SPDR = _sim_spi_mcp23s17_reg_iodira;
				break;
			case 0x01://_REG_IODIRB
				SPDR = _sim_spi_mcp23s17_reg_iodirb;
				break;
			case 0x12://_REG_GPIOA
				SPDR = _sim_spi_mcp23s17_reg_gpioa;
				break;
			case 0x13://_REG_GPIOB
				SPDR = _sim_spi_mcp23s17_reg_gpiob;
				break;
			default:
				break;
			}
		}
		else if (_sim_spi_mcp23s17_address == 0x40)
		{ //write
			switch (_sim_spi_mcp23s17_register)
			{
			case 0x00://_REG_IODIRA
				_sim_spi_mcp23s17_reg_iodira = SPDR;
				break;
			case 0x01://_REG_IODIRB
				_sim_spi_mcp23s17_reg_iodirb = SPDR;
				break;
			case 0x12://_REG_GPIOA
				_sim_spi_mcp23s17_reg_gpioa = SPDR;
				break;
			case 0x13://_REG_GPIOB
				_sim_spi_mcp23s17_reg_gpiob = SPDR;
				break;
			default:
				break;
			}
			SPDR = 0xff;
		}
		break;
	}
}


uint8_t _sim_spi_tmc2130_state[2] = {0, 0};


void _sim_spi_tmc2130(uint8_t axis)
{
	if ((axis == 0) && (PORTB & (1<<4)))
	{
		_sim_spi_mcp23s17_state = 0;
		return;
	}
	if ((axis == 1) && (PORTB & (1<<4)))
	{
		_sim_spi_mcp23s17_state = 0;
		return;
	}
//	switch (_sim_spi_mcp23s17_state)
	{
	}
}

void _sim_spi_tmc2130x2(void)
{
	_sim_spi_tmc2130(0);
	_sim_spi_tmc2130(1);
}

void _sim_spi(void)
{
	_sim_spi_mcp23s17();
	_sim_spi_tmc2130x2();
	SPSR |= (1 << SPIF);
}
#endif //_SIM_SPI

//const uint8_t serial[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
const uint8_t serial[16] =   {'4','6','_','0', '6','1','9','6', '7','8','C','1', '2','3','4','5'};

uint8_t pgm_read_byte(uint8_t* addr)
{
	uint32_t ad = (uint32_t)addr;
	if (ad < 0x10000)
	{
		if ((ad >= 0x7ff0) && (ad < 0x8000))
		{
			return serial[ad - 0x7ff0];
		}
		return 0xff;
	}
	return (*((uint8_t*)addr));
}

uint16_t pgm_read_word(uint16_t* addr)
{
	uint32_t ad = (uint32_t)addr;
	if (ad < 0x10000)
	{
		return 0xffff;
	}
	return (*((uint16_t*)addr));
}

uint32_t pgm_read_dword(uint32_t* addr)
{
	uint32_t ad = (uint32_t)addr;
	if (ad < 0x10000)
	{
		return 0xffffffff;
	}
	return (*((uint32_t*)addr));
}

void copy_format(char* format_new, const char* format)
{
	int len = strlen(format);
	int arg = 0;
	char c;
	int i;
	strcpy(format_new, format);
	for (i = 0; i < len; i++)
	{
		c = format_new[i];
		if (arg)
		{
			if (strchr("duifs", c))
				arg = 0;
			else if (c == '%')
				arg = 0;
			else if (c == 'S')
			{
				format_new[i] = 's';
				arg = 0;
			}
			else if (strchr("0123456789+-l", c) == 0)
				arg = 0;
		}
		else
			if (c == '%')
				arg = 1;
	}
}

int printf_P(const char* format, ...)
{
	int ret;
	char format_new[1024];
	va_list va;
	va_start(va, format);
	copy_format(format_new, format);
	ret = vprintf(format_new, va);
	va_end(va);
	return ret;
}

int fprintf_P(FILE* file, const char* format, ...)
{
	int ret;
	char format_new[1024];
	va_list va;
	va_start(va, format);
	copy_format(format_new, format);
	ret = vfprintf(file, format_new, va);
	va_end(va);
	return ret;
}

int vfprintf_P(FILE* file, const char* format, va_list arg)
{
	int ret;
	char format_new[1024];
	copy_format(format_new, format);
	ret = vfprintf(file, format_new, arg);
	return ret;
}
