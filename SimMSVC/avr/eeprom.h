//eeprom.h
#ifndef _AVR_EEPROM_H_
#define _AVR_EEPROM_H_

#include <inttypes.h>


#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern uint8_t eeprom_read_byte (const uint8_t *__p);
extern void eeprom_write_byte (const uint8_t *__p, uint8_t value);
extern void eeprom_update_byte (const uint8_t *__p, uint8_t value);
extern uint16_t eeprom_read_word(const uint16_t *__p);
extern void eeprom_write_word (const uint16_t *__p, uint16_t value);
extern void eeprom_update_word (const uint16_t *__p, uint16_t value);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_AVR_EEPROM_H_
