#ifndef _AVR_BOOT_H_
#define _AVR_BOOT_H_


#define GET_LOW_FUSE_BITS      1
#define GET_HIGH_FUSE_BITS     2
#define GET_EXTENDED_FUSE_BITS 3
#define GET_LOCK_BITS          4


#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern uint8_t boot_signature_byte_get(uint8_t addr);
extern uint8_t boot_lock_fuse_bits_get(uint8_t addr);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_AVR_BOOT_H_
