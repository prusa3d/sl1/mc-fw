// pgmspace.h
#ifndef _PGMSPACE_H
#define _PGMSPACE_H

#define PROGMEM
#define PSTR
//#define printf_P printf
//#define fprintf_P fprintf
#define fputs_P fputs
#define strncmp_P strncmp
#define sscanf_P sscanf
//#define pgm_read_byte(addr) (*((uint8_t*)addr))
//#define pgm_read_word(addr) (*((uint16_t*)addr))
//#define pgm_read_dword(addr) (*((uint32_t*)addr))

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern uint8_t pgm_read_byte(uint8_t* addr);
extern uint16_t pgm_read_word(uint16_t* addr);
extern uint32_t pgm_read_dword(uint32_t* addr);

#include <stdio.h>

extern int printf_P(const char* format, ...);
extern int fprintf_P(FILE* file, const char* format, ...);
extern int vfprintf_P(FILE* file, const char* format, va_list arg);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_PGMSPACE_H
