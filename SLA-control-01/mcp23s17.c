/*
 * gpio expander functions
 */
#include "mcp23s17.h"

#ifdef MCP23S17

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include "io_atmega32u4.h"
#include "spi.h"
#include "cmd.h"

#define _REG_IODIRA    0x00
#define _REG_IODIRB    0x01
#define _REG_IPOLA     0x02
#define _REG_IPOLB     0x03
#define _REG_GPINTENA  0x04
#define _REG_GPINTENB  0x05
#define _REG_INTCONA   0x06
#define _REG_INTCONB   0x07
#define _REG_DEFVALA   0x08
#define _REG_DEFVALB   0x09
#define _REG_ICONA     0x0a
#define _REG_ICONB     0x0b
#define _REG_GPPUA     0x0c
#define _REG_GPPUB     0x0d
#define _REG_INTFA     0x0e
#define _REG_INTFB     0x0f
#define _REG_INTCAPA   0x10
#define _REG_INTCAPB   0x11
#define _REG_GPIOA     0x12
#define _REG_GPIOB     0x13
#define _REG_OLATA     0x14
#define _REG_OLATB     0x15

uint8_t mcp23s17_rd(uint8_t dev_addr, uint8_t reg)
{
	uint8_t val;
	spi_setup(MCP23S17_SPCR, MCP23S17_SPSR);
	PIN_CLR(MCP23S17_CS_PIN);
	spi_txrx(0x41 | (dev_addr << 1));
	spi_txrx(reg);
	val = spi_txrx(0xff);
	PIN_SET(MCP23S17_CS_PIN);
	return val;
}

void mcp23s17_wr(uint8_t dev_addr, uint8_t reg, uint8_t val)
{
	spi_setup(MCP23S17_SPCR, MCP23S17_SPSR);
	PIN_CLR(MCP23S17_CS_PIN);
	spi_txrx(0x40 | (dev_addr << 1));
	spi_txrx(reg);
	spi_txrx(val);
	PIN_SET(MCP23S17_CS_PIN);
}

uint8_t mcp23s17_init(uint8_t dev_addr, uint16_t iodir, uint16_t gpio)
{
	PIN_OUT(MCP23S17_CS_PIN); //CS output
	PIN_SET(MCP23S17_CS_PIN); //CS high
	PIN_CLR(MCP23S17_CS_PIN); //toggle CS once for sure
	PIN_SET(MCP23S17_CS_PIN); //..
	mcp23s17_dir_a(dev_addr, iodir & 0xff);
	mcp23s17_dir_b(dev_addr, iodir >> 8);
	mcp23s17_out_a(dev_addr, gpio & 0xff);
	mcp23s17_out_b(dev_addr, gpio >> 8);
	if (mcp23s17_rd(dev_addr, _REG_IODIRA) != (iodir & 0xff)) return 0;
	if (mcp23s17_rd(dev_addr, _REG_IODIRB) != (iodir >> 8)) return 0;
	return 1;
}

void mcp23s17_dir_a(uint8_t dev_addr, uint8_t iodira)
{
	mcp23s17_wr(dev_addr, _REG_IODIRA, iodira);
}

void mcp23s17_dir_b(uint8_t dev_addr, uint8_t iodirb)
{
	mcp23s17_wr(dev_addr, _REG_IODIRB, iodirb);
}

void mcp23s17_out_a(uint8_t dev_addr, uint8_t gpioa)
{
	mcp23s17_wr(dev_addr, _REG_GPIOA, gpioa);
}

void mcp23s17_out_b(uint8_t dev_addr, uint8_t gpiob)
{
	mcp23s17_wr(dev_addr, _REG_GPIOB, gpiob);
}

uint8_t mcp23s17_in_a(uint8_t dev_addr)
{
	uint8_t ret = mcp23s17_rd(dev_addr, _REG_GPIOA);
	return ret;
}

uint8_t mcp23s17_in_b(uint8_t dev_addr)
{
	uint8_t ret = mcp23s17_rd(dev_addr, _REG_GPIOB);
	return ret;
}

#endif //MCP23S17
