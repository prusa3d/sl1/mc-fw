/**
 * signature, pseudo unique number and serial number functions
 */
#include "sla_sign.h"
#include "sla.h"
#include <avr/boot.h>
#include <avr/pgmspace.h>


void sla_print_signature(FILE* out)
{
	uint8_t r;
	uint8_t c;
	uint8_t addr = 0;
	for (r = 0; r < 8; r++)
	{
		fprintf_P(out, PSTR("%04x"), addr);
		for (c = 0; c < 16; c++)
			fprintf_P(out, PSTR(" %02x"), boot_signature_byte_get(addr++));
		fprintf_P(out, PSTR("\n"));
	}
}

const uint8_t serial_signature_addr[16] PROGMEM = {
0x01,
0x03,
0x05,
0x0e,
0x0f,
0x11,
0x12,
0x13,
0x15,
0x16,
0x17,
0x38,
0x3a,
0x00, //CPU signature 0x1e
0x02, //CPU signature 0x95
0x04  //CPU signature 0x87
};

void sla_read_pseudo_unique(char* psn)
{
	uint8_t i;
	for (i = 0; i < 16; i++)
		psn[i] = boot_signature_byte_get(pgm_read_byte(serial_signature_addr + i));
}

#define SERIAL_NUMBER_ADDR 0x7ff0

void sla_read_serial(uint8_t* psn)
{
	uint8_t i;
	for (i = 0; i < 16; i++)
		psn[i] = pgm_read_byte(SERIAL_NUMBER_ADDR + i);
}
