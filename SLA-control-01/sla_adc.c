/*
 * logic layer for ADC for SL1
 */
#include "sla_adc.h"
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "sla.h"
#include "adc.h"
#include "intpol.h"
#include "sla_spi.h"

uint8_t sla_adc_changed = 0;
uint16_t sla_temp_raw[4];
uint16_t sla_volt_raw[4];
int16_t sla_temp[4] = {400, 200, 200, 200};
int16_t sla_volt[4] = {0, 0, 0, 24000};
uint8_t sla_adc_timer = 0;
uint8_t sla_mux = 0;
const uint16_t uvled_temp_table_raw[34] PROGMEM =
{  73,  83,  95, 109, 125, 144, 165, 189, 217, 248,
  284, 323, 366, 412, 462, 514, 567, 620, 673, 723,
  770, 813, 851, 885, 913, 937, 957, 973, 986, 995,
  1003,1009,1013,1016
};
const uint16_t ambient_temp_table_raw[34] PROGMEM =
{  25,  29,  34,  40,  46,  54,  64,  75,  88, 105,
  124, 146, 173, 204, 241, 282, 330, 382, 439, 500,
  563, 625, 687, 744, 796, 842, 882, 915, 941, 963,
  979,992,1001,1008
};

void sla_set_mux(uint8_t mux)
{
	sla_gpio[1] = (sla_gpio[1] & ~0x18) | ((mux & 0x03) << 3);
	sla_spi_async(SLA_SPI_REQ_GPIO_OUT_B);
	sla_mux = mux;
}

void adc_ready(void)
{
	sla_temp_raw[sla_mux] = adc_val[0];
	sla_volt_raw[sla_mux] = adc_val[1];
	sla_adc_changed |= (0x11 << sla_mux);
	sla_set_mux((sla_mux + 1) & 0x03);
}

void sla_adc_cycle(void)
{
	uint8_t adc_changed;
	cli();
	adc_changed = sla_adc_changed;
	sla_adc_changed = 0;
	sei();
	if (adc_changed)
	{
		if (adc_changed & 0x01) sla_temp[0] = sla_calc_temp_uvled(sla_temp_raw[0]);
		if (adc_changed & 0x02) sla_temp[1] = sla_calc_temp_ambient(sla_temp_raw[1]);
		if (adc_changed & 0x04) sla_temp[2] = sla_calc_temp_ambient(sla_temp_raw[2]);
		if (adc_changed & 0x08) sla_temp[3] = sla_calc_temp_ambient(sla_temp_raw[3]);
		if (adc_changed & 0x10) sla_volt[0] = sla_calc_volt_uvled(sla_volt_raw[0]);
		if (adc_changed & 0x20) sla_volt[1] = sla_calc_volt_uvled(sla_volt_raw[1]);
		if (adc_changed & 0x40) sla_volt[2] = sla_calc_volt_uvled(sla_volt_raw[2]);
		if (adc_changed & 0x80) sla_volt[3] = sla_calc_volt_power(sla_volt_raw[3]);
	}
}

//calculate uv-led voltage [mV] from raw value
int16_t sla_calc_volt_uvled(uint16_t raw)
{
	if ((sla_leds & SLA_MSK_ULED) == 0)
		return 0; //report 0 while uvled off
	//adc input voltage
	uint16_t uadc = adc_calc_mV(raw >> 2);
	//input voltage before voltage divider
	int16_t udiv = (int16_t)(((uint32_t)uadc * SLA_VOLT_MEAS_DIVR) >> 10);
	//UV-led voltage
	int16_t uled = (int16_t)(sla_volt[3] - udiv/* - SLA_VOLT_MEAS_VCSR*/);
	//limit negative values
	if (uled < 0) uled = 0;
	return uled;
}

int16_t sla_calc_volt_power(uint16_t raw)
{
#if (BOARD == SLA_REV06)
	//adc input voltage
	uint16_t uadc = adc_calc_mV(raw >> 2);
	//input voltage before voltage divider
	int16_t upwr = (int16_t)(((uint32_t)uadc * SLA_VOLT_MEAS_DIVR2) >> 10);
	return upwr;
#endif //(BOARD == SLA_REV06)
}

//calculate uv-led temperature [x0.1C] from raw value
int16_t sla_calc_temp_uvled(uint16_t raw)
{
	return interpolate_i16_ylin_P(raw >> 2, 34, uvled_temp_table_raw, 1250, -50);
}

//calculate ambient temperature [x0.1C] from raw value
int16_t sla_calc_temp_ambient(uint16_t raw)
{
	return interpolate_i16_ylin_P(raw >> 2, 34, ambient_temp_table_raw, 1250, -50);
}
