#!/bin/bash
#
# memstat.sh - memory usage statistics

BOARD=$1
if [ -z "$BOARD" ]; then echo 'No input file specified!' >&2; exit 1; fi

flash_size=28672
ram_size=2560

flash_usage=$(stat -c %s ./SLA-control_rev0${BOARD}.bin)
flash_usage_per=$((100 * $flash_usage / $flash_size))

ram_end=$(grep 'N _end' SLA-control_rev0${BOARD}.sym)
ram_end="0x${ram_end:4:4}"
ram_end=$(($ram_end))

ram_usage=$((ram_end + 1))
ram_usage_per=$((100 * $ram_usage / $ram_size))

echo "Memory usage:"
echo " flash: $(printf '0x%04x (%5d' $flash_usage $flash_usage) ""bytes, $flash_usage_per%)"
echo "   ram: $(printf '0x%04x (%5d' $ram_usage $ram_usage) ""bytes, $ram_usage_per%)"

exit 0
