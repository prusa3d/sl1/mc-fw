/*
 * pin setup helpers
 */
#include "sla_serv.h"
#include "avr/io.h"
#include "sla.h"

uint8_t sla_get_POR(uint8_t port)
{
	switch (port)
	{
	case 'B': return PORTB;
	case 'C': return PORTC;
	case 'D': return PORTD;
	case 'E': return PORTE;
	case 'F': return PORTF;
	}
	return 0;
}

void sla_set_POR(uint8_t port, uint8_t val)
{
	switch (port)
	{
	case 'B': PORTB = val; break;
	case 'C': PORTC = val; break;
	case 'D': PORTD = val; break;
	case 'E': PORTE = val; break;
	case 'F': PORTF = val; break;
	}
}

uint8_t sla_get_PIN(uint8_t port)
{
	switch (port)
	{
	case 'B': return PINB;
	case 'C': return PINC;
	case 'D': return PIND;
	case 'E': return PINE;
	case 'F': return PINF;
	}
	return 0;
}

void sla_set_PIN(uint8_t port, uint8_t val)
{
	switch (port)
	{
	case 'B': PINB = val; break;
	case 'C': PINC = val; break;
	case 'D': PIND = val; break;
	case 'E': PINE = val; break;
	case 'F': PINF = val; break;
	}
}

uint8_t sla_get_DDR(uint8_t port)
{
	switch (port)
	{
	case 'B': return DDRB;
	case 'C': return DDRC;
	case 'D': return DDRD;
	case 'E': return DDRE;
	case 'F': return DDRF;
	}
	return 0;
}

void sla_set_DDR(uint8_t port, uint8_t val)
{
	switch (port)
	{
	case 'B': DDRB = val; break;
	case 'C': DDRC = val; break;
	case 'D': DDRD = val; break;
	case 'E': DDRE = val; break;
	case 'F': DDRF = val; break;
	}
}
