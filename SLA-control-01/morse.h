//morse.h
#ifndef _MORSE_H
#define _MORSE_H
#include "config.h"
#include <inttypes.h>

//morse player bit-pair values 
#define MORSE_SPC 0 // space
#define MORSE_DOT 1 // dot
#define MORSE_DSH 2 // dash
#define MORSE_END 3 // end

//morse numbers (0..9):   num    morse       quad       bin (msb first)     hex
#define MORSE_0 (0xaa80) // 0 = "-----   " = 22222000 = 10101010 10000000 = 0xaa80
#define MORSE_1 (0x6a80) // 1 = ".----   " = 12222000 = 01101010 10000000 = 0x6a80
#define MORSE_2 (0x5a80) // 2 = "..---   " = 11222000 = 01011010 10000000 = 0x5a80
#define MORSE_3 (0x5680) // 3 = "...--   " = 11122000 = 01010110 10000000 = 0x5680
#define MORSE_4 (0x5580) // 4 = "....-   " = 11112000 = 01010101 10000000 = 0x5580
#define MORSE_5 (0x5540) // 5 = ".....   " = 11111000 = 01010101 01000000 = 0x5540
#define MORSE_6 (0x9540) // 6 = "-....   " = 21111000 = 10010101 01000000 = 0x9540
#define MORSE_7 (0xa540) // 7 = "--...   " = 22111000 = 10100101 01000000 = 0xa540
#define MORSE_8 (0xa940) // 8 = "---..   " = 22211000 = 10101001 01000000 = 0xa940
#define MORSE_9 (0xaa40) // 9 = "----.   " = 22221000 = 10101010 01000000 = 0xaa40

//morse charss (A..Z): char    morse   quad   bin        hex
#define MORSE_A (0x00) // A = ".-"   = 1230 = 01101100 = 0x00
#define MORSE_B (0x00) // B = "-..." = 2111 = 
#define MORSE_C (0x00) // C = "-.-." = 2121 = 
#define MORSE_D (0x00) // D = "-.."  = 2113 = 
#define MORSE_E (0x00) // E = "."    = 1300 = 
#define MORSE_F (0x00) // F = "..-." = 1121 = 
#define MORSE_G (0x00) // G = "--."  = 2213 = 
#define MORSE_H (0x00) // H = "...." = 1111 = 
#define MORSE_I (0x00) // I = ".."   = 1130 = 
#define MORSE_J (0x00) // J = ".---" = 1222 = 
#define MORSE_K (0x00) // K = "-.-"  = 2123 = 
#define MORSE_L (0x00) // L = ".-.." = 1211 = 
#define MORSE_M (0x00) // M = "--"   = 2230 = 
#define MORSE_N (0x00) // N = "-."   = 2130 = 
#define MORSE_O (0x00) // O = "---"  = 2223 = 
#define MORSE_P (0x00) // P = ".--." = 1221 = 
#define MORSE_Q (0x00) // Q = "--.-" = 2212 = 
#define MORSE_R (0x00) // R = ".-."  = 1213 = 
#define MORSE_S (0x00) // S = "..."  = 1113 = 
#define MORSE_T (0x00) // T = "-"    = 2300 = 

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern void morse_num(uint8_t num);
extern void morse_cycle(void);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_MORSE_H
