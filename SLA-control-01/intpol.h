#ifndef _INTPOL_H
#define _INTPOL_H
#include <inttypes.h>

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern int16_t interpolate_i16_P(int16_t x, uint8_t c, const int16_t* px, const int16_t* py);
extern int16_t interpolate_i16_ylin_P(int16_t x, uint8_t c, const uint16_t* px, int16_t y0, int16_t yd);
extern int16_t interpolate_i16_xlin_P(int16_t x, uint8_t c, int16_t x0, int16_t xd, const int16_t* py);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_INTPOL_H
