#ifndef _CMD_SLA_H
#define _CMD_SLA_H
#include "cmd.h"

#define MOD_MSK_0      0x0000
#define MOD_ID_0       0xff

#define CMD_ID_RST     0x01 //reset
#define CMD_ID_VER     0x02 //version
#define CMD_ID_SER     0x03 //serial number
#define CMD_ID_ENA     0x0c //enabled
#define CMD_ID_LED     0x0d //leds
#define CMD_ID_DIR     0x0f //direction
#define CMD_ID_MOT     0x10 //motion
#define CMD_ID_END     0x11 //endstops enabled
#define CMD_ID_PUN     0x12 //pseudo unique number
#define CMD_ID_REV     0x13 //board revision

#define CMD_ID_LOG     0x42 //log
#define CMD_ID_TST     0x43 //test

//SLA commands
#define CMD_ID_BEEP    0x81 //*beep(frequency, lenght)
#define CMD_ID_PLED    0x82 //*powerLed(state)
#define CMD_ID_SHDN    0x83 //*shutdown()
#define CMD_ID_ULED    0x84 //*uvLed(state)
#define CMD_ID_FANS    0x8a //*getFanState()
#define CMD_ID_MOTR    0x8d //*motorsRelease()
#define CMD_ID_TWUP    0x8e //*towerUpMicroSteps(microStepsRequested, delay)
#define CMD_ID_TWDN    0x8f //*towerDownMicroSteps(microStepsRequested, delay)
#define CMD_ID_TWCU    0x95 //*towerCurrent()
#define CMD_ID_TICU    0x96 //*tiltCurrent()
#define CMD_ID_TWHO    0x97 //*towerHome()
#define CMD_ID_TIHO    0x98 //*tiltHome()
#define CMD_ID_TWSG    0x99 //*towerSgth()
#define CMD_ID_TISG    0x9a //*tiltSgth()
#define CMD_ID_FRPM    0x9d //*getFanRPM(), setFanRPM()
#define CMD_ID_TEMP    0x9e //*getTemperatures()
#define CMD_ID_EECL    0x9f //*eepromClear()
#define CMD_ID_EERX    0xa0 //*eepromReadHex()
#define CMD_ID_EEWX    0xa1 //*eepromWriteHex()
#define CMD_ID_TWMA    0xa2 //*towerMoveAbsolute()
#define CMD_ID_TIMA    0xa3 //*tiltMoveAbsolute()
#define CMD_ID_TWPO    0xa4 //*getTowerPosition(), setTowerPosition()
#define CMD_ID_TIPO    0xa5 //*getTiltPosition(), setTiltPosition()
#define CMD_ID_TWCF    0xa6 //*getTowerConfig(), setTowerConfig()
#define CMD_ID_TICF    0xa7 //*getTiltConfig(), setTiltConfig()
#define CMD_ID_TWCS    0xa8 //*getTowerConfigSelected(), selectTowerConfig()
#define CMD_ID_TICS    0xa9 //*getTiltConfigSelected(), selectTiltConfig()
#define CMD_ID_ECHO    0xaa //*getEcho(), setEcho()

#define CMD_ID_RSEN    0xad //*getResinSensorEnabled(), setResinSensorEnabled()
#define CMD_ID_RSST    0xae //*getResinSensorState()
#define CMD_ID_RSME    0xaf //*resinSensorMeassure()
#define CMD_ID_VOLT    0xb0 //*getVoltages()
#define CMD_ID_PPWM    0xb1 //*getPledPwm(), setPledPwm()
#define CMD_ID_FPWM    0xb2 //*getFansPwm(), setFansPwm()
#define CMD_ID_FANE    0xb3 //*getFansError()
#define CMD_ID_WIDL    0xb4 //*waitIdle()
#define CMD_ID_BOOT    0xb5 //*startBootloader()
#define CMD_ID_TRAW    0xb6 //*getRawTemperatures()
#define CMD_ID_VRAW    0xb7 //*getRawVoltages()
#define CMD_ID_UPWM    0xb8 //*getUledPwm(), setUledPwm()
#define CMD_ID_PSPD    0xb9 //*getPledSpeed(), setPledSpeed()
#define CMD_ID_SGBC    0xba //*sgBuffCount
#define CMD_ID_SGBD    0xbb //*sgBuffData
#define CMD_ID_TWPH    0xbc //*getTowerPhase(), setTowerPhase()
#define CMD_ID_TIPH    0xbd //*getTiltPhase(), setTiltPhase()
#define CMD_ID_WDEL    0xbe //*waitDelay()
#define CMD_ID_PPST    0xbf //*powerPanicStatus()
#define CMD_ID_TWHC    0xc0 //*towerHomeCalibrate()
#define CMD_ID_TIHC    0xc1 //*tiltHomeCalibrate()
#define CMD_ID_TWST    0xc2 //*towerStep()
#define CMD_ID_TIST    0xc3 //*tiltStep()
#define CMD_ID_FMSK    0xc4 //*setFanCheckMask(), getFanCheckMask()
#define CMD_ID_GOSE    0xc5 //*goServiceMode()
#define CMD_ID_TISE    0xc6 //*tiltSeparate()
#define CMD_ID_TISC    0xc7 //tiltSgCompensation
#define CMD_ID_TWTF    0xc8 //tower tmc flags
#define CMD_ID_TITF    0xc9 //tilt tmc flags
#define CMD_ID_USTA    0xca //getUvLedStatistics(), setUvLedStatistics()
#define CMD_ID_TWGF    0xcb //tower sla_goto_fullstep()
#define CMD_ID_TIGF    0xcc //tilt sla_goto_fullstep()
#define CMD_ID_TMCC    0xcd //write/read TMC register
#define CMD_ID_ULCD    0xce // start/stop exposition display counter
#define CMD_ID__min    0xe0 // min service command ID
#define CMD_ID__POR    0xe1 // getPORT(), setPORT()
#define CMD_ID__DDR    0xe2 // getDDR(), setDDR()
#define CMD_ID__PIN    0xe3 // getPIN(), setPIN()

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern uint8_t sla_cmd_id;

extern int8_t sla_cmd_E_rst(void);
extern int8_t sla_cmd_E_tst(void);
extern int8_t sla_cmd_E_shdn(void);
extern int8_t sla_cmd_E_motr(void);
extern int8_t sla_cmd_E_twho(void);
extern int8_t sla_cmd_E_tiho(void);
extern int8_t sla_cmd_E_eecl(void);
extern int8_t sla_cmd_E_widl(void);
extern int8_t sla_cmd_E_boot(void);
extern int8_t sla_cmd_E_sgbd(void);
extern int8_t sla_cmd_E_twhc(void);
extern int8_t sla_cmd_E_tihc(void);
extern int8_t sla_cmd_E_gose(void);
extern int8_t sla_cmd_E_twgf(void);
extern int8_t sla_cmd_E_tigf(void);

extern int8_t sla_cmd_Q_ui8(void);
extern int8_t sla_cmd_Q_i8(void);
extern int8_t sla_cmd_Q_i16(void);
extern int8_t sla_cmd_Q_ui16(void);
extern int8_t sla_cmd_Q_ui8_x4(void);
extern int8_t sla_cmd_Q_i16_x4(void);
extern int8_t sla_cmd_Q_ui16_x4(void);
extern int8_t sla_cmd_Q_(void);
extern int8_t sla_cmd_Q_ver(void);
extern int8_t sla_cmd_Q_ser(void);
extern int8_t sla_cmd_Q_pun(void);
extern int8_t sla_cmd_Q_rev(void);
extern int8_t sla_cmd_Q_uled(void);
extern int8_t sla_cmd_Q_twpo(void);
extern int8_t sla_cmd_Q_tipo(void);
extern int8_t sla_cmd_Q_XXcf(void);
extern int8_t sla_cmd_Q_sgbd(void);
extern int8_t sla_cmd_Q_ppst(void);
extern int8_t sla_cmd_Q_tisc(void);
extern int8_t sla_cmd_Q_usta(void);
extern int8_t sla_cmd_Q_frpm(void);
extern int8_t sla_cmd_Q_eerx(void);

extern int8_t sla_cmd_E_ulcd(void);
extern int8_t sla_cmd_E_ui8(void);
extern int8_t sla_cmd_E_i8(void);
extern int8_t sla_cmd_E_ui16(void);
extern int8_t sla_cmd_E_i16(void);
extern int8_t sla_cmd_E_i32(void);
extern int8_t sla_cmd_E_beep(void);
extern int8_t sla_cmd_E_uled(void);
extern int8_t sla_cmd_E_eecl1(void);
extern int8_t sla_cmd_E_XXcf(void);
extern int8_t sla_cmd_E_frpm(void);
extern int8_t sla_cmd_E_twhc1(void);
extern int8_t sla_cmd_E_tihc1(void);
extern int8_t sla_cmd_E_ppst(void);
extern int8_t sla_cmd_E_tisc(void);
extern int8_t sla_cmd_E_usta(void);

#ifdef SLA_TISE
extern int8_t sla_cmd_E_tise(void);
#endif //SLA_TISE

extern int8_t sla_cmd_E__por_pin_ddr(void);
extern int8_t sla_cmd_E_frpm(void);
extern int8_t sla_cmd_Q_XXcf1(void);
extern int8_t sla_cmd_Q__por_pin_ddr(void);

#ifdef SLA_DBG_TMCC
extern int8_t sla_cmd_Q_tmcc(void);
#endif

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_CMD_SLA_H
