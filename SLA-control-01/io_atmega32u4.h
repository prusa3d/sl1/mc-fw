//io_atmega2560.h
#ifndef _IO_ATMEGA32U4
#define _IO_ATMEGA32U4

#define __PIN_P0   PIND
#define __PIN_P1   PIND
#define __PIN_P2   PIND
#define __PIN_P3   PIND
#define __PIN_P4   PIND
#define __PIN_P5   PINC
#define __PIN_P6   PIND
#define __PIN_P7   PINE
#define __PIN_P8   PINB
#define __PIN_P9   PINB
#define __PIN_P10  PINB
#define __PIN_P11  PINB
#define __PIN_P12  PIND
#define __PIN_P13  PINC
#define __PIN_P14  PINB
#define __PIN_P15  PINB
#define __PIN_P16  PINB
#define __PIN_P17  PINB
#define __PIN_P18  PINF
#define __PIN_P19  PINF
#define __PIN_P20  PINF
#define __PIN_P21  PINF
#define __PIN_P22  PINF
#define __PIN_P23  PINF
#define __PIN_P24  PIND
#define __PIN_P25  PIND
#define __PIN_P26  PINB
#define __PIN_P27  PINB
#define __PIN_P28  PINB
#define __PIN_P29  PIND
#define __PIN_P30  PIND

#define __PORT_P0   PORTD
#define __PORT_P1   PORTD
#define __PORT_P2   PORTD
#define __PORT_P3   PORTD
#define __PORT_P4   PORTD
#define __PORT_P5   PORTC
#define __PORT_P6   PORTD
#define __PORT_P7   PORTE
#define __PORT_P8   PORTB
#define __PORT_P9   PORTB
#define __PORT_P10  PORTB
#define __PORT_P11  PORTB
#define __PORT_P12  PORTD
#define __PORT_P13  PORTC
#define __PORT_P14  PORTB
#define __PORT_P15  PORTB
#define __PORT_P16  PORTB
#define __PORT_P17  PORTB
#define __PORT_P18  PORTF
#define __PORT_P19  PORTF
#define __PORT_P20  PORTF
#define __PORT_P21  PORTF
#define __PORT_P22  PORTF
#define __PORT_P23  PORTF
#define __PORT_P24  PORTD
#define __PORT_P25  PORTD
#define __PORT_P26  PORTB
#define __PORT_P27  PORTB
#define __PORT_P28  PORTB
#define __PORT_P29  PORTD
#define __PORT_P30  PORTD

#define __DDR_P0   DDRD
#define __DDR_P1   DDRD
#define __DDR_P2   DDRD
#define __DDR_P3   DDRD
#define __DDR_P4   DDRD
#define __DDR_P5   DDRC
#define __DDR_P6   DDRD
#define __DDR_P7   DDRE
#define __DDR_P8   DDRB
#define __DDR_P9   DDRB
#define __DDR_P10  DDRB
#define __DDR_P11  DDRB
#define __DDR_P12  DDRD
#define __DDR_P13  DDRC
#define __DDR_P14  DDRB
#define __DDR_P15  DDRB
#define __DDR_P16  DDRB
#define __DDR_P17  DDRB
#define __DDR_P18  DDRF
#define __DDR_P19  DDRF
#define __DDR_P20  DDRF
#define __DDR_P21  DDRF
#define __DDR_P22  DDRF
#define __DDR_P23  DDRF
#define __DDR_P24  DDRD
#define __DDR_P25  DDRD
#define __DDR_P26  DDRB
#define __DDR_P27  DDRB
#define __DDR_P28  DDRB
#define __DDR_P29  DDRD
#define __DDR_P30  DDRD

#define __BIT_P0   2 // D0 - PD2
#define __BIT_P1   3 // D1 - PD3
#define __BIT_P2   1 // D2 - PD1 - SDA
#define __BIT_P3   0 // D3 - PD0 - SCL
#define __BIT_P4   4 // D4 - PD4
#define __BIT_P5   6 // D5 - PC6
#define __BIT_P6   7 // D6 - PD7
#define __BIT_P7   6 // D7 - PE6
#define __BIT_P8   4 // D8 - PB4
#define __BIT_P9   5 // D9 - PB5
#define __BIT_P10  6 // D10 - PB6
#define __BIT_P11  7 // D11 - PB7
#define __BIT_P12  6 // D12 - PD6
#define __BIT_P13  7 // D13 - PC7
#define __BIT_P14  3 // D14 - MISO - PB3
#define __BIT_P15  1 // D15 - SCK - PB1
#define __BIT_P16  2 // D16 - MOSI - PB2
#define __BIT_P17  0 // D17 - SS - PB0
#define __BIT_P18  7 // D18 - A0 - PF7
#define __BIT_P19  6 // D19 - A1 - PF6
#define __BIT_P20  5 // D20 - A2 - PF5
#define __BIT_P21  4 // D21 - A3 - PF4
#define __BIT_P22  1 // D22 - A4 - PF1
#define __BIT_P23  0 // D23 - A5 - PF0
#define __BIT_P24  4 // D24 / D4 - A6 - PD4
#define __BIT_P25  7 // D25 / D6 - A7 - PD7
#define __BIT_P26  4 // D26 / D8 - A8 - PB4
#define __BIT_P27  5 // D27 / D9 - A9 - PB5
#define __BIT_P28  6 // D28 / D10 - A10 - PB6
#define __BIT_P29  6 // D29 / D12 - A11 - PD6
#define __BIT_P30  5 // D30 / TX Led - PD5 

#define __BIT(pin) __BIT_P##pin
#define __MSK(pin) (1 << __BIT(pin))

#define __PIN(pin) __PIN_P##pin
#define __PORT(pin) __PORT_P##pin
#define __DDR(pin) __DDR_P##pin

#define PIN(pin) __PIN(pin)
#define PORT(pin) __PORT(pin)
#define DDR(pin) __DDR(pin)

#define PIN_INP(pin) DDR(pin) &= ~__MSK(pin)
#define PIN_OUT(pin) DDR(pin) |= __MSK(pin)
#define PIN_CLR(pin) PORT(pin) &= ~__MSK(pin)
#define PIN_SET(pin) PORT(pin) |= __MSK(pin)
#define PIN_VAL(pin, val) if (val) PIN_SET(pin); else PIN_CLR(pin);
#define PIN_GET(pin) ((PIN(pin) & __MSK(pin))?1:0)
#define PIN_TOG(pin) PIN(pin) = __MSK(pin)


#define PIN_OCR1A  9 //PB5
#define PIN_OCR1B 10 //PB6
#define PIN_OCR1C 11 //PB7

#define PIN_OCR3A  5 //PC6
#define PIN_OCR3B -1 //NC
#define PIN_OCR3C -1 //NC


#endif //_IO_ATMEGA32U4
