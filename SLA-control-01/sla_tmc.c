/**
 * functions for trinamic stepper drivers
 */
#include "sla_tmc.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "sla.h"
#include "tmc2130.h"
#include "sla_spi.h"

int16_t sla_tmc_tstep1000[TMC2130_NUMAXES];
uint8_t sla_tmc_end_aux = 0;

void sla_tmc_setup_pins(void)
{
	DDRB |= 0x20;   //STEP X - output
	DDRB |= 0x40;   //STEP Y - output
	PORTB &= ~0x20; //STEP X = 0
	PORTB &= ~0x40; //STEP Y = 0
	DDRD &= ~0x40;  //DIAG X - input
	DDRD &= ~0x80;  //DIAG Y - input
	DDRD |= 0x02;   //CS X - output
	DDRD |= 0x10;   //CS Y - output
	PORTD |= 0x02;  //CS X = 1
	PORTD |= 0x10;  //CS Y = 1
}

uint8_t sla_tmc_get_ena(void)
{
	return (((sla_gpio[0] & 0x02) >> 1) | ((sla_gpio[0] & 0x08) >> 2)) ^ 0x03;
}

void sla_tmc_set_ena(uint8_t mask)
{
	mask ^= 0x03;
	sla_gpio[0] = ((sla_gpio[0] & ~0x0a) | ((mask & 0x01) << 1) | ((mask & 0x02) << 2));
	if (mask & 0x02)
		sla_tiho_state = -1;
	if (mask & 0x01)
	    sla_twho_state = -1;
	sla_spi_sync(SLA_SPI_REQ_GPIO_OUT_A);
}

uint8_t sla_tmc_get_dir(void)
{
	return (sla_gpio[0] & 0x01) | ((sla_gpio[0] & 0x04) >> 1);
}

void sla_tmc_set_dir(uint8_t mask)
{
	sla_gpio[0] = (sla_gpio[0] & ~0x05) | (mask & 0x01) | ((mask & 0x02) << 1);
	sla_spi_sync(SLA_SPI_REQ_GPIO_OUT_A);
}

void sla_tmc_cs_low(uint8_t axis)
{
	switch (axis)
	{
	case 0:
		PORTD &= ~0x02;  //CS X = 0
		break;
	case 1:
		PORTD &= ~0x10;  //CS Y = 0
		break;
	}
}

void sla_tmc_cs_high(uint8_t axis)
{
	switch (axis)
	{
	case 0:
		PORTD |= 0x02;  //CS X = 1
		break;
	case 1:
		PORTD |= 0x10;  //CS Y = 1
		break;
	}
}

void sla_tmc_do_step(uint8_t mask)
{
	if (mask & 1) PORTB |= 0x20;
	if (mask & 2) PORTB |= 0x40;
	PORTB &= ~0x20;
	PORTB &= ~0x40;
}

void sla_tmc_set_step(uint8_t axis, uint8_t val)
{
	switch (axis)
	{
	case 0:
		if (val) PORTB |= 0x20;
		else PORTB &= ~0x20;
		break;
	case 1:
		if (val) PORTB |= 0x40;
		else PORTB &= ~0x40;
		break;
	}
}

uint8_t sla_tmc_get_diag(void)
{
#ifndef _SIMULATOR
	return (PIND >> 6) | sla_tmc_end_aux;
#else
	return (((!(ST4_GET_DIR() & 1) && st4_axis[0].pos > 120292) || (ST4_GET_DIR() & 1 && st4_axis[0].pos < -120292) && (st4_end & 0x01))) | sla_tmc_end_aux;
#endif //_SIMULATOR
}

// check if ioin match desired value, wait 500us before sampling ioin
// bit0 is stp signal, bit1 is dir signal and bit4 is enable signal
// returns error bitmask (bit is set means signal does not match)
uint8_t sla_tmc_check_signals(uint8_t axis, uint8_t val)
{
	uint8_t ioin;
	_delay_us(500);                    // wait 500us
	cli();                             // disable interrupts
	ioin = tmc2130_read_ioin(axis);    // sample ioin
	sei();                             // enable interrupts
	return (ioin ^ val) & SLA_TMC_SIG_ALL;
}

// test all tmc signals - toggle and sample ioin
// step forward and then back with disabled motors (keep current phase)
// then enable motors for ~1ms and restore original state
uint8_t sla_tmc_test_signals(uint8_t axis)
{
	uint8_t err = 0;                     // mask for errors - step
	uint8_t mask = (1 << axis);          // mask for desired axis
	uint8_t tmp_ena = sla_tmc_get_ena(); // save current state of 'enable' signals
	uint8_t tmp_dir = sla_tmc_get_dir(); // save current state of 'dir' signals
	sla_tmc_set_ena(tmp_ena & (~mask));  // ena=1 (disable axis, ena is inverted)
	//sla_tmc_set_step(axis, 0);         // stp=0 (it is not necessary set to 0, because 0 is default)
	sla_tmc_set_dir(tmp_dir & (~mask));  // dir=0
	err |= sla_tmc_check_signals(axis, 0x10); // sample ioin (signal states should be: ena=1, dir=0, stp=0)
	sla_tmc_set_step(axis, 1);           // stp=1
	err |= sla_tmc_check_signals(axis, 0x11); // sample ioin (signal states should be: ena=1, dir=0, stp=1)
	sla_tmc_set_step(axis, 0);           // stp=0
	sla_tmc_set_dir(tmp_dir | mask);     // dir=1
	err |= sla_tmc_check_signals(axis, 0x12); // sample ioin (signal states should be: ena=1, dir=1, stp=0)
	sla_tmc_do_step(mask);               // do step (because we want return to starting phase)
	sla_tmc_set_ena(tmp_ena | mask);     // ena=0 (enable axis)
	err |= sla_tmc_check_signals(axis, 0x02); // sample ioin (signal states should be: ena=0, dir=1, stp=0)
	sla_tmc_set_dir(tmp_dir);            // restore original state of 'dir' signal
	sla_tmc_set_ena(tmp_ena);            // restore original state of 'enable' signal
	return err;
}

void sla_tmc_wait_standstill(uint8_t axis)
{
#ifdef _SIMULATOR
	return;
#endif //_SIMULATOR
	while (1)
	{
		if (axis == 0)
		{
			sla_spi_sync(SLA_SPI_REQ_TMC0_RD_DRV_STATUS);
			if (sla_spi_tmc_drv[0] & 0x80000000) break;
		}
		else if (axis == 1)
		{
			sla_spi_sync(SLA_SPI_REQ_TMC1_RD_DRV_STATUS);
			if (sla_spi_tmc_drv[1] & 0x80000000) break;
		}
	}
}

int16_t sla_tmc_calc_steps_to_phase(uint16_t actual_phase, uint16_t target_phase, uint8_t microstep_resolution)
{
	return ((int16_t)(target_phase - actual_phase) / (microstep_resolution * 2));
}

uint16_t sla_tmc_measure_tstep1000(uint8_t axis)
{
	uint8_t i;
	uint8_t mask = (1 << axis);          // mask for desired axis
	uint32_t tstep_sum = 0;				 // measured tstep value
	cli();
	for (i = 0; i < 16; i++)
	{
		sla_tmc_do_step(mask);           // do step
		_delay_us(999);                  //
		sla_tmc_do_step(mask);           // do step
		tstep_sum += tmc2130_read_tstep(axis);//
		_delay_us(999);                  //
	}
	sei();
	return ((uint16_t)tstep_sum >> 4);
}

int16_t sla_tmc_calc_cents(float f0, float f1)
{
	int16_t cents = 0;
	if (f1 == f0)
		return 0;
	else if (f1 > f0)
	{
		while (f0 < f1)
		{
			f0 *= 1.0005777895065548592967925757932F;
			cents++;
		}
	}
	else
	{
		while (f1 < f0)
		{
			f1 *= 1.0005777895065548592967925757932F;
			cents--;
		}
	}
	return cents;
}
