/*
 * resin sensor functions
 */
#include "sla_rsens.h"
#include "avr/io.h"
#include "sla.h"
#include "sla_spi.h"

uint8_t sla_rsens = 0;

void sla_rsens_enable(void)
{
	sla_gpio[1] |= 0x04;
	sla_spi_sync(SLA_SPI_REQ_GPIO_OUT_B);
	sla_rsens |= 0x01;
}

void sla_rsens_disable(void)
{
	sla_gpio[1] &= ~0x04;
	sla_spi_sync(SLA_SPI_REQ_GPIO_OUT_B);
	sla_rsens = 0;
}

uint8_t sla_rsens_get_state(void)
{
	return (PINE & 0x04)?1:0;
}

int8_t sla_rsens_meassure(uint32_t steps)
{
	sla_rsens |= 0x02;
	return st4_mor(0, -((int32_t)steps));
}

void sla_rsens_cycle(void)
{
	if (sla_rsens == 0x03) //resin sensor enabled and messurement is running
		if (sla_rsens_get_state()) //resin sensor is active
		{
			sla_rsens &= ~0x02; //stop meassurement
			st4_msk &= ~0x01; //stop tower
		}
}
