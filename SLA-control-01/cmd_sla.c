/*
 * parser of the commands.
 */
#include "cmd_sla.h"
#include <string.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/boot.h>
#include <avr/wdt.h>
#include "sys.h"
#include "tmc2130.h"
#include "st4.h"
#include "sla.h"
#include "snd.h"
#include "cmd.h"

#include "sla_sign.h"
#include "sla_diag.h"
#include "sla_cfg.h"
#include "sla_adc.h"
#include "sla_tmc.h"
#include "sla_rsens.h"
#include "sla_serv.h"
#include "sla_spi.h"

uint8_t sla_cmd_id = 0;
var_num_t sla_cmd_val[4];
char* sla_cmd_pstr;
int8_t sla_cmd_ret;

int8_t cmd_parse_mod_msk(char* pstr, uint16_t* pmod_msk)
{
	int8_t ret = 0;
	char c;
	do
	{
		c = *pstr++;
		switch (c)
		{
		case '!':
		case '?':
			if (ret == 0) *pmod_msk = MOD_MSK_0;
			return ret;
		default:
			return CMD_ER_SYN;
		}
		ret++;
	} while (ret <= 3);
	return CMD_ER_SYN;
}

const uint8_t PROGMEM cmd_sla_3[] = {
#ifndef SLA_2ND_BOARD_TEST2
'd','i','r',CMD_ID_DIR,
'e','n','a',CMD_ID_ENA,
'e','n','d',CMD_ID_END,
'l','e','d',CMD_ID_LED,
'l','o','g',CMD_ID_LOG,
'm','o','t',CMD_ID_MOT,
'r','s','t',CMD_ID_RST,
's','e','r',CMD_ID_SER,
't','s','t',CMD_ID_TST,
'v','e','r',CMD_ID_VER,
'r','e','v',CMD_ID_REV,
'p','u','n',CMD_ID_PUN,
#endif //SLA_2ND_BOARD_TEST
};

const uint8_t PROGMEM cmd_sla_4[] = {
#ifndef SLA_2ND_BOARD_TEST2
'b','e','e','p',CMD_ID_BEEP,
'b','o','o','t',CMD_ID_BOOT,
'e','e','c','l',CMD_ID_EECL,
'e','e','r','x',CMD_ID_EERX,
'e','e','w','x',CMD_ID_EEWX,
'e','c','h','o',CMD_ID_ECHO,
'f','a','n','e',CMD_ID_FANE,
'f','a','n','s',CMD_ID_FANS,
'f','p','w','m',CMD_ID_FPWM,
'f','r','p','m',CMD_ID_FRPM,
'm','o','t','r',CMD_ID_MOTR,
'p','l','e','d',CMD_ID_PLED,
'p','p','s','t',CMD_ID_PPST,
'p','p','w','m',CMD_ID_PPWM,
'p','s','p','d',CMD_ID_PSPD,
'r','s','e','n',CMD_ID_RSEN,
'r','s','m','e',CMD_ID_RSME,
'r','s','s','t',CMD_ID_RSST,
's','g','b','c',CMD_ID_SGBC,
's','g','b','d',CMD_ID_SGBD,
's','h','d','n',CMD_ID_SHDN,
't','e','m','p',CMD_ID_TEMP,
't','i','c','f',CMD_ID_TICF,
't','i','c','s',CMD_ID_TICS,
't','i','c','u',CMD_ID_TICU,
't','i','h','c',CMD_ID_TIHC,
't','i','h','o',CMD_ID_TIHO,
't','i','m','a',CMD_ID_TIMA,
't','i','p','h',CMD_ID_TIPH,
't','i','p','o',CMD_ID_TIPO,
't','i','s','g',CMD_ID_TISG,
't','i','s','t',CMD_ID_TIST,
't','r','a','w',CMD_ID_TRAW,
't','w','c','f',CMD_ID_TWCF,
't','w','c','s',CMD_ID_TWCS,
't','w','c','u',CMD_ID_TWCU,
't','w','d','n',CMD_ID_TWDN,
't','w','h','c',CMD_ID_TWHC,
't','w','h','o',CMD_ID_TWHO,
't','w','m','a',CMD_ID_TWMA,
't','w','p','h',CMD_ID_TWPH,
't','w','p','o',CMD_ID_TWPO,
't','w','s','g',CMD_ID_TWSG,
't','w','s','t',CMD_ID_TWST,
't','w','u','p',CMD_ID_TWUP,
'u','l','e','d',CMD_ID_ULED,
'u','p','w','m',CMD_ID_UPWM,
'v','o','l','t',CMD_ID_VOLT,
'v','r','a','w',CMD_ID_VRAW,
'w','d','e','l',CMD_ID_WDEL,
'w','i','d','l',CMD_ID_WIDL,
'f','m','s','k',CMD_ID_FMSK,
'g','o','s','e',CMD_ID_GOSE,
't','w','t','f',CMD_ID_TWTF,
't','i','t','f',CMD_ID_TITF,
#ifdef SLA_TISE
't','i','s','e',CMD_ID_TISE,
#endif //SLA_TISE
't','i','s','c',CMD_ID_TISC,
'u','s','t','a',CMD_ID_USTA,
'u','l','c','d',CMD_ID_ULCD,
't','w','g','f',CMD_ID_TWGF,
't','i','g','f',CMD_ID_TIGF,
'_','p','o','r',CMD_ID__POR,
'_','d','d','r',CMD_ID__DDR,
'_','p','i','n',CMD_ID__PIN,
't','m','c','c',CMD_ID_TMCC,
#else //SLA_2ND_BOARD_TEST
'u','l','e','d',CMD_ID_ULED,
'u','p','w','m',CMD_ID_UPWM,
#endif //SLA_2ND_BOARD_TEST
};

int8_t cmd_parse_cmd_id(char* pstr, uint8_t* pcmd_id)
{
	//empty command (0char, no args)
	if (pstr[0] == 0)
	{
		*pcmd_id = CMD_ID_;
		return 0;
	}
	else if ((pstr[3] == 0) || (pstr[3] == ' '))
	{
		if ((*pcmd_id = cmd_parse_tab(pstr, (uint8_t*)cmd_sla_3, 3, sizeof(cmd_sla_3)/4)) != CMD_ID_unk)
			return 3;
	}
	else if ((pstr[4] == 0) || (pstr[4] == ' '))
	{
		if ((*pcmd_id = cmd_parse_tab(pstr, (uint8_t*)cmd_sla_4, 4, sizeof(cmd_sla_4)/5)) != CMD_ID_unk)
			return 4;
	}
	return CMD_ER_SYN;
}

int8_t sla_cmd_print_axis_config(sla_axis_config_t* pcfg)
{
	int8_t ret = 0;
	ret += cmd_print_ui16(pcfg->st4.sr0);
	ret += cmd_print_ui16(pcfg->st4.srm);
	ret += cmd_print_ui16(pcfg->st4.acc);
	ret += cmd_print_ui16(pcfg->st4.dec);
	ret += cmd_print_ui8(pcfg->tmc.cur);
	ret += cmd_print_i8(pcfg->tmc.sgt);
	ret += cmd_print_ui16(pcfg->tmc.cst);
	return ret;
}

int8_t sla_cmd_scan_axis_config(sla_axis_config_t* pcfg, char* pstr)
{
	char* p = pstr;
	int8_t ret;
	if ((ret = cmd_scan_ui16(p, &(pcfg->st4.sr0))) < 0) return ret;
	if (*(p += ret)) if ((ret = cmd_scan_ui16(p, &(pcfg->st4.srm))) < 0) return ret;
	if (*(p += ret)) if ((ret = cmd_scan_ui16(p, &(pcfg->st4.acc))) < 0) return ret;
	if (*(p += ret)) if ((ret = cmd_scan_ui16(p, &(pcfg->st4.dec))) < 0) return ret;
	if (*(p += ret)) if ((ret = cmd_scan_ui8(p, &(pcfg->tmc.cur))) < 0) return ret;
	if (*(p += ret)) if ((ret = cmd_scan_ui8(p, &(pcfg->tmc.sgt))) < 0) return ret;	// TODO this should be actually signed
	if (*(p += ret)) if ((ret = cmd_scan_ui16(p, &(pcfg->tmc.cst))) < 0) return ret;
	return (p - pstr);
}

const cmd_entry_t cmd_sla_ewo[] PROGMEM =
{
	{ CMD_ID_RST, sla_cmd_E_rst },
	{ CMD_ID_TST, sla_cmd_E_tst },
	{ CMD_ID_SHDN, sla_cmd_E_shdn },
	{ CMD_ID_MOTR, sla_cmd_E_motr },
	{ CMD_ID_TWHO, sla_cmd_E_twho },
	{ CMD_ID_TIHO, sla_cmd_E_tiho },
	{ CMD_ID_EECL, sla_cmd_E_eecl },
	{ CMD_ID_WIDL, sla_cmd_E_widl },
	{ CMD_ID_BOOT, sla_cmd_E_boot },
	{ CMD_ID_SGBD, sla_cmd_E_sgbd },
	{ CMD_ID_TWHC, sla_cmd_E_twhc },
	{ CMD_ID_TIHC, sla_cmd_E_tihc },
	{ CMD_ID_GOSE, sla_cmd_E_gose },
};

const cmd_entry_t cmd_sla_qwo[] PROGMEM =
{
	{ CMD_ID_, sla_cmd_Q_ },
	{ CMD_ID_RST, sla_cmd_Q_ui8 },
	{ CMD_ID_VER, sla_cmd_Q_ver },
	{ CMD_ID_SER, sla_cmd_Q_ser },
	{ CMD_ID_PUN, sla_cmd_Q_pun },
	{ CMD_ID_REV, sla_cmd_Q_rev },
	{ CMD_ID_ENA, sla_cmd_Q_ui8 },
	{ CMD_ID_LED, sla_cmd_Q_ui8 },
	{ CMD_ID_DIR, sla_cmd_Q_ui8 },
	{ CMD_ID_MOT, sla_cmd_Q_ui8 },
	{ CMD_ID_END, sla_cmd_Q_ui8 },
	{ CMD_ID_LOG, sla_cmd_Q_ui8 },
	{ CMD_ID_PLED, sla_cmd_Q_ui8 },
	{ CMD_ID_ULED, sla_cmd_Q_uled },
	{ CMD_ID_FANS, sla_cmd_Q_ui8 },
	{ CMD_ID_TWCU, sla_cmd_Q_ui8 },
	{ CMD_ID_TICU, sla_cmd_Q_ui8 },
	{ CMD_ID_TWHO, sla_cmd_Q_i8 },
	{ CMD_ID_TIHO, sla_cmd_Q_i8 },
	{ CMD_ID_EERX, sla_cmd_Q_eerx },
#ifdef SLA_TISE
	{ CMD_ID_TISE, sla_cmd_Q_i8 },
#endif //SLA_TISE
	{ CMD_ID_TISC, sla_cmd_Q_tisc },
	{ CMD_ID_TWSG, sla_cmd_Q_ui8 },
	{ CMD_ID_TISG, sla_cmd_Q_ui8 },
	{ CMD_ID_FRPM, sla_cmd_Q_frpm},
	{ CMD_ID_TEMP, sla_cmd_Q_i16_x4 },
	{ CMD_ID_VOLT, sla_cmd_Q_i16_x4 },
	{ CMD_ID_TRAW, sla_cmd_Q_ui16_x4 },
	{ CMD_ID_VRAW, sla_cmd_Q_ui16_x4 },
	{ CMD_ID_PPWM, sla_cmd_Q_ui8 },
	{ CMD_ID_PSPD, sla_cmd_Q_ui8 },
	{ CMD_ID_UPWM, sla_cmd_Q_ui8 },
	{ CMD_ID_FANE, sla_cmd_Q_ui8 },
	{ CMD_ID_TWPO, sla_cmd_Q_twpo },
	{ CMD_ID_TIPO, sla_cmd_Q_tipo },
	{ CMD_ID_TWCF, sla_cmd_Q_XXcf },
	{ CMD_ID_TICF, sla_cmd_Q_XXcf },
	{ CMD_ID_TWCS, sla_cmd_Q_i8 },
	{ CMD_ID_TICS, sla_cmd_Q_i8 },
	{ CMD_ID_ECHO, sla_cmd_Q_i8 },
	{ CMD_ID_RSEN, sla_cmd_Q_ui8 },
	{ CMD_ID_RSST, sla_cmd_Q_ui8 },
	{ CMD_ID_SGBC, sla_cmd_Q_ui8 },
	{ CMD_ID_SGBD, sla_cmd_Q_sgbd },
	{ CMD_ID_TWPH, sla_cmd_Q_ui16 },
	{ CMD_ID_TIPH, sla_cmd_Q_ui16 },
	{ CMD_ID_PPST, sla_cmd_Q_ppst },
	{ CMD_ID_TWHC, sla_cmd_Q_i16 },
	{ CMD_ID_TIHC, sla_cmd_Q_i16 },
	{ CMD_ID_FMSK, sla_cmd_Q_i8 },
	{ CMD_ID_TWTF, sla_cmd_Q_ui8 },
	{ CMD_ID_TITF, sla_cmd_Q_ui8 },
	{ CMD_ID_USTA, sla_cmd_Q_usta },
};

const cmd_entry_t cmd_sla_ewi[] PROGMEM =
{
	{ CMD_ID_ENA, sla_cmd_E_ui8 },
	{ CMD_ID_LED, sla_cmd_E_ui8 },
	{ CMD_ID_DIR, sla_cmd_E_ui8 },
	{ CMD_ID_MOT, sla_cmd_E_ui8 },
	{ CMD_ID_END, sla_cmd_E_ui8 },
	{ CMD_ID_LOG, sla_cmd_E_ui8 },
	{ CMD_ID_SHDN, sla_cmd_E_ui8 },
	{ CMD_ID_BEEP, sla_cmd_E_beep },
	{ CMD_ID_PLED, sla_cmd_E_ui8 },
	{ CMD_ID_ULED, sla_cmd_E_uled },
	{ CMD_ID_TWUP, sla_cmd_E_ui16 },
	{ CMD_ID_TWDN, sla_cmd_E_ui16 },
	{ CMD_ID_FANS, sla_cmd_E_ui8 },
	{ CMD_ID_TWCU, sla_cmd_E_ui8 },
	{ CMD_ID_TICU, sla_cmd_E_ui8 },
	{ CMD_ID_TWSG, sla_cmd_E_i8 },
	{ CMD_ID_TISG, sla_cmd_E_i8 },
	{ CMD_ID_TWMA, sla_cmd_E_i32 },
	{ CMD_ID_TIMA, sla_cmd_E_i16 },
	{ CMD_ID_TWPO, sla_cmd_E_i32 },
	{ CMD_ID_TIPO, sla_cmd_E_i16 },
	{ CMD_ID_TWCF, sla_cmd_E_XXcf },
	{ CMD_ID_TICF, sla_cmd_E_XXcf },
	{ CMD_ID_TWCS, sla_cmd_E_i8 },
	{ CMD_ID_TICS, sla_cmd_E_i8 },
	{ CMD_ID_ECHO, sla_cmd_E_ui8 },
	{ CMD_ID_PPWM, sla_cmd_E_ui8 },
	{ CMD_ID_PSPD, sla_cmd_E_ui8 },
	{ CMD_ID_UPWM, sla_cmd_E_ui8 },
	{ CMD_ID_FRPM, sla_cmd_E_frpm },
	{ CMD_ID_RSEN, sla_cmd_E_ui8 },
	{ CMD_ID_RSME, sla_cmd_E_i32 },
	{ CMD_ID_WDEL, sla_cmd_E_ui16 },
	{ CMD_ID_TWPH, sla_cmd_E_ui16 },
	{ CMD_ID_TIPH, sla_cmd_E_ui16 },
	{ CMD_ID_TWST, sla_cmd_E_i8 },
	{ CMD_ID_TIST, sla_cmd_E_i8 },
	{ CMD_ID_TWHC, sla_cmd_E_twhc1 },
	{ CMD_ID_TIHC, sla_cmd_E_tihc1 },
	{ CMD_ID_FMSK, sla_cmd_E_ui8 },
	{ CMD_ID_PPST, sla_cmd_E_ppst },
#ifdef SLA_TISE
	{ CMD_ID_TISE, sla_cmd_E_tise },
#endif //SLA_TISE
	{ CMD_ID_TISC, sla_cmd_E_tisc },
	{ CMD_ID__POR, sla_cmd_E__por_pin_ddr },
	{ CMD_ID__PIN, sla_cmd_E__por_pin_ddr },
	{ CMD_ID__DDR, sla_cmd_E__por_pin_ddr },
	{ CMD_ID_USTA, sla_cmd_E_usta },
	{ CMD_ID_TWGF, sla_cmd_E_twgf },
	{ CMD_ID_TIGF, sla_cmd_E_tigf },
	{ CMD_ID_ULCD, sla_cmd_E_ulcd },
};

const cmd_entry_t cmd_sla_qwi[] PROGMEM =
{
	{ CMD_ID_TWCF, sla_cmd_Q_XXcf1 },
	{ CMD_ID_TICF, sla_cmd_Q_XXcf1 },
	{ CMD_ID__POR, sla_cmd_Q__por_pin_ddr },
	{ CMD_ID__PIN, sla_cmd_Q__por_pin_ddr },
	{ CMD_ID__DDR, sla_cmd_Q__por_pin_ddr },
#ifdef SLA_DBG_TMCC
	{ CMD_ID_TMCC, sla_cmd_Q_tmcc },
#endif
};

int8_t cmd_do_mod_wout_args(uint8_t mod_id, char pref, uint8_t cmd_id)
{
	sla_cmd_id = cmd_id;
	if (mod_id == MOD_ID_0)
	{
		if (pref == '!')
			return cmd_do(cmd_id, cmd_sla_ewo, (sizeof(cmd_sla_ewo) / sizeof(cmd_entry_t)));
		else if (pref == '?')
			return cmd_do(cmd_id, cmd_sla_qwo, (sizeof(cmd_sla_qwo) / sizeof(cmd_entry_t)));
	}
	return CMD_ER_SYN;
}

int8_t cmd_do_mod_with_args(uint8_t mod_id, char pref, uint8_t cmd_id, char* pstr)
{
	sla_cmd_id = cmd_id;
	if (mod_id == MOD_ID_0)
	{
		sla_cmd_pstr = pstr;
		if (pref == '!')
			return cmd_do(cmd_id, cmd_sla_ewi, (sizeof(cmd_sla_ewi) / sizeof(cmd_entry_t)));
		else if (pref == '?')
			return cmd_do(cmd_id, cmd_sla_qwi, (sizeof(cmd_sla_qwi) / sizeof(cmd_entry_t)));
	}
	return CMD_ER_SYN;
}

int8_t cmd_do_wout_args(uint16_t mod_msk, char pref, uint8_t cmd_id)
{
	int8_t ret = CMD_ER_SYN;
	if ((sla_mode == SLA_MODE_ERRO) && (
		(cmd_id != CMD_ID_) &&
		(cmd_id != CMD_ID_RST)
		))
		return CMD_ER_ONP; // commands with args not accepted in error mode
	if (mod_msk == MOD_MSK_0)
		return cmd_do_mod_wout_args(MOD_ID_0, pref, cmd_id);
	return ret;
}

int8_t cmd_do_with_args(uint16_t mod_msk, char pref, uint8_t cmd_id, char* pstr)
{
	if (sla_mode == SLA_MODE_ERRO)
		return CMD_ER_ONP; // commands with args not accepted in error mode
	if ((cmd_id >= CMD_ID__min) && (sla_mode != SLA_MODE_SERV))
		return CMD_ER_ONP; // not in service mode
	if (mod_msk == MOD_MSK_0)
		return cmd_do_mod_with_args(MOD_ID_0, pref, cmd_id, pstr);
	return CMD_OK;
}



// SET WITHOUT PARAMETER - cmd_sla_ewo
int8_t sla_cmd_E_rst(void)
{
	sys_reset();
	return CMD_OK;
}

int8_t sla_cmd_E_tst(void)
{
	cli();
	cmd_err_printf_P(PSTR("#spi0=%x drv0=%lx gst0=%x gcf0=%lx\n"), tmc2130_spi_status[0], sla_spi_tmc_drv[0], (int)tmc2130_read_gstat(0), tmc2130_read_gconf(0));
	cmd_err_printf_P(PSTR("#spi1=%x drv1=%lx gst1=%x gcf1=%lx\n"), tmc2130_spi_status[1], sla_spi_tmc_drv[1], (int)tmc2130_read_gstat(1), tmc2130_read_gconf(1));
	sei();
	return CMD_OK;
}

int8_t sla_cmd_E_shdn(void)
{
	sla_shdn(0);
	return CMD_OK;
}

int8_t sla_cmd_E_motr(void)
{
	sla_motr();
	return CMD_OK;
}

int8_t sla_cmd_E_twho(void)
{
	return sla_twho();
}

int8_t sla_cmd_E_tiho(void)
{
	return sla_tiho();
}

int8_t sla_cmd_E_eecl(void)
{
	eeprom_erase(sla_cfg_addr(SLA_CFG_TWCFGN), sla_cfg_addr(SLA_CFG_END) - sla_cfg_addr(SLA_CFG_TWCFGN)); //erase only selected region in EEPROM
	return CMD_OK;
}

int8_t sla_cmd_E_widl(void)
{
	while ((st4_msk & 0x03) || (sla_twho_state > 0) || (sla_tiho_state > 0))
	{
		sla_cycle();
	}
	return CMD_OK;
}

int8_t sla_cmd_E_boot(void)
{
	sys_bootloader();
	return CMD_OK;
}

int8_t sla_cmd_E_sgbd(void)
{
	sla_sgbufc = 0;
	return CMD_OK;
}

int8_t sla_cmd_E_twhc(void)
{
	return sla_twhc();
}

int8_t sla_cmd_E_tihc(void)
{
	return sla_tihc();
}

int8_t sla_cmd_E_gose(void)
{
	sla_set_mode(SLA_MODE_SERV);
	return CMD_OK;
}



// GET WITHOUT PARAMETER - cmd_sla_qwo
void* sla_cmd_Q_ptr(void)
{
	switch (sla_cmd_id)
	{
	//ui8
	case CMD_ID_LED:  return &sla_leds;
	case CMD_ID_LOG:  return &sla_log;
	case CMD_ID_PLED: return &sla_pled;
	case CMD_ID_PPWM: return &sla_pled_pwm;
	case CMD_ID_PSPD: return &sla_pled_spd;
	case CMD_ID_UPWM: return &sla_uled.pwm;
	case CMD_ID_FANE: return &sla_fans.fane;
	case CMD_ID_SGBC: return &sla_sgbufc;
	//i8
	case CMD_ID_TWHO: return &sla_twho_state;
	case CMD_ID_TIHO: return &sla_tiho_state;
#ifdef SLA_TISE
	case CMD_ID_TISE: return &sla_tise_state;
#endif //SLA_TISE
	case CMD_ID_TWCS: return &sla_twcs_index;
	case CMD_ID_TICS: return &sla_tics_index;
	case CMD_ID_ECHO: return &cmd_echo;
	case CMD_ID_FMSK: return &sla_fans.fmsk;
	//i16
	case CMD_ID_TWHC: return &sla_twho_phase;
	case CMD_ID_TIHC: return &sla_tiho_phase;
	//ui16 x4
	case CMD_ID_VRAW: return sla_volt_raw;
	case CMD_ID_TRAW: return sla_temp_raw;
	//i16 x4
	case CMD_ID_VOLT: return sla_volt;
	case CMD_ID_TEMP: return sla_temp;
	}
	return 0;
}

int8_t sla_cmd_Q_ui8(void)
{
	uint8_t val = 0;
	uint8_t* ptr = sla_cmd_Q_ptr();
	if (ptr)
		val = *ptr;
	else
		switch (sla_cmd_id)
		{
		case CMD_ID_RST: val = sys_mcusr; sys_mcusr = 0; break;
		case CMD_ID_ENA: val = sla_tmc_get_ena(); break;
		case CMD_ID_DIR: val = sla_tmc_get_dir(); break;
		case CMD_ID_MOT: val = st4_msk & 0x0f; break;
		case CMD_ID_END: val = st4_end & 0x0f; break;
		case CMD_ID_TWCU: val = tmc2130_get_cur(0); break;
		case CMD_ID_TICU: val = tmc2130_get_cur(1); break;
		case CMD_ID_TWSG: val = tmc2130_get_sgt(0); break;
		case CMD_ID_TISG: val = tmc2130_get_sgt(1); break;
		case CMD_ID_RSEN: val = sla_rsens & 0x01; break;
		case CMD_ID_RSST: val = sla_rsens_get_state(); break;
		case CMD_ID_TWTF: val = sla_get_tmc_flags(0); break;
		case CMD_ID_TITF: val = sla_get_tmc_flags(1); break;
		case CMD_ID_FANS: val = sla_spwm.enable & 0x07; break;	// return only flags of fans. Ignore rest softPWM devices
		}
#ifdef SLA_2ND_BOARD_TEST3
	if (sla_cmd_id == CMD_ID_UPWM)
		return CMD_OK;
#endif //SLA_2ND_BOARD_TEST3

	cmd_print_ui8(val);
	return CMD_OK;
}

int8_t sla_cmd_Q_i8(void)
{
	int8_t val = 0;
	int8_t* ptr = sla_cmd_Q_ptr();
	if (ptr)
		val = *ptr;
	cmd_print_i8(val);
	return CMD_OK;
}

int8_t sla_cmd_Q_i16(void)
{
	int16_t val = 0;
	int16_t* ptr = sla_cmd_Q_ptr();
	if (ptr)
		val = *ptr;
	cmd_print_i16(val);
	return CMD_OK;
}

int8_t sla_cmd_Q_ui16(void)
{
	uint16_t val = 0;
	uint16_t* ptr = sla_cmd_Q_ptr();
	if (ptr)
		val = *ptr;
	else
		switch (sla_cmd_id)
		{
		case CMD_ID_TWPH: val = sla_get_phase(0); break;
		case CMD_ID_TIPH: val = sla_get_phase(1); break;
		}
	cmd_print_ui16(val);
	return CMD_OK;
}

int8_t sla_cmd_Q_ui8_x4(void)
{
	uint8_t i;
	uint8_t* ptr = sla_cmd_Q_ptr();
	if (ptr)
		for (i = 0; i < 4; i++)
			cmd_print_ui8(ptr[i]);
	return CMD_OK;
}

int8_t sla_cmd_Q_i16_x4(void)
{
	uint8_t i;
	int16_t* ptr = sla_cmd_Q_ptr();
	if (ptr)
		for (i = 0; i < 4; i++)
			cmd_print_i16(ptr[i]);
	return CMD_OK;
}

int8_t sla_cmd_Q_ui16_x4(void)
{
	uint8_t i;
	uint16_t* ptr = sla_cmd_Q_ptr();
	if (ptr)
		for (i = 0; i < 4; i++)
			cmd_print_ui16(ptr[i]);
	return CMD_OK;
}

int8_t sla_cmd_Q_usta(void)
{
	cmd_print_ui32(sla_uled.uvled_counter); //print UV LED counter [s]
	cmd_print_ui32(sla_uled.display_counter); //print exposure display counter [s]
	return CMD_OK;
}

int8_t sla_cmd_Q_frpm(void)
{
	uint16_t val = 0;
	for(uint8_t i = 0; i < SLA_FAN_NUM; i++)
	{
		if (sla_fan[i].rpm != 0 && sla_spwm.enable & (1 << i)) {
			val = sla_fan[i].rpm;
		} else {
			val = 0;
		}
		cmd_print_ui16(val);
	}
	return CMD_OK;
}

int8_t sla_cmd_Q_(void)
{
	//TWMO and TIMO
	sla_states = 0;
//				sla_states &= ~0x0003;
	sla_states |= st4_msk & 0x03;
	//SBUT and COVS
//				sla_states &= ~0x00c0;
	if (sla_mode != SLA_MODE_ERRO)
	{
		sla_spi_sync(SLA_SPI_REQ_GPIO_IN_A | SLA_SPI_REQ_GPIO_IN_B);
	}
	if (sla_get_start_button() != 0) sla_states |= SLA_STATE_SBUT;
	if (sla_get_cover_switch() != 0) sla_states |= SLA_STATE_COVS;
	if (sla_get_end3() != 0) sla_states |= SLA_STATE_END3;
	if (sys_mcusr != 0) sla_states |= SLA_STATE_FRST;
	if (sla_mode == SLA_MODE_IDLE)
		sla_set_mode(SLA_MODE_LIVE);
	else if (sla_mode == SLA_MODE_ERRO)
		sla_states |= SLA_STATE_FERR;
	sla_live_timer = SLA_LIFE_TIME;
	if (sla_fans.fane)
		sla_states |= SLA_STATE_FANE;
	cmd_print_ui16(sla_states);
	return CMD_OK;
}

int8_t sla_cmd_Q_ver(void)
{
	cmd_printf_P(PSTR("%d.%d.%d%S "), FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_VERSION_PATCH, PSTR(FW_BUILDSX));
	return CMD_OK;
}

int8_t sla_cmd_Q_ser(void)
{
	if (sla_sn[0] == SLA_SN_WRONG)
		printf_P(PSTR("NA "));
	else
	{
		struct
		{
			char dat[4+1];
			char ean[3+1];
			char typ;
			char num[5+1];
		} sn;
		memset(&sn, 0, sizeof(sn));
		strncpy(sn.dat, (char*)sla_sn+3, 4);
		strncpy(sn.ean, (char*)sla_sn+7, 3);
		sn.typ = sla_sn[10];
		strncpy(sn.num, (char*)sla_sn+11, 5);
		printf_P(PSTR("CZPX%4sX%3sX%c%5s "), sn.dat, sn.ean, sn.typ, sn.num);
	}
	return CMD_OK;
}

int8_t sla_cmd_Q_pun(void)
{
	char pun[16];
	uint8_t i;
	sla_read_pseudo_unique(pun);
	for (i = 0; i < 16; i++)
		printf_P(PSTR("%02x"), pun[i]);
	return CMD_OK;
}

int8_t sla_cmd_Q_rev(void)
{
	printf_P(PSTR("%d %d "), (int)BOARD, (((sla_sn[0] & 0x0f) << 4) | (sla_sn[1] & 0x0f)));	// [7:5] suffix, [4:0] revision. 010|00110 -> 6c
	return CMD_OK;
}

int8_t sla_cmd_Q_uled(void)
{
#ifdef SLA_2ND_BOARD_TEST3
	return CMD_OK;
#endif //SLA_2ND_BOARD_TEST3

#ifndef SLA_2ND_BOARD_TEST
	cmd_print_ui8((sla_leds & SLA_MSK_ULED)?1:0);
	if (sla_uled.ms) cmd_print_ui32(sla_uled.ms);
#else //SLA_2ND_BOARD_TEST
	uint8_t uled = (sla_leds & SLA_MSK_ULED)?1:0;
	uled |= (st4_msk & 0x01)?1:0;
	cmd_print_ui8(uled);
	if (sla_uled.ms)
		cmd_print_ui16(sla_uled.ms);
	else
		if (uled)
			cmd_print_ui16(1);
#endif //SLA_2ND_BOARD_TEST
	return CMD_OK;
}

int8_t sla_cmd_Q_twpo(void)
{
	cmd_print_i32(st4_axis[0].pos);
	return CMD_OK;
}

int8_t sla_cmd_Q_tipo(void)
{
	cmd_print_i16((int16_t)(st4_axis[1].pos));
	return CMD_OK;
}

int8_t sla_cmd_Q_XXcf(void)
{
	uint8_t axis = 0;
	sla_axis_config_t cfg;
	if (sla_cmd_id == CMD_ID_TWCF) axis = 0;
	else if (sla_cmd_id == CMD_ID_TICF) axis = 1;
	sla_get_axis_config(axis, &cfg);
	sla_cmd_print_axis_config(&cfg);
	return CMD_OK;
}

int8_t sla_cmd_Q_sgbd(void)
{
	uint8_t i = sla_sgbufw - sla_sgbufc;
	uint8_t c = 10;
	if (c > sla_sgbufc) c = sla_sgbufc;
	sla_sgbufc -= c;
	while (c--)
		cmd_printf_P(PSTR("%02hhx "), sla_sgbuff[i++]);
	return CMD_OK;
}

int8_t sla_cmd_Q_ppst(void)
{
	cmd_print_ui16(10*(eeprom_read_byte((const uint8_t*)0x3ff) - 1));
	return CMD_OK;
}

int8_t sla_cmd_Q_tisc(void)
{
	cmd_print_ui8(sla_tiho_comp_sgn);
	cmd_print_ui8(sla_tiho_comp_sgp);
	return CMD_OK;
}

int8_t sla_cmd_Q_eerx(void)
{
	sla_print_eeprom(cmd_out);
	return CMD_OK;
}



// SET WITH PARAMETER - cmd_sla_ewi
void* sla_cmd_E_ptr(void)
{
	switch (sla_cmd_id)
	{
	case CMD_ID_PPWM: return &sla_pled_pwm;
	case CMD_ID_PSPD: return &sla_pled_spd;
	case CMD_ID_LOG: return &sla_log;
	}
	return 0;
}

int8_t sla_cmd_E_ui8(void)
{
	uint8_t val;
	uint8_t* ptr;
	if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
	switch (sla_cmd_id)
	{
	case CMD_ID_ENA:
		if (val > 3) return CMD_ER_OOR;
		sla_tmc_set_ena(val);
		return CMD_OK;
	case CMD_ID_LED:
		sla_set_led(val);
		return CMD_OK;
	case CMD_ID_DIR:
		if (val > 3) return CMD_ER_OOR;
		st4_msk = (st4_msk & 0x0f) | (val << 4);
		sla_tmc_set_dir(val);
		return CMD_OK;
	case CMD_ID_MOT:
		if (val > 3) return CMD_ER_OOR;
		st4_msk = (st4_msk & 0xfc) | (st4_msk & val);
		return CMD_OK;
	case CMD_ID_END:
		if (val > 3) return CMD_ER_OOR;
		st4_end = (st4_end & 0xf0) | (val & 0x0f);
		return CMD_OK;
	case CMD_ID_FANS:
		if (val > 15) return CMD_ER_OOR;
		sla_set_fan(val);
		return CMD_OK;
	case CMD_ID_TWCU:
		if (val > 63) return CMD_ER_OOR;
		sla_spi_tmc_cur[0] = val;
		sla_spi_sync(SLA_SPI_REQ_TMC0_SET_CUR);
		return CMD_OK;
	case CMD_ID_TICU:
		if (val > 63) return CMD_ER_OOR;
		sla_spi_tmc_cur[1] = val;
		sla_spi_sync(SLA_SPI_REQ_TMC1_SET_CUR);
		return CMD_OK;
	case CMD_ID_SHDN:
		sla_shdn(val);
		return CMD_OK;
	case CMD_ID_PLED:
		if (val > 3) return CMD_ER_OOR;
		sla_set_pled(val);
		return CMD_OK;
	case CMD_ID_ECHO:
		if (val > 1) return CMD_ER_OOR;
		cmd_echo = val;
		return CMD_OK;
	case CMD_ID_PPWM:
		if (val > SLA_SPWM_DEFAULT) return CMD_ER_OOR;
		sla_pled_pwm = val;
		return CMD_OK;
	case CMD_ID_UPWM:
#ifndef SLA_2ND_BOARD_TEST3
		if (val > SLA_UPWM_MAX) return CMD_ER_OOR;
		sla_set_uled_pwm(val);
#endif //SLA_2ND_BOARD_TEST3
		return CMD_OK;
	case CMD_ID_RSEN:
		if (val > 1) return CMD_ER_OOR;
		if (val) sla_rsens_enable();
		else sla_rsens_disable();
		return CMD_OK;
	case CMD_ID_FMSK:
		if (val > 15) return CMD_ER_OOR;
		sla_fans.fmsk = val;
		return CMD_OK;
	default:
		ptr = (uint8_t*)sla_cmd_E_ptr();
		if (ptr)
		{
			*ptr = val;
			return CMD_OK;
		}
	}
	return CMD_ER_NUL;
}

int8_t sla_cmd_E_i8(void)
{
	int8_t val;
	if ((sla_cmd_ret = cmd_scan_i8(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
	switch (sla_cmd_id)
	{
	case CMD_ID_TWSG:
		sla_spi_tmc_sgt[0] = val;
		sla_spi_sync(SLA_SPI_REQ_TMC0_SET_SGT);
		return CMD_OK;
	case CMD_ID_TISG:
		sla_spi_tmc_sgt[1] = val;
		sla_spi_sync(SLA_SPI_REQ_TMC1_SET_SGT);
		return CMD_OK;
	case CMD_ID_TWCS:
		return sla_twcs(val);
	case CMD_ID_TICS:
		return sla_tics(val);
	case CMD_ID_TWST:
		return sla_do_steps(0, val);
	case CMD_ID_TIST:
		return sla_do_steps(1, val);
	}
	return CMD_ER_NUL;
}

int8_t sla_cmd_E_ui16(void)
{
	uint16_t val;
	if ((sla_cmd_ret = cmd_scan_ui16(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
	switch (sla_cmd_id)
	{
	case CMD_ID_TWUP:
		if (sla_twup(val) < 0) return CMD_ER_BSY;
		return CMD_OK;
	case CMD_ID_TWDN:
		if (sla_twdn(val) < 0) return CMD_ER_BSY;
		return CMD_OK;
	case CMD_ID_WDEL:
		sla_wdel_ms = val;
		while (sla_wdel_ms)
			sla_cycle();
		return CMD_OK;
	case CMD_ID_TWPH:
		if (val > 1023) return CMD_ER_OOR;
		return sla_set_phase(0, val);
	case CMD_ID_TIPH:
		if (val > 1023) return CMD_ER_OOR;
		return sla_set_phase(1, val);
	}
	return CMD_ER_NUL;
}

int8_t sla_cmd_E_i16(void)
{
	int16_t val;
	if ((sla_cmd_ret = cmd_scan_i16(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
	switch (sla_cmd_id)
	{
	case CMD_ID_TIMA:
		if (sla_tima(val) < 0) return CMD_ER_BSY;
		return CMD_OK;
	case CMD_ID_TIPO:
		st4_axis[1].pos = val;
		return CMD_OK;
	}
	return CMD_ER_NUL;
}

int8_t sla_cmd_E_i32(void)
{
	int32_t val;
	if ((sla_cmd_ret = cmd_scan_i32(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
	switch (sla_cmd_id)
	{
	case CMD_ID_TWMA:
		if (sla_twma(val) < 0) return CMD_ER_BSY;
		return CMD_OK;
	case CMD_ID_TWPO:
		st4_axis[0].pos = val;
		return CMD_OK;
	case CMD_ID_RSME:
		if (val > 120000) return CMD_ER_OOR;
		sla_rsens_meassure(val);
		return CMD_OK;
	}
	return CMD_ER_NUL;
}

int8_t sla_cmd_E_beep(void)
{
	uint16_t val0;
	uint16_t val1;
	if ((sla_cmd_ret = cmd_scan_ui16(sla_cmd_pstr, &val0)) < 0) return sla_cmd_ret;
	sla_cmd_pstr += sla_cmd_ret;
	if ((sla_cmd_ret = cmd_scan_ui16(sla_cmd_pstr, &val1)) < 0) return sla_cmd_ret;
	if ((val0 < 16) || (val0 > 5000)) return CMD_ER_OOR;
	if (val1 > 10000) return CMD_ER_OOR;
	sla_beep(val0, val1);
	return CMD_OK;
}


int8_t sla_cmd_E_uled(void)
{
#ifdef SLA_2ND_BOARD_TEST3
	return CMD_OK;
#endif //SLA_2ND_BOARD_TEST3

	uint8_t val0;
	uint32_t val1;
	if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &val0)) < 0) return sla_cmd_ret;
	sla_cmd_pstr += sla_cmd_ret;
	val1 = 0;
	if (*sla_cmd_pstr) if ((sla_cmd_ret = cmd_scan_ui32(sla_cmd_pstr, &val1)) < 0) return sla_cmd_ret;
	sla_uled.ms = val1;
	if (val0) sla_set_led(sla_leds | SLA_MSK_ULED);
	else sla_set_led(sla_leds & ~SLA_MSK_ULED);
#ifdef SLA_2ND_BOARD_TEST
	if (val0 && val1)
	{
		if (st4_msk & 1)
			return CMD_ER_BSY;
		if (val1 < 3000) val1 = 3000;
		if (val1 > 65000) val1 = 65000;
		uint16_t sr = (uint32_t)15000 * 3400 / val1;
		sla_twcs(2);
		st4_axis[0].sr0 = sr / 2;
		st4_axis[0].srm = sr;
		st4_calc_acdc(0);
		st4_end = 0;
		if (st4_axis[0].pos == 0)
		{
			sla_twma(-50000);
		}
		else if (st4_axis[0].pos == -50000)
		{
			sla_twma(0);
		}
	}
#endif //SLA_2ND_BOARD_TEST
	return CMD_OK;
}

int8_t sla_cmd_E_eecl1(void)
{
	uint16_t val0;
	uint16_t val1;
	if ((sla_cmd_ret = cmd_scan_ui16(sla_cmd_pstr, &val0)) < 0) return sla_cmd_ret;
	if (val0 > 1023) return CMD_ER_OOR;
	sla_cmd_pstr += sla_cmd_ret;
	if (*sla_cmd_pstr) if ((sla_cmd_ret = cmd_scan_ui16(sla_cmd_pstr, &val1)) < 0) return sla_cmd_ret;
	if (val1 < 1) return CMD_ER_OOR;
	if (val1 > (1024 - val0)) return CMD_ER_OOR;
	eeprom_erase(val0, val1);
	return CMD_OK;
}

int8_t sla_cmd_E_XXcf(void)
{
	int8_t ret = 0;
	uint8_t axis = 0;
	sla_axis_config_t cfg;
	if (sla_cmd_id == CMD_ID_TWCF) axis = 0;
	else if (sla_cmd_id == CMD_ID_TICF) axis = 1;
	if ((ret = sla_cmd_scan_axis_config(&cfg, sla_cmd_pstr)) < 0) return ret;
	sla_set_axis_config(axis, &cfg);
	sla_save_axis_config(axis);
	return CMD_OK;
}

int8_t sla_cmd_E_frpm(void)
{
	uint16_t val = 0;
	for (uint8_t i = 0; i < SLA_FAN_NUM; i++)
	{
		if ((sla_cmd_ret = cmd_scan_ui16(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
		switch (i)
		{
			case 0:
				if (val > SLA_FRPM_UV_MAX) return CMD_ER_OOR;
				break;
			case 1:
				if (val > SLA_FRPM_BLOWER_MAX) return CMD_ER_OOR;
				break;
			case 2:
				if (val > SLA_FRPM_REAR_MAX) return CMD_ER_OOR;
				break;
		}
		if (val < SLA_FRPM_MIN) return CMD_ER_OOR;
		sla_cmd_pstr += sla_cmd_ret;
		#ifdef _SIMULATOR
			sla_fan[i].tacho = 30000 / val;
			sla_fan[i].rpm = val;
		#else
			if(i == REAR_FAN_IDX) {
				sla_fan[i].target_rpm = val;
				sla_fan[i].rpm_tolerance = val / 100 * 90;
			} else {
				sla_spwm_dev[i].duty = 30000 / val;
				sla_fan[i].rpm_tolerance = (30000 / (val / 100 * 90)) + 1;
			}
		#endif

		sla_set_fan(sla_spwm.enable);
	}
	return CMD_OK;
}

int8_t sla_cmd_E_twhc1(void)
{
	int16_t val0;
	if ((sla_cmd_ret = cmd_scan_i16(sla_cmd_pstr, &val0)) < 0) return sla_cmd_ret;
	if ((val0 < -1) || (val0 > ((1024 >> sla_mstep_resolution[0]) - 1))) return CMD_ER_OOR;
	sla_twho_phase = val0;
	sla_cfg_dirty |= (1 << SLA_CFG_TWHPHA);
	sla_cfg_save();
	return CMD_OK;
}

int8_t sla_cmd_E_tihc1(void)
{
	int16_t val0;
	if ((sla_cmd_ret = cmd_scan_i16(sla_cmd_pstr, &val0)) < 0) return sla_cmd_ret;
	if ((val0 < -1) || (val0 > ((1024 >> sla_mstep_resolution[1]) - 1))) return CMD_ER_OOR;
	sla_tiho_phase = val0;
	sla_cfg_dirty |= (1 << SLA_CFG_TIHPHA);
	sla_cfg_save();
	return CMD_OK;
}

int8_t sla_cmd_E_ppst(void)
{
	uint8_t val0;
	if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &val0)) < 0) return sla_cmd_ret;
	if (val0 > 1) return CMD_ER_OOR;
	sla_set_power_panic_status(val0);
	return CMD_OK;
}

int8_t sla_cmd_E_tisc(void)
{
	if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &sla_tiho_comp_sgn)) < 0) return sla_cmd_ret;
	sla_cmd_pstr += sla_cmd_ret;
	if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &sla_tiho_comp_sgp)) < 0) return sla_cmd_ret;
	sla_cfg_dirty |= (1 << SLA_CFG_TIHOSC);
	sla_cfg_save();
	return CMD_OK;
}

#ifdef SLA_TISE
int8_t sla_cmd_E_tise(void)
{
	int16_t val0; //tighten target position
	uint16_t val1; //tighten wait delay
	int16_t val2; //finish target position
	uint16_t val3; //finish wait delay
	if ((sla_cmd_ret = cmd_scan_i16(sla_cmd_pstr, &val0)) < 0) return sla_cmd_ret;
	sla_cmd_pstr += sla_cmd_ret;
	if ((sla_cmd_ret = cmd_scan_ui16(sla_cmd_pstr, &val1)) < 0) return sla_cmd_ret;
	sla_cmd_pstr += sla_cmd_ret;
	if ((sla_cmd_ret = cmd_scan_i16(sla_cmd_pstr, &val2)) < 0) return sla_cmd_ret;
	sla_cmd_pstr += sla_cmd_ret;
	if ((sla_cmd_ret = cmd_scan_ui16(sla_cmd_pstr, &val3)) < 0) return sla_cmd_ret;
	sla_tise_tpos = val0;
	sla_tise_tdel = val1;
	sla_tise_fpos = val2;
	sla_tise_fdel = val3;
	return sla_tise();
}
#endif //SLA_TISE

int8_t sla_cmd_E__por_pin_ddr(void)
{
	uint8_t val0;
	uint8_t val1;
	if (((val0 = *sla_cmd_pstr) < 'B') || (val0 > 'F')) return CMD_ER_OOR;
	sla_cmd_pstr += 1; if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &val1)) < 0) return sla_cmd_ret;
	switch (sla_cmd_id)
	{
	case CMD_ID__POR: sla_set_POR(val0, val1); break;
	case CMD_ID__PIN: sla_set_PIN(val0, val1); break;
	case CMD_ID__DDR: sla_set_DDR(val0, val1); break;
	}
	return CMD_OK;
}

int8_t sla_cmd_E_usta(void)
{
	uint8_t val;
	if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
		sla_cmd_pstr += sla_cmd_ret;
	if (val == 1)		//clear uv led statistics (when UV led is changed)
		sla_uled.uvled_counter = 0;
	else if (val == 2)	//clear display statistics (when display is changed)
		sla_uled.display_counter = 0;
	eeprom_update_bytes(sla_cfg_addr(SLA_CFG_USTA), (uint8_t*)(&sla_uled.uvled_counter), 4);
	eeprom_update_bytes(sla_cfg_addr(SLA_CFG_USTA_DSP), (uint8_t*)(&sla_uled.display_counter), 4);
	return CMD_OK;
}

int8_t sla_cmd_E_ulcd(void)
{
	uint8_t val;
	if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
		sla_cmd_pstr += sla_cmd_ret;
	if ((val < 0) || (val > 1)) return CMD_ER_OOR;
	sla_uled.display_counter_mask = val;
	return CMD_OK;
}

int8_t sla_cmd_E_twgf(void)
{
	uint8_t val;
	if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
		sla_cmd_pstr += sla_cmd_ret;
	if ((val < 0) || (val > 1)) return CMD_ER_OOR;
	sla_goto_fullstep(sla_get_phase(0), 0, val);
	return CMD_OK;
}

int8_t sla_cmd_E_tigf(void)
{
	uint8_t val;
	if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
		sla_cmd_pstr += sla_cmd_ret;
	if ((val < 0) || (val > 1)) return CMD_ER_OOR;
	sla_goto_fullstep(sla_get_phase(1), 1, val);
	return CMD_OK;
}



// GET WITH PARAMETER - cmd_sla_qwi
int8_t sla_cmd_Q_XXcf1(void)
{
	uint8_t val0;
	uint8_t axis = 0;
	uint16_t addr;
	sla_axis_config_t cfg;
	if (sla_cmd_id == CMD_ID_TWCF) axis = 0;
	else if (sla_cmd_id == CMD_ID_TICF) axis = 1;
	if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &val0)) < 0) return sla_cmd_ret;
	if (val0 > 15) return CMD_ER_OOR;
	if (axis == 0) addr = sla_cfg_addr(SLA_CFG_TWCFGN);
	else if (axis == 1) addr = sla_cfg_addr(SLA_CFG_TICFGN);
	eeprom_read_bytes(addr + 16 * val0, (uint8_t*)(&cfg), 16);
	sla_cmd_print_axis_config(&cfg);
	return CMD_OK;
}

int8_t sla_cmd_Q__por_pin_ddr(void)
{
	uint8_t val;
	uint8_t val0;
	if (((val0 = *sla_cmd_pstr) < 'B') || (val0 > 'F')) return CMD_ER_OOR;
	switch (sla_cmd_id)
	{
	case CMD_ID__POR: val = sla_get_POR(val0); break;
	case CMD_ID__PIN: val = sla_get_PIN(val0); break;
	case CMD_ID__DDR: val = sla_get_DDR(val0); break;
	}
	cmd_print_ui8(val);
	return CMD_OK;
}

#ifdef SLA_DBG_TMCC
int8_t sla_cmd_Q_tmcc(void)
{
       uint8_t axis;
       uint8_t write;
       uint8_t reg;
       uint32_t val;
       if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &axis)) < 0) return sla_cmd_ret;
               sla_cmd_pstr += sla_cmd_ret;
       if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &write)) < 0) return sla_cmd_ret;
               sla_cmd_pstr += sla_cmd_ret;
       if ((sla_cmd_ret = cmd_scan_ui8(sla_cmd_pstr, &reg)) < 0) return sla_cmd_ret;
               sla_cmd_pstr += sla_cmd_ret;
       if ((sla_cmd_ret = cmd_scan_ui32(sla_cmd_pstr, &val)) < 0) return sla_cmd_ret;
               sla_cmd_pstr += sla_cmd_ret;
       if(write)
               tmc2130_wr(axis, reg, &val);
       val = 0;
       tmc2130_rd(axis, reg, &val);
       cmd_print_ui32(val);

       return CMD_OK;
}
#endif
