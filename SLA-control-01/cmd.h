#ifndef _CMD_H
#define _CMD_H
#include "config.h"
#include <inttypes.h>
#include <stdio.h>

//result codes
#define CMD_OK         0 //ok - success
#define CMD_ER_UNK    -1 //error 1 - unknown/unspecified failure
#define CMD_ER_BSY    -2 //error 2 - busy
#define CMD_ER_SYN    -3 //error 3 - syntax error
#define CMD_ER_OOR    -4 //error 4 - parameter out of range
#define CMD_ER_ONP    -5 //error 5 - operation not permitted
#define CMD_ER_NUL    -6 //error 6 - null pointer
#define CMD_ER_CNF    -7 //error 7 - command not found

//data types
#define TYPE_UI8    0x00
#define TYPE_UI16   0x01
#define TYPE_UI32   0x02
#define TYPE_I8     0x04
#define TYPE_I16    0x05
#define TYPE_I32    0x06
#define TYPE_FLOAT  0x08

#define CMD_ID_unk     0xff //unknown commad
#define CMD_ID_        0x00 //empty commad

//variant union type
typedef union
{
	uint8_t  ui8;
	uint16_t ui16;
	uint32_t ui32;
	int8_t   i8;
	int16_t  i16;
	int32_t  i32;
	float    flt;
} var_num_t;

typedef int8_t (*cmd_func_t)(void);

typedef struct
{
	uint8_t id;
	cmd_func_t func;
} cmd_entry_t;

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern FILE* cmd_in;
extern FILE* cmd_out;
extern FILE* cmd_err;
extern uint8_t cmd_echo;

extern int8_t cmd_print_ui8(uint8_t val);
extern int8_t cmd_print_ui16(uint16_t val);
extern int8_t cmd_print_ui32(uint32_t val);
extern int8_t cmd_print_i8(int8_t val);
extern int8_t cmd_print_i16(int16_t val);
extern int8_t cmd_print_i32(int32_t val);
extern int8_t cmd_scan_ui8(char* pstr, uint8_t* pval);
extern int8_t cmd_scan_ui16(char* pstr, uint16_t* pval);
extern int8_t cmd_scan_ui32(char* pstr, uint32_t* pval);
extern int8_t cmd_scan_i8(char* pstr, int8_t* pval);
extern int8_t cmd_scan_i16(char* pstr, int16_t* pval);
extern int8_t cmd_scan_i32(char* pstr, int32_t* pval);
extern void cmd_putc(char c);
extern int cmd_printf_P(const char* format, ...);
extern int cmd_err_printf_P(const char* format, ...);
extern void cmd_process(void);
extern uint8_t cmd_parse_tab(char* pstr, uint8_t* ptab, uint8_t len, uint8_t cnt);
extern int8_t cmd_do(uint8_t cmd_id, const cmd_entry_t* table, uint8_t count);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_CMD_H
