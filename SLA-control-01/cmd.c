/*
 * file stream command processor
 * processes the commands received and send via UART
 */
#include "cmd.h"
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <avr/pgmspace.h>

FILE* cmd_in = 0;
FILE* cmd_out = 0;
FILE* cmd_err = 0;
uint8_t cmd_echo = 0;

extern int8_t cmd_parse_mod_msk(char* pstr, uint16_t* pmod_msk);
extern int8_t cmd_parse_cmd_id(char* pstr, uint8_t* pcmd_id);
extern int8_t cmd_do_wout_args(uint16_t mod_msk, char pref, uint8_t cmd_id);
extern int8_t cmd_do_with_args(uint16_t mod_msk, char pref, uint8_t cmd_id, char* pstr);

int8_t cmd_print_ui8(uint8_t val)
{
	int n = cmd_printf_P(PSTR("%u "), val);
	return (int8_t)((n < 0)?0:n);
}

int8_t cmd_print_ui16(uint16_t val)
{
	int n = cmd_printf_P(PSTR("%u "), val);
	return (int8_t)((n < 0)?0:n);
}

int8_t cmd_print_ui32(uint32_t val)
{
	int n = cmd_printf_P(PSTR("%lu "), val);
	return (int8_t)((n < 0)?0:n);
}

int8_t cmd_print_i8(int8_t val)
{
	int n = cmd_printf_P(PSTR("%i "), val);
	return (int8_t)((n < 0)?0:n);
}

int8_t cmd_print_i16(int16_t val)
{
	int n = cmd_printf_P(PSTR("%i "), val);
	return (int8_t)((n < 0)?0:n);
}

int8_t cmd_print_i32(int32_t val)
{
	int n = cmd_printf_P(PSTR("%li "), val);
	return (int8_t)((n < 0)?0:n);
}

#ifdef STRTO_SCAN

int8_t cmd_scan_ui8(char* pstr, uint8_t* pval)
{
	char* pend = pstr;
	uint32_t val = strtoul(pstr, &pend, 10);
	if ((errno == ERANGE) || (val >= 256)) return CMD_ER_OOR;
	*pval = (uint8_t)val;
	return (int8_t)(pend - pstr);
}

int8_t cmd_scan_ui16(char* pstr, uint16_t* pval)
{
	char* pend = pstr;
	uint32_t val = strtoul(pstr, &pend, 10);
	if ((errno == ERANGE) || (val >= 65536)) return CMD_ER_OOR;
	*pval = (uint16_t)val;
	return (int8_t)(pend - pstr);
}

int8_t cmd_scan_ui32(char* pstr, uint32_t* pval)
{
	char* pend = pstr;
	uint32_t val = strtoul(pstr, &pend, 10);
	if (errno == ERANGE) return CMD_ER_OOR;
	return (int8_t)(pend - pstr);
}

int8_t cmd_scan_i8(char* pstr, int8_t* pval)
{
	char* pend = pstr;
	int32_t val = strtol(pstr, &pend, 10);
	if ((errno == ERANGE) || (val > 127) || (val < -128)) return CMD_ER_OOR;
	*pval = (int8_t)val;
	return (int8_t)(pend - pstr);
}

int8_t cmd_scan_i16(char* pstr, int16_t* pval)
{
	char* pend = pstr;
	int32_t val = strtol(pstr, &pend, 10);
	if ((errno == ERANGE) || (val > 32767) || (val < -32768)) return CMD_ER_OOR;
	*pval = (int16_t)val;
	return (int8_t)(pend - pstr);
}

int8_t cmd_scan_i32(char* pstr, int32_t* pval)
{
	char* pend = pstr;
	int32_t val = strtol(pstr, &pend, 10);
	if (errno == ERANGE) return CMD_ER_OOR;
	*pval = val;
	return (int8_t)(pend - pstr);
}

#else //STRTO_SCAN

int8_t cmd_scan_ui8(char* pstr, uint8_t* pval)
{
#ifdef _SIMULATOR
	uint16_t val;
	int n; if (sscanf_P(pstr, PSTR("%hu%n"), &val, &n) != 1) return CMD_ER_SYN;
	*pval = (uint8_t)val;
#else //_SIMULATOR
	int n; if (sscanf_P(pstr, PSTR("%hhu%n"), pval, &n) != 1) return CMD_ER_SYN;
#endif //_SIMULATOR
	return (int8_t)n;
}

int8_t cmd_scan_ui16(char* pstr, uint16_t* pval)
{
#ifdef _SIMULATOR
	uint16_t val;
	int n; if (sscanf_P(pstr, PSTR("%hu%n"), &val, &n) != 1) return CMD_ER_SYN;
	*pval = (uint16_t)val;
#else //_SIMULATOR
	int n; if (sscanf_P(pstr, PSTR("%u%n"), pval, &n) != 1) return CMD_ER_SYN;
#endif //_SIMULATOR
	return (int8_t)n;
}

int8_t cmd_scan_ui32(char* pstr, uint32_t* pval)
{
	int n; if (sscanf_P(pstr, PSTR("%lu%n"), pval, &n) != 1) return CMD_ER_SYN;
	return (int8_t)n;
}

int8_t cmd_scan_i8(char* pstr, int8_t* pval)
{
#ifdef _SIMULATOR
	int16_t val;
	int n; if (sscanf_P(pstr, PSTR("%hi%n"), &val, &n) != 1) return CMD_ER_SYN;
	*pval = (int8_t)val;
#else //_SIMULATOR
	int n; if (sscanf_P(pstr, PSTR("%hhi%n"), pval, &n) != 1) return CMD_ER_SYN;
#endif //_SIMULATOR
	return (int8_t)n;
}

int8_t cmd_scan_i16(char* pstr, int16_t* pval)
{
#ifdef _SIMULATOR
	int16_t val;
	int n; if (sscanf_P(pstr, PSTR("%hi%n"), &val, &n) != 1) return CMD_ER_SYN;
	*pval = (int16_t)val;
#else //_SIMULATOR
	int n; if (sscanf_P(pstr, PSTR("%i%n"), pval, &n) != 1) return CMD_ER_SYN;
#endif //_SIMULATOR
	return (int8_t)n;
}

int8_t cmd_scan_i32(char* pstr, int32_t* pval)
{
	int n; if (sscanf_P(pstr, PSTR("%li%n"), pval, &n) != 1) return CMD_ER_SYN;
	return (int8_t)n;
}

#endif //STRTO_SCAN

void cmd_putc(char c)
{
	fputc(c, cmd_out);
}

int cmd_printf_P(const char* format, ...)
{
	int ret;
	va_list args;
	va_start(args, format);
	ret = vfprintf_P(cmd_out, format, args);
	va_end(args);
	return ret;
}

int cmd_err_printf_P(const char* format, ...)
{
	int ret;
	va_list args;
	va_start(args, format);
	ret = vfprintf_P(cmd_err, format, args);
	va_end(args);
	return ret;
}


#ifdef _SIMULATOR
#ifndef __unix__
#include <conio.h>
#endif
#include <sys/ioctl.h>

int kbhit() {
    int notRead;
    ioctl(0, FIONREAD, &notRead);
    return notRead;
}

#define getch getchar

#endif //_SIMULATOR

void cmd_process(void)
{
	static char line[CMD_MAX_LINE];
	static int count = 0;
	char c = -1;
	char* pstr = 0;
	uint16_t mod_msk = 0;
	uint8_t cmd_id = 0;
	int8_t ret = -1;
	if (count < CMD_MAX_LINE)
	{
#ifdef _SIMULATOR
		//simulator single char mode
#ifdef __unix__
		while (kbhit() && ((c = (char)getch()) >= 0))
#else
		while (kbhit() && ((c = (char)getch()) >= 0) && (putchar((c==0x0d)?'\n':c)))
#endif
		//TODO: line mode
#else //_SIMULATOR
		while ((c = (char)fgetc(cmd_in)) >= 0)
#endif //_SIMULATOR
		{
			if (c == '\r') c = 0;
			if (c == '\n') c = 0;
			line[count] = c;
			if (c) count++;
			if ((c == 0) || (count >= CMD_MAX_LINE)) break;
		}
	}
	if (count >= CMD_MAX_LINE)
	{ //command overflow
		cmd_printf_P(PSTR("ERROR: command overflow\n"), count);
		count = 0;
	}
	else if ((count > 0) && (c == 0))
	{ //line received
		if (cmd_echo)
		{
			fputs(line, cmd_out);
			fputc('\n', cmd_out);
		}
		pstr = line;
		mod_msk = 0;
		ret = cmd_parse_mod_msk(pstr, &mod_msk);
		if (ret >= 0)
		{
			pstr += ret;
			c = *pstr;
			pstr++;
			ret = cmd_parse_cmd_id(pstr, &cmd_id);
			if (ret >= 0)
			{
				pstr += ret;
				if (*pstr == 0)
					ret = cmd_do_wout_args(mod_msk, c, cmd_id);
				else if (*pstr == ' ')
					ret = cmd_do_with_args(mod_msk, c, cmd_id, pstr + 1);
				else
					ret = CMD_ER_SYN;
			}
		}

#ifndef SLA_2ND_BOARD_TEST

#ifndef SLA_2ND_BOARD_TEST3
		cmd_printf_P((ret == 0)?PSTR("ok\n"):PSTR("e%d\n"), -ret);
#else //SLA_2ND_BOARD_TEST
		if ((cmd_id != 0x84) && (cmd_id != 0xb8))//CMD_ID_ULED and CMD_ID_UPWM
			cmd_printf_P((ret == 0)?PSTR("ok\n"):PSTR("e%d\n"), -ret);
#endif //SLA_2ND_BOARD_TEST3

#else //SLA_2ND_BOARD_TEST
		if (ret == 0) cmd_printf_P(PSTR("ok\n"));
#endif //SLA_2ND_BOARD_TEST

		count = 0;
	}
	else
	{ //nothing received
		// TODO: lifetime
	}
}

uint8_t cmd_parse_tab(char* pstr, uint8_t* ptab, uint8_t len, uint8_t cnt)
{
	uint8_t i;
	while (cnt--)
	{
		for (i = 0; i < len; i++)
			if (((uint8_t)pstr[i]) != pgm_read_byte(ptab + i))
				break;
		if (i == len)
			return pgm_read_byte(ptab + len);
		ptab += (len + 1);
	}
	return CMD_ID_unk;
}

int8_t cmd_do(uint8_t cmd_id, const cmd_entry_t* table, uint8_t count)
{
	uint8_t i;
	cmd_func_t func;
	for (i = 0; i < count; i++)
		if (pgm_read_byte((table + i)) == cmd_id)
		{
#ifndef _SIMULATOR
			func = (cmd_func_t) pgm_read_word(((uint16_t)(table + i)) + 1);
#else //_SIMULATOR
			func = table[i].func;
#endif //_SIMULATOR
			if (func)
				return func();
			return CMD_ER_NUL;
		}
	return CMD_ER_CNF;
}

