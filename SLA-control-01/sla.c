/*
 * functions specific for SL1
 * stepper motor movement, controling PWM, ...
 */
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/boot.h>

#include "sla.h"
#include "io_atmega32u4.h"
#include "sys.h"
#include "timer0.h"
#include "spi.h"
#include "mcp23s17.h"
#include "tmc2130.h"
#include "st4.h"
#include "snd.h"
#include "cmd.h"
#include "morse.h"
#include "adc.h"
#include "sla_diag.h"
#include "sla_sign.h"
#include "sla_cfg.h"
#include "sla_adc.h"
#include "sla_tmc.h"
#include "sla_rsens.h"
#include "sla_spi.h"

#define SLA_TWHO_NONE  -1  //not initialized
#define SLA_TWHO_IDLE   0  //idle, initialized
#define SLA_TWHO_START  1  //started
#define SLA_TWHO_GOMINC 2  //going to minimum (coarse)
#define SLA_TWHO_GOFWDC 3  //going forward (coarse)
#define SLA_TWHO_GOMINA 4  //going to minimum (acurate)
#define SLA_TWHO_GOFWDF 5  //going forward (final)
#define SLA_TWHO_ENOEND -2 //endstop not reached
#define SLA_TWHO_EBLOCK -3 //axis maybe blocked
#define SLA_TWHO_OFFS_I 0 //init
#define SLA_TWHO_OFFS_C 1 //calibrate
#define SLA_TWHO_OFFS_M 2 //measure

#define SLA_TIHO_NONE  -1  //not initialized
#define SLA_TIHO_IDLE   0  //idle, initialized
#define SLA_TIHO_START  1  //started
#define SLA_TIHO_GOBCKS 2  //going to minimum (silent)
#define SLA_TIHO_GOFWDS 3  //going forward (silent)
#define SLA_TIHO_GOMINC 4  //going to minimum (coarse)
#define SLA_TIHO_GOFWDC 5  //going forward (coarse)
#define SLA_TIHO_GOMINA 6  //going minimum (acurate)
#define SLA_TIHO_GOFWDF 7  //going forward (final)
#define SLA_TIHO_ENOEND -2 //endstop not reached
#define SLA_TIHO_EBLOCK -3 //axis maybe blocked
#define SLA_TIHO_OFFS_I 0 //init
#define SLA_TIHO_OFFS_C 1 //calibrate
#define SLA_TIHO_OFFS_M 2 //measure

#ifdef SLA_TISE
#define SLA_TISE_NONE   -1 //not initialized
#define SLA_TISE_IDLE    0 //idle, initialized
#define SLA_TISE_START   1 //started
#define SLA_TISE_TRUN    2 //tighten - running
#define SLA_TISE_TWAIT   3 //tighten - waiting
#define SLA_TISE_FRUN    4 //finish - running
#define SLA_TISE_FWAIT   5 //finish - waiting
#define SLA_TISE_ENOEND -2 //endstop not reached
#define SLA_TISE_EBLOCK -3 //axis maybe blocked

int8_t sla_tise_state = SLA_TISE_NONE;
uint32_t sla_tise_pos0 = 0;
int16_t sla_tise_tpos = 0;  //tighten target position
uint16_t sla_tise_tdel = 0; //tighten wait delay
int16_t sla_tise_fpos = 0;  //finish target position
uint16_t sla_tise_fdel = 0; //finish wait delay
uint16_t sla_tise_ms = 0; //timer
#endif //SLA_TISE

uint8_t sla_twho_calib = 0;
int16_t sla_twho_phase = -1;
int16_t sla_twho_offset = -1;
int8_t sla_twho_state = SLA_TWHO_NONE;
int8_t sla_twho_offset_state = SLA_TWHO_OFFS_I;
uint32_t sla_twho_pos0 = 0;

uint8_t sla_tiho_calib = 0;
int16_t sla_tiho_phase = -1; // stores default tilt home phase. Tilt is forced to go to fullstep position after homing
int16_t sla_tiho_offset = -1;
int8_t sla_tiho_state = SLA_TIHO_NONE;
int8_t sla_tiho_offset_state = SLA_TIHO_OFFS_I;
uint32_t sla_tiho_pos0 = 0;
uint8_t sla_tiho_comp_sgup = 0;
uint8_t sla_tiho_comp_sgdn = 0;
int8_t sla_tiho_comp = 0;
uint8_t sla_tiho_comp_sgn = 0;
uint8_t sla_tiho_comp_sgp = 0;

volatile uint8_t sla_mode = SLA_MODE_BOOT;
volatile uint8_t sla_live_timer = 0;
volatile uint16_t sla_states = 0;
uint8_t sla_sn[16]; //serial number
int8_t sla_twcs_index = -1;
int8_t sla_tics_index = -1;
uint8_t sla_mstep_resolution[TMC2130_NUMAXES] = {SLA_TW_MRES, SLA_TI_MRES};
uint16_t sla_beep_ms = 0;
sla_fans_state_t sla_fans = {0};
sla_fan_t sla_fan[SLA_FAN_NUM] = {0};
sla_spwm_state_t sla_spwm;
sla_spwm_dev_t sla_spwm_dev[SLA_SPWM_DEV] = {0};	//FAN1, FAN2, FAN3, START-LED
uint8_t sla_time_s = 0;
uint8_t sla_time_m = 0;
uint8_t sla_time_h = 0;
uint16_t sla_ms = 0;
uint8_t sla_shdn_timer = 0;
uint8_t sla_shdn_timer2 = 0;
uint8_t sla_leds = 0;
uint8_t sla_pled = SLA_PLED_DISA;
uint8_t sla_pled_pwm = SLA_SPWM_DEFAULT;
uint16_t sla_pled_pha = 0;
uint8_t sla_pled_spd = 8;
sla_uled_t sla_uled = {0};
uint8_t sla_log = 0;
uint8_t sla_sgbuff[256];
uint8_t sla_sgbufw = 0;
uint8_t sla_sgbufc = 0;
uint8_t sla_olicnt_tower = 0;
uint8_t sla_olicnt_tilt = 0;
volatile uint16_t sla_wdel_ms = 0;
uint8_t sla_power_panic_status = 0;
uint8_t sla_gpio[2] =
{
	MCP23S17_DEF_GPO & 0xff,
	MCP23S17_DEF_GPO >> 8,
};
uint8_t sla_gpio_r[2];
uint8_t sla_tmc_timer = 0;

extern inline void isr_fan_adjust_rpm(void);
extern inline void isr_pwm(void);
extern inline void isr_fan_rpm(void);
extern inline void isr_tmc(void);
extern inline void sla_cycle_tiho(void);
extern inline void sla_cycle_twho(void);

void sla_init(void)
{
	int8_t ret;

#ifndef SLA_2ND_BOARD_TEST
	printf_P(PSTR("MCUSR=0x%02x\n"), sys_mcusr);
#endif //SLA_2ND_BOARD_TEST

	snd_init();
	sla_cfg_init();

	//internal diagnostics - synchronous part (steps 1..5):
	// appl/boot flash checksum, serial number, fuses, lock
	ret = sla_diag_sync_1_5();
	if (ret != SLA_ERROR_NONE) sla_set_error(ret);
	spi_init();

	//setup trinamic CS signals to high (to prevent colissions in SPI port)
	DDRD |= 0x12;  //TMC CS0..1 output
	PORTD |= 0x12; //TMC CS0..1 high

	_delay_ms(10);

	ret = sla_init_gpio();
#if defined(SLA_AUTOKILL) && !defined(_SIMULATOR)
	//do not shutdown if any reset flag set, except SYS_RESET_PORF
	if (((sys_mcusr & 0xfe) == 0) && !sla_get_start_button())
	{
		cmd_err_printf_P(PSTR("killed\n"));
		sla_set_power(0);
		while (1);
	}
#endif //defined(SLA_AUTOKILL) && !defined(_SIMULATOR)
	if (ret < 0) sla_set_error(SLA_ERROR_GSPI);

	adc_init();
	sla_init_leds();				//power led on, rx and tx leds off
	sla_uled.pwm = SLA_UPWM_MAX;	//defalut UV led PWM
	sla_init_spwm();
	sla_tmc_setup_pins();
	ret = tmc2130_init();

#ifndef SLA_DIAG_SKIP_TSPI
	if (ret < 0)
		sla_set_error(SLA_ERROR_TSPI);
	else //initialize drivers and steper only if both are working
#endif //SLA_DIAG_SKIP_TSPI
	{
		tmc2130_set_mres(0, sla_mstep_resolution[0]); //16 microsteps for tower
		tmc2130_set_mres(1, sla_mstep_resolution[1]); //64 microsteps for tilt
		tmc2130_set_cur(0, SLA_TW_CUR_DEF);
		tmc2130_set_sgt(0, SLA_TW_SGT_DEF);
		tmc2130_set_cur(1, SLA_TI_CUR_DEF);
		tmc2130_set_sgt(1, SLA_TI_SGT_DEF);

		_delay_ms(50);

		st4_setup_axis(0, 800, 3200, 17600, 250, 250); //res=800ustep/mm, sr0=1mm/s, srm=8mm/s, acc=100mm/s^2, dec=50mm/s^2
		st4_setup_axis(1, 3200, 960, 1280, 12, 12); //res=3200ustep/rev, sr0=0.25rev/s, srm=0.5rev/s, acc=1rev/s^2, dec=1rev/s^2
		st4_setup_timer();

		st4_end = 0x03; //enable endstop for tower and tilt
	}

	DDRF &= ~0x70; //FAN[1:3]-TACH input
	PORTF |= 0x70; //FAN[1:3]-TACH pullup enabled
	DDRF |= 0x80;  //FAN3-PWM as output

	sla_set_mux(0);
	sla_beep(440, 50);

	timer0_init();
	//setup INT6 - falling edge
	EICRB &= ~(3 << ISC60);
	EICRB |= (2 << ISC60);
	//enable INT6 interrupt (power panic)
	//EIMSK |= (1 << INT6);
#ifndef SLA_DIAG_SKIP_TSIG
	if (sla_mode == SLA_MODE_BOOT) //no error
		if (sla_tmc_test_signals(0) || sla_tmc_test_signals(1))
			sla_set_error(SLA_ERROR_TSIG);
#endif //SLA_DIAG_SKIP_TSIG
	if (sla_mode == SLA_MODE_BOOT) //no error
	{
		sla_tmc_tstep1000[0] = sla_tmc_measure_tstep1000(0);
		sla_tmc_tstep1000[1] = sla_tmc_measure_tstep1000(1);
	}
#ifdef SLA_DBG_TMC_TSTEP
	{
		int16_t tstp_dif_tw_cents = sla_tmc_calc_cents(sla_tmc_tstep1000[0], 12000 >> (sla_mstep_resolution[0]));
		int16_t tstp_dif_ti_cents = sla_tmc_calc_cents(sla_tmc_tstep1000[1], 12000 >> sla_mstep_resolution[1]));
		cmd_err_printf_P(PSTR("tstep0=%d (%d cent)\n"), sla_tmc_tstep1000[0], tstp_dif_tw_cents);
		cmd_err_printf_P(PSTR("tstep1=%d (%d cent)\n"), sla_tmc_tstep1000[1], tstp_dif_ti_cents);
	}
#endif //SLA_DBG_TMC_TSTEP
	if (sla_mode == SLA_MODE_BOOT)
	{
#ifdef SLA_DIAG
		sla_set_mode(SLA_MODE_DIAG);
		sla_diag_state = SLA_DIAG_TSIG;
#else //SLA_DIAG
		sla_set_mode(SLA_MODE_IDLE);
		sla_diag_state = SLA_DIAG_NONE;
#endif //SLA_DIAG
	}
}

void sla_set_mode(uint8_t mode)
{
	switch (mode)
	{
	case SLA_MODE_ERRO:
		break;
	case SLA_MODE_BOOT:
		break;
	case SLA_MODE_DIAG:
		sla_set_pled(SLA_PLED_PULS);
		sla_pled_spd = 32;
		break;
	case SLA_MODE_IDLE:
		sla_set_pled(SLA_PLED_PULS);
		sla_pled_spd = 8;
		break;
	case SLA_MODE_LIVE:
		sla_set_pled(SLA_PLED_CONT);
		break;
	case SLA_MODE_SERV:
		sla_set_pled(SLA_PLED_BLNK);
		sla_pled_spd = 64;
		break;
	}
	sla_mode = mode;
}

void morse_out_L(void)
{
	snd_set_freq(0);
	sla_start_led_off();
}

void morse_out_H(void)
{
	snd_set_freq(880);
	sla_start_led_on();
}

// never-ending loop for signalizing hard errors
// signalization of this error is simple loop with disabled interrupts and watchdog
// to prevent unstable behavior because flash is maybe corrupted
void sla_set_error(uint8_t error)
{
	cmd_err_printf_P(PSTR("ERROR=%d\n"), error); //error message
	sla_mode = SLA_MODE_ERRO;
	morse_num(error);
}

void sla_shdn(uint8_t delay)
{
	if (delay == 0)
		sla_set_power(0);
	sla_shdn_timer = delay;
}

void sla_beep(uint16_t freq, uint16_t durr)
{
	snd_set_freq((float)freq);
	sla_beep_ms = durr;
}

void sla_motr(void)
{
	sla_tmc_set_ena(0);
}

int8_t sla_twup(uint16_t steps)
{
	return st4_mor(0, (int32_t)steps);
}

int8_t sla_twdn(uint16_t steps)
{
	return st4_mor(0, -((int32_t)steps));
}

int8_t sla_twma(int32_t pos)
{
	sla_tmc_set_ena(sla_tmc_get_ena() | 1);
	return st4_moa(0, pos);
}

int8_t sla_tima(int16_t pos)
{
	sla_tmc_set_ena(sla_tmc_get_ena() | 2);
	return st4_moa(1, pos);
}

void bubblesort_uint8(uint8_t* data, uint8_t size, uint8_t* data2)
{
	uint8_t i;
	uint8_t changed = 1;
	while (changed)
	{
		changed = 0;
		for (i = 0; i < (size - 1); i++)
			if (data[i] > data[i+1])
			{
				uint8_t register d = data[i];
				data[i] = data[i+1];
				data[i+1] = d;
				if (data2)
				{
					d = data2[i];
					data2[i] = data2[i+1];
					data2[i+1] = d;
				}
				changed = 1;
			}
	}
}

uint8_t clusterize_uint8(uint8_t* data, uint8_t size, uint8_t* ccnt, uint8_t* cval, uint8_t tol)
{
	uint8_t i;
	uint8_t cnt = 1;
	uint16_t sum = data[0];
	uint8_t cl = 0;
	uint8_t d;
	uint8_t val;
	uint8_t dif;
	for (i = 1; i < size; i++)
	{
		d = data[i];
		val = sum / cnt;
		dif = 0;
		if (val > d) dif = val - d;
		else dif = d - val;
		if (dif <= tol)
		{
			cnt += 1;
			sum += d;
		}
		else
		{
			if (ccnt) ccnt[cl] = cnt;
			if (cval) cval[cl] = val;
			cnt = 1;
			sum = d;
			cl += 1;
		}
	}
	if (ccnt) ccnt[cl] = cnt;
	if (cval) cval[cl] = sum / cnt;
	return ++cl;
}

uint8_t sla_home_calib[SLA_HCAL_CNT];

#ifdef SLA_DBG_HOME_CALIB
#define _DBG_HOME_CALIB(...) cmd_err_printf_P(__VA_ARGS__)
#else
#define _DBG_HOME_CALIB(...)
#endif
uint8_t sla_home_calib_calc(void)
{
	uint8_t i;
	uint8_t cl;
	uint8_t cnt[SLA_HCAL_CNT];
	uint8_t val[SLA_HCAL_CNT];
	bubblesort_uint8(sla_home_calib, SLA_HCAL_CNT, 0);
	_DBG_HOME_CALIB(PSTR("sorted samples:\n"));
	for (i = 0; i < SLA_HCAL_CNT; i++)
		_DBG_HOME_CALIB(PSTR(" i=%2d step=%2d\n"), i, sla_home_calib[i]);
	cl = clusterize_uint8(sla_home_calib, SLA_HCAL_CNT, cnt, val, 1);
	_DBG_HOME_CALIB(PSTR("clusters:\n"));
	for (i = 0; i < cl; i++)
		_DBG_HOME_CALIB(PSTR(" i=%2d cnt=%2d val=%2d\n"), i, cnt[i], val[i]);
	bubblesort_uint8(cnt, cl, val);
	_DBG_HOME_CALIB(PSTR("result value: %d\n"), val[cl-1]);
	return val[cl-1];
}
#undef _DBG_HOME_CALIB

int8_t sla_twho(void)
{
	if (st4_msk & 1) return CMD_ER_BSY;
	sla_twho_state = SLA_TWHO_START;
	sla_twho_calib = (sla_twho_phase >= 0)?0:SLA_HCAL_CNT;
	return CMD_OK;
}

int8_t sla_twhc(void)
{
	if (st4_msk & 1) return CMD_ER_BSY;
	sla_twho_state = SLA_TWHO_START;
	sla_twho_offset = -1;
	sla_twho_calib = 10;
	return CMD_OK;
}

int8_t sla_tiho(void)
{
	if (st4_msk & 2) return CMD_ER_BSY;
	sla_tiho_state = SLA_TIHO_START;
	return CMD_OK;
}

int8_t sla_tihc(void)
{
	if (st4_msk & 2) return CMD_ER_BSY;
	sla_tiho_state = SLA_TIHO_START;
	sla_tiho_phase = -1;
	return CMD_OK;
}

#ifdef SLA_DBG_HOME_TOWER
#define _DBG_HOME_TOWER(...) cmd_err_printf_P(__VA_ARGS__)
_INLINE void sla_twho_log_step(uint8_t step)
{
	_DBG_HOME_TOWER(PSTR("#twho%d- %d %ld %ld\n"), step, st4_esw & 0x01, st4_axis[0].pos, st4_axis[0].pos - sla_twho_pos0);
}
#else
#define _DBG_HOME_TOWER(...)
#define sla_twho_log_step(step)
#endif
_INLINE void sla_twho_set_IDLE(void)
{
	_DBG_HOME_TOWER(PSTR("#twho ok - IDLE\n"));
	sla_twho_state = SLA_TWHO_IDLE;
#ifdef SLA_2ND_BOARD_TEST
	st4_axis[0].pos = 0;
#endif //SLA_2ND_BOARD_TEST
}
_INLINE void sla_twho_set_EBLOCK(void)
{
	_DBG_HOME_TOWER(PSTR("#twho error EBLOCK\n"));
	sla_twho_state = SLA_TWHO_EBLOCK;
}
_INLINE void sla_twho_set_ENOEND(void)
{
	_DBG_HOME_TOWER(PSTR("#twho error ENOEND\n"));
	sla_twho_state = SLA_TWHO_ENOEND;
}

#ifdef SLA_DBG_HOME_TILT
#define _DBG_HOME_TILT(...) cmd_err_printf_P(__VA_ARGS__)
_INLINE void sla_tiho_log_step(uint8_t step)
{
	_DBG_HOME_TILT(PSTR("#tiho%d- %d %ld %ld\n"), step, st4_esw & 0x02, st4_axis[1].pos, st4_axis[1].pos - sla_tiho_pos0);
}
#else
#define _DBG_HOME_TILT(...)
#define sla_tiho_log_step(step)
#endif
_INLINE void sla_tiho_set_IDLE(void)
{
	_DBG_HOME_TILT(PSTR("#tiho ok - IDLE\n"));
	sla_tiho_state = SLA_TIHO_IDLE;
}
_INLINE void sla_tiho_set_EBLOCK(void)
{
	_DBG_HOME_TILT(PSTR("#tiho error EBLOCK\n"));
	sla_tiho_state = SLA_TIHO_EBLOCK;
}
_INLINE void sla_tiho_set_ENOEND(void)
{
	_DBG_HOME_TILT(PSTR("#tiho error ENOEND\n"));
	sla_tiho_state = SLA_TIHO_ENOEND;
}

#ifdef SLA_TISE
int8_t sla_tise(void)
{
	if (st4_msk & 1) return CMD_ER_BSY;
	sla_tise_state = SLA_TISE_START;
	return CMD_OK;
}

#ifdef SLA_DBG_SEPA_TILT
#define _DBG_SEPA_TILT(...) cmd_err_printf_P(__VA_ARGS__)
_INLINE void sla_tise_log_state(uint8_t state)
{
	static uint8_t sla_tise_log_old_state = -1;
	if (sla_tise_log_old_state == sla_tise_state) return;
	sla_tise_log_old_state = sla_tise_state;
	_DBG_SEPA_TILT(PSTR("#tise%d- %d %ld %ld\n"), state, st4_esw & 0x02, st4_axis[1].pos, st4_axis[1].pos - sla_tise_pos0);
}
#else
#define _DBG_SEPA_TILT(...)
#define sla_tise_log_state(step)
#endif
_INLINE void sla_tise_set_IDLE(void)
{
	_DBG_SEPA_TILT(PSTR("#tise ok - IDLE\n"));
	sla_tise_state = SLA_TISE_IDLE;
}
_INLINE void sla_tise_set_EBLOCK(void)
{
	_DBG_SEPA_TILT(PSTR("#tise error EBLOCK\n"));
	sla_tise_state = SLA_TISE_EBLOCK;
}
_INLINE void sla_tise_set_ENOEND(void)
{
	_DBG_SEPA_TILT(PSTR("#tise error ENOEND\n"));
	sla_tise_state = SLA_TISE_ENOEND;
}

#endif //SLA_TISE

extern _INLINE void sla_cycle_twho(void)
{
	uint32_t dist;
	switch (sla_twho_state)
	{
	case SLA_TWHO_START:
		sla_twho_offset_state = SLA_TWHO_OFFS_I;
		sla_twho_pos0 = st4_axis[0].pos;
		sla_twho_log_step(0);
		sla_twcs(0);
		sla_tmc_set_ena(sla_tmc_get_ena() | 1);
		st4_end |= 1;
		st4_mor(0, SLA_TW_RNG_HOM);
		sla_twho_state = SLA_TWHO_GOMINC;
		break;
	case SLA_TWHO_GOMINC:
		if ((st4_msk & 1) == 0)
		{
			sla_twho_log_step(1);
			if (st4_esw & 0x01)
			{
				_delay_ms(100);
				sla_twho_pos0 = st4_axis[0].pos;
				st4_mor(0, -SLA_TW_BST_HOM);
				sla_twho_state = SLA_TWHO_GOFWDC;
			}
			else
				sla_twho_set_ENOEND();
		}
		break;
	case SLA_TWHO_GOFWDC:
		if ((st4_msk & 1) == 0)
		{
			sla_twho_log_step(2);
			if ((st4_esw & 0x01) == 0)
			{
				sla_twho_pos0 = st4_axis[0].pos;
				sla_twcs(1);
				sla_twho_pos0 = st4_axis[0].pos;
				st4_mor(0, 3*SLA_TW_BST_HOM);
				sla_twho_state = SLA_TWHO_GOMINA;
			}
			else
				sla_twho_set_EBLOCK();
		}
		break;
	case SLA_TWHO_GOMINA:
		if ((st4_msk & 1) == 0)
		{
			sla_twho_log_step(3);
			if (st4_esw & 0x01)
			{
				dist = st4_axis[0].pos - sla_twho_pos0;
				switch (sla_twho_offset_state)
				{
				case SLA_TIHO_OFFS_I: //init
					printf_P(PSTR("#twho set offsetA: %d\n"), dist);
					if (sla_twho_offset >= 0)
						sla_twho_offset_state = SLA_TWHO_OFFS_M;
					else
						sla_twho_offset_state = SLA_TWHO_OFFS_C;
					sla_twho_state = SLA_TWHO_GOMINC;
					break;
				case SLA_TIHO_OFFS_C: //calib
					printf_P(PSTR("#twho set offsetB: %d\n"), dist);
					sla_twho_offset = dist;
					sla_twho_offset_state = SLA_TWHO_OFFS_M;
					sla_twho_state = SLA_TWHO_GOMINC;
					break;
				case SLA_TIHO_OFFS_M: //measure
					printf_P(PSTR("#twho offset=%d dist=%d\n"), sla_twho_offset, dist);
					if ((dist >= (sla_twho_offset - 128)) && (dist <= (sla_twho_offset + 128)))
					{
						{
							sla_twcs(0);
							sla_twho_pos0 = st4_axis[0].pos;
							st4_mor(0, -SLA_TW_BS1_HOM);
							sla_twho_state = SLA_TWHO_GOFWDF;
						}
					}
					else
					{
						sla_twho_offset = -1;
						sla_twho_set_EBLOCK();
					}
					break;
				}
			}
			else
				sla_twho_set_ENOEND();
		}
		break;
	case SLA_TWHO_GOFWDF:
		if ((st4_msk & 1) == 0)
		{
			sla_twho_log_step(4);
			if (st4_esw & 0x01)
				sla_twho_set_EBLOCK();
			else
				sla_twho_set_IDLE();
		}
		break;
	}
}
#undef _DBG_HOME_TOWER

uint8_t sla_calc_sgbuf_avg(void)
{
	uint8_t i = sla_sgbufw - sla_sgbufc;
	uint8_t c = sla_sgbufc;
	uint16_t sum = 0;
	while (c--)
		sum += sla_sgbuff[i++];
	return sum / sla_sgbufc;
}

void sla_calc_tiho_comp(void)
{
	uint8_t sg_avg = (((uint16_t)sla_tiho_comp_sgup + (uint16_t)sla_tiho_comp_sgdn)) / 2;
	sla_tiho_comp = 0;
	if ((sla_tiho_comp_sgn != 0xff) && (sla_tiho_comp_sgp != 0xff) &&
		(sla_tiho_comp_sgn != 0x00) && (sla_tiho_comp_sgp != 0x00))
	{
		if (sg_avg < sla_tiho_comp_sgn) sla_tiho_comp = -1;
		else if (sg_avg > sla_tiho_comp_sgp) sla_tiho_comp = 1;
	}
	printf_P(PSTR("#sg_avg=%hhu, comp=%hhd\n"), sg_avg, sla_tiho_comp);
}

uint8_t print_sgbuf(void)
{
	uint8_t sg_avg = sla_calc_sgbuf_avg();
	printf_P(PSTR("#sg=%hhu, cnt=%hhu\n"), sg_avg, sla_sgbufc);
	sla_sgbufc = 0;
	return sg_avg;
}

_INLINE void sla_cycle_tiho(void)
{
	switch (sla_tiho_state)
	{
	case SLA_TIHO_START:
		sla_tmc_set_ena(sla_tmc_get_ena() | 2);
		sla_tics(0);
		sla_spi_sync(SLA_SPI_REQ_GPIO_IN_A);
		if (sla_get_end3() != 0)
			sla_tiho_state = SLA_TIHO_GOMINC;
		else
			sla_tiho_state = SLA_TIHO_GOFWDS;
		break;
	case SLA_TIHO_GOFWDS:
		if ((st4_msk & 2) == 0)
		{
			st4_mor(1, -SLA_TI_RNG_HOM);
			sla_tiho_state = SLA_TIHO_GOMINC;
		}
		break;
	case SLA_TIHO_GOMINC:
		if ((st4_msk & 2) == 0)
		{
			st4_mor(1, SLA_TI_BST_HOM);
			sla_tiho_state = SLA_TIHO_GOFWDC;
		}
		break;
	case SLA_TIHO_GOFWDC:
		if ((st4_msk & 2) == 0)
		{
			if (sla_get_end3() != 0)
				sla_tiho_set_EBLOCK();	// tilt is stuck at endstop
			else
			{
				sla_tics(1);
				st4_mor(1, -2*SLA_TI_BST_HOM);
				sla_tiho_state = SLA_TIHO_GOMINA;
			}
		}
		break;
	case SLA_TIHO_GOMINA:
		if ((st4_msk & 2) == 0)
		{
			sla_spi_sync(SLA_SPI_REQ_GPIO_IN_A);
			if (sla_get_end3() != 0)
			{
				if (sla_tiho_phase < 0)
				{
					sla_tiho_phase = sla_get_phase(1);
					sla_cfg_dirty |= (1 << SLA_CFG_TIHPHA);
					sla_cfg_save();
				}
				sla_goto_fullstep(sla_tiho_phase, 1, 0);
				sla_tiho_set_IDLE();
			}
			else
				sla_tiho_set_ENOEND();
		}
		break;
	}
	return;
}
#undef _DBG_HOME_TILT

#ifdef SLA_TISE
_INLINE void sla_cycle_tise(void)
{
	sla_tise_log_state(sla_tise_state);
	switch (sla_tise_state)
	{
	case SLA_TISE_START:
		sla_tise_pos0 = st4_axis[1].pos;
		sla_tics(5);
		st4_end &= ~2;
		st4_moa(1, sla_tise_tpos);
		sla_tise_state = SLA_TISE_TRUN;
		break;
	case SLA_TISE_TRUN:
		if ((st4_msk & 2) == 0)
		{
			sla_tise_state = SLA_TISE_TWAIT;
			sla_tise_ms = sla_tise_tdel;
		}
		break;
	case SLA_TISE_TWAIT:
		if (sla_tise_ms == 0)
		{
			sla_tise_state = SLA_TISE_FRUN;
			sla_tics(4);
			st4_end |= 2;
			st4_moa(1, sla_tise_fpos);
		}
		break;
	case SLA_TISE_FRUN:
		if ((st4_msk & 2) == 0)
		{
			if (st4_axis[1].pos == sla_tise_fpos)
			{
				sla_tise_state = SLA_TISE_IDLE;
				sla_tics(0);
			}
			else
			{
				sla_tise_state = SLA_TISE_FWAIT;
				sla_tise_ms = sla_tise_fdel;
			}
		}
		break;
	case SLA_TISE_FWAIT:
		if (sla_tise_ms == 0)
		{
			sla_tise_state = SLA_TISE_TWAIT;
		}
		break;
	}
}
#endif //SLA_TISE

#undef _DBG_SEPA_TILT

void sla_cycle(void)
{
	if (sla_diag_state != SLA_DIAG_NONE)
	{
		sla_diag_cycle();
		return;
	}

	if (sla_twho_state > 0)
		sla_cycle_twho();
	if (sla_tiho_state > 0)
		sla_cycle_tiho();
#ifdef SLA_TISE
	if (sla_tise_state > 0)
		sla_cycle_tise();
#endif //SLA_TISE

#ifndef _SIMULATOR
	sla_adc_cycle();
#endif
}

int8_t sla_init_gpio(void)
{
	if (!mcp23s17_init(0x00, MCP23S17_DEF_DIR, MCP23S17_DEF_GPO)) return -1;
	sla_gpio_r[0] = mcp23s17_in_a(0x00);
	sla_gpio_r[1] = mcp23s17_in_b(0x00);
	return 0;
}

void sla_init_spwm(void)
{
	sla_spwm_dev[3].period = SLA_SPWM_DEFAULT;	//set pwm period for START-LED
	sla_spwm.enable = 0x08;						//enable START-LED

	for(uint8_t i = 0;i < SLA_FAN_NUM; i++)
	{
		if(i == REAR_FAN_IDX) {
			sla_spwm_dev[i].duty = REAR_FAN_DUTY_MAX / 2; // Start at 50% power
			sla_spwm_dev[i].period = REAR_FAN_HW_DUTY_MAX; // Rear fan uses fixed period and regulates duty cycle
		} else {
			sla_spwm_dev[i].duty = SLA_SPWM_DEFAULT;	//set pwm duty (20ms -> 1500 RPM)
			sla_spwm_dev[i].period = SLA_SPWM_DEFAULT * 3;	//set pwm period
		}
		sla_fan[i].rpm_tolerance = SLA_SPWM_TOLERANCE;
	}
}

int8_t sla_twcs(int8_t index)
{
	if (index == -1)
	{
		sla_twcs_index = index;
		return CMD_OK;
	}
	else if ((index >= 0) && (index <= 15))
	{
		sla_twcs_index = index;
		sla_cfg_load_XXcf(0, index);
		return CMD_OK;
	}
	return CMD_ER_OOR;
}

int8_t sla_tics(int8_t index)
{
	if (index == -1)
	{
		sla_tics_index = index;
		return CMD_OK;
	}
	else if ((index >= 0) && (index <= 15))
	{
		sla_tics_index = index;
		sla_cfg_load_XXcf(1, index);
		return CMD_OK;
	}
	return CMD_ER_OOR;
}

void sla_set_pled(uint8_t pled)
{
	sla_pled = pled;
	if (pled != SLA_PLED_DISA)
		sla_spwm.enable |= (1 << 3);
	else
		sla_spwm.enable &= ~(1 << 3);
}

uint16_t sla_get_phase(uint8_t axis)
{
	sla_spi_sync(SLA_SPI_REQ_TMC0_RD_MSCNT + axis * SLA_SPI_REQ_TMC0_RD_MSCNT);
	return sla_spi_tmc_msc[axis];
}

// sets stepper motor to given microstepping phase
int8_t sla_set_phase(uint8_t axis, uint16_t phase)
{
	int16_t steps;
	sla_spi_sync(SLA_SPI_REQ_TMC0_RD_MSCNT + axis * SLA_SPI_REQ_TMC0_RD_MSCNT);
	steps = sla_tmc_calc_steps_to_phase(sla_spi_tmc_msc[axis], phase, sla_mstep_resolution[axis]);
	printf_P(PSTR("#%d %u %u\n"), steps, sla_spi_tmc_msc[axis], phase);
	return sla_do_steps(axis, steps);
}

// move stepper by given number of steps (this is synchronous operation. Not used for normal axis movement)
int8_t sla_do_steps(uint8_t axis, int16_t steps)
{
	if (st4_msk & (1 << axis)) return CMD_ER_BSY;
	st4_axis[axis].pos += steps;
	if (steps < 0)
	{
		steps = -steps;
		sla_tmc_set_dir(sla_tmc_get_dir() | (1 << axis));
	}
	else
		sla_tmc_set_dir(sla_tmc_get_dir() & ~(1 << axis));
	while (steps--)
	{
		cli();
		sla_tmc_do_step(1 << axis);
		sei();
		_delay_us(1000);
	}
	return CMD_OK;
}

// move stepper to the first higher fullstep (same current in both coils A and B. In this config stepper should have biggest holding torque)
void sla_goto_fullstep(uint16_t actual_phase, uint8_t axis, uint8_t go_up)
{
	uint16_t target_phase = (256 * ((actual_phase + 128 + 256 * go_up) >> 8) - 128) - 2 * axis;
	sla_set_phase(axis, target_phase);
}

void sla_set_power_panic_status(uint8_t val)
{
	if (val)
	{
		EIMSK |= (1 << INT6);
		sla_power_panic_status = 1;
	}
	else
	{
		EIMSK &= ~(1 << INT6);
		sla_power_panic_status = 0;
	}
}

ISR(INT6_vect)
{
	uint32_t timer = 0;
	timer0_ms = 0;
	EIMSK &= ~(1 << INT6);
	sei();
	sla_tmc_set_ena(0);
	sla_set_fan(0);
	sla_set_led(0);
	sla_beep(2000, 2000);
	cmd_err_printf_P(PSTR("#INT6\n"));
	while (1)
	{
		cmd_err_printf_P(PSTR("%ld\n"), timer);
		if (sla_power_panic_status)
		{
			if (sla_power_panic_status < 255)
			{
				eeprom_write_byte((uint8_t*)(0x3ff), sla_power_panic_status);
				sla_power_panic_status++;
			}
		}
		timer += 10;
		while (timer > timer0_ms);
	}
}

void sla_get_axis_config(uint8_t axis, sla_axis_config_t* pcfg)
{
	pcfg->st4.sr0 = st4_axis[axis].sr0;
	pcfg->st4.srm = st4_axis[axis].srm;
	pcfg->st4.acc = st4_axis[axis].acc;
	pcfg->st4.dec = st4_axis[axis].dec;
	pcfg->tmc.cur = tmc2130_get_cur(axis);
	pcfg->tmc.sgt = tmc2130_get_sgt(axis);
	pcfg->tmc.cst = tmc2130_get_cst(axis);
}

void sla_set_axis_config(uint8_t axis, sla_axis_config_t* pcfg)
{
	sla_tmc_wait_standstill(axis);
	st4_axis[axis].sr0 = pcfg->st4.sr0;
	st4_axis[axis].srm = pcfg->st4.srm;
	st4_axis[axis].acc = pcfg->st4.acc;
	st4_axis[axis].dec = pcfg->st4.dec;
	st4_calc_acdc(axis);
	sla_spi_tmc_cur[axis] = pcfg->tmc.cur;
	sla_spi_tmc_sgt[axis] = pcfg->tmc.sgt;
	sla_spi_tmc_cst[axis] = pcfg->tmc.cst;
	if (axis == 0)
		sla_spi_sync(SLA_SPI_REQ_TMC0_SET_CUR | SLA_SPI_REQ_TMC0_SET_SGT | SLA_SPI_REQ_TMC0_SET_CST);
	else if (axis == 1)
		sla_spi_sync(SLA_SPI_REQ_TMC1_SET_CUR | SLA_SPI_REQ_TMC1_SET_SGT | SLA_SPI_REQ_TMC1_SET_CST);
}

void sla_save_axis_config(uint8_t axis)
{
	if (axis == 0)
		sla_cfg_dirty |= (1 << SLA_CFG_TWCFGN);
	else if (axis == 1)
		sla_cfg_dirty |= (1 << SLA_CFG_TICFGN);
	sla_cfg_save();
}

uint8_t sla_get_tmc_flags(uint8_t axis)
{
	uint8_t flags = 0;
	printf_P(PSTR("#spi_status[%d]=%02hhx\n"), axis, tmc2130_spi_status[axis]);
	printf_P(PSTR("#drv_status[%d]=%08lx\n"), axis, tmc2130_spi_status[axis]);
	if (tmc2130_spi_status[axis] & 0x03) flags |= SLA_TMC_ERR; //error flag
	if (sla_spi_tmc_drv[axis] & 0x04000000) flags |= SLA_TMC_OTW; //over-temperature warning
	if (sla_spi_tmc_drv[axis] & 0x02000000) flags |= SLA_TMC_OTE; //over-temperature error
	if (axis == 0)
	{
		printf_P(PSTR("#olicnt_tower=%hhu\n"), sla_olicnt_tower);
		if (sla_olicnt_tower >= SLA_OLICNT_MAX)
			flags |= SLA_TMC_OLI;
	}
	else if (axis == 1)
	{
		printf_P(PSTR("#olicnt_tilt=%hhu\n"), sla_olicnt_tilt);
		if (sla_olicnt_tilt >= SLA_OLICNT_MAX)
			flags |= SLA_TMC_OLI;
	}
	return flags;
}

_INLINE void isr_tmc(void) // runs every ms
{
	uint8_t axis = 1; //default is tilt
	uint16_t sg[2];
	uint8_t tmc_ena;
	if (sla_tmc_timer == 0)
	{ //start of cycle (timer == 0)
		//request drv_status for both axes and gpio input A
		sla_spi_req_mask |= SLA_SPI_REQ_TMC0_RD_DRV_STATUS | SLA_SPI_REQ_TMC1_RD_DRV_STATUS | SLA_SPI_REQ_GPIO_IN_A;
		sla_tmc_timer = SLA_TMC_DELAY + 1;
	}
	if (st4_msk & 0x03) //motion
	{
		if ((st4_msk & 0x02) == 0) axis = 0; //select tower if tilt not moving
		if (sla_tmc_timer == 2)
		{ //in this phase check open load indicators
			tmc_ena = sla_tmc_get_ena();
			if (tmc_ena & 1)
			{
				if ((sla_spi_tmc_drv[0] & 0xe0000000) == 0x60000000)
				{
					if (sla_olicnt_tower < 20)
						sla_olicnt_tower++;
				}
				else
					if (sla_olicnt_tower) sla_olicnt_tower--;
			}
			if (tmc_ena & 2)
			{
				if ((sla_spi_tmc_drv[1] & 0xe0000000) == 0x60000000)
				{
					if (sla_olicnt_tilt < 20)
						sla_olicnt_tilt++;
				}
				else
					if (sla_olicnt_tilt) sla_olicnt_tilt--;
			}
		}
		else if (sla_tmc_timer == 1)
		{ //end of cycle (timer == 1) - check auc endstops
			sg[0] = sla_spi_tmc_drv[0] & 0x3ff;
			sg[1] = sla_spi_tmc_drv[1] & 0x3ff;
			sla_sgbuff[sla_sgbufw++] = (uint8_t)(sg[axis] >> 2);
			if (sla_sgbufc < 255)
				sla_sgbufc++;
			if ((st4_msk & 0x11) == 0x01) //tower is moving up
			{
#ifdef SLA_TW_AUX_END
				if ((sla_twcs_index == 0) && (sg[0] < 32) && (st4_axis[0].cac == 0))
				{ //profile 0 selected, stallguard < 32, motor is running at max speed
					sla_tmc_end_aux = 1;
					sla_beep(1000, 500);
				}
#endif //SLA_TW_AUX_END
			}
			if ((st4_msk & 0x22) == 0x22) //tilt is moving down
			{
#ifdef SLA_TI_AUX_END
				if ((sla_tics_index == 0) && (sg[1] < 32) && (st4_axis[1].cac == 0))
				{ //profile 0 selected, stallguard < 32, motor is running at max speed
					sla_tmc_end_aux = 2;
					sla_beep(1000, 500);
				}
#endif //SLA_TI_AUX_END
#ifdef SLA_TI_OPT_END
				if (sla_get_end3())
				{
					sla_tmc_end_aux = 2;
				}
#endif //SLA_TI_OPT_END
			}
		}
	}
	else //no motion
	{
		sla_tmc_end_aux = 0;
	}
	sla_tmc_timer--;
}

_INLINE void isr_pwm(void) // runs every ms.
{

	if (sla_spwm_dev[3].on_index == sla_spwm.index) // control START-LED pwm
	{
		switch (sla_pled)
		{
			//case SLA_PLED_DISA: in this case spwm device is disabled
			case SLA_PLED_CONT:
				sla_spwm_dev[3].duty = sla_pled_pwm;
				break;
			case SLA_PLED_PULS:
				if (sla_pled_pha > (SLA_SPWM_DEFAULT << 4))
					sla_spwm_dev[3].duty = 2 * SLA_SPWM_DEFAULT - (sla_pled_pha >> 4);
				else
					sla_spwm_dev[3].duty = (sla_pled_pha >> 4);
				break;
			case SLA_PLED_BLNK:
				if (sla_pled_pha > (SLA_SPWM_DEFAULT << 4))
					sla_spwm_dev[3].duty = sla_pled_pwm;
				else
					sla_spwm_dev[3].duty = 0;
				break;
		}
		sla_pled_pha += sla_pled_spd;
		if (sla_pled_pha > ((2 * SLA_SPWM_DEFAULT) << 4))
			sla_pled_pha = 0;
	}

	for (uint8_t i = 0; i < SLA_SPWM_DEV;i++)
	{
		if (sla_spwm.enable & (1 << i))
		{
			if (sla_spwm_dev[i].on_index == sla_spwm.index) //turn output ON
			{
				sla_spwm.cache |= (1 << i);
				sla_spwm.on_flag |= (1 << i);
				sla_spwm_dev[i].on_index += sla_spwm_dev[i].period; // when output will be turned back on
				if(i == REAR_FAN_IDX) {
					// Rear fan has different duty cycle handling
					// Leveraging dedicated PWM channel this uses much faster PWM ~ 125KHz
					// As a side effect the real PWM has just 8 levels. In order to provide smoother control
					// this stores rounding error 256 levels -> 8 levels and applies the error next time.
					// This effectively implements temporal dithering while realying on fan momentum to further
					// smooth the movement.
					uint8_t duty = sla_spwm_dev[i].duty + sla_spwm_dev[i].duty_error;
					if(duty > REAR_FAN_DUTY_MAX) {
						duty = REAR_FAN_DUTY_MAX;
					}
					sla_spwm_dev[i].duty_error = duty % REAR_FAN_DUTY_SCALE;
					sla_spwm_dev[i].off_index = sla_spwm.index + duty / REAR_FAN_DUTY_SCALE; // when output will be turned off
				} else {
					sla_spwm_dev[i].off_index = sla_spwm.index + sla_spwm_dev[i].duty;	// when output will be turned off
				}
			}
			if (sla_spwm_dev[i].off_index == sla_spwm.index) //turn output OFF
			{
				sla_spwm.cache &= ~(1 << i);
			}
		}
		else
		{
			sla_spwm.cache &= ~(1 << i);
		}
	}

	// FAN3-PWM - Control dedicated PWM gpio (this is 4pin fan)
	if (sla_spwm.cache & 1 << REAR_FAN_IDX)
		PORTF &= ~0x80;
	else
		PORTF |= 0x80;

	if (sla_spwm.cache & 0x08) //START-LED
		PORTB |= 0x80;
	else
		PORTB &= ~0x80;


	// send data from cache to GPIOs
	// Set standard power on as power is controlled by dedicated PWM signal
	sla_gpio[0] = (sla_gpio[0] & SLA_MSK_GPIO0) | ((sla_spwm.cache | (sla_spwm.enable & (1 << REAR_FAN_IDX))) << 4);
	sla_spi_async(SLA_SPI_REQ_GPIO_OUT_A);
	sla_spwm.index++;
}

_INLINE void isr_fan_rpm(void) // runs every ms
{
	uint8_t raw_tach = sla_get_tach(); //get actual tacho signal
	uint8_t edge = 0;

	edge = (sla_fans.prev_tach ^ raw_tach) & sla_spwm.cache; //count both edges
	sla_fans.prev_tach = raw_tach; //store current state for next time
	for (uint8_t i = 0; i < SLA_FAN_NUM; i++)
	{
		if (i == REAR_FAN_IDX) {
			// Rear fan has dedicated RPM control (as it is 4pin fan)
			if (edge & (1 << i)) {
				// Just count edges, rear fan has constant power
				sla_fan[i].tacho_counter++;
			}
		} else {
			if (sla_spwm.on_flag & (1 << i)) //beginning of spwm pulse
			{
				sla_spwm.on_flag &= ~(1 << i);
				sla_fan[i].prev_edge_index = 0; //clear edge index
				continue; //first timeslot in spwm pulse only store sla_fans.prev_tach to avoid incorrect measurement
			}

			if (edge & (1 << i)) //if fan has edge in tacho
			{
				if(sla_fan[i].prev_edge_index != 0) //compute techo only if we have previous index stored
				{
					sla_fan[i].tacho_buffer += sla_spwm.index - sla_fan[i].prev_edge_index;
					sla_fan[i].tacho_counter++; //count number of edges
				}
				sla_fan[i].prev_edge_index = sla_spwm.index; // store actual timeslot so we can calculate tacho period at next edge
			}
		}
	}
}

inline void isr_fan_adjust_rpm(void) // runs every second
{
	for (uint8_t i = 0; i < SLA_FAN_NUM; i++)
	{
		if (sla_spwm.enable & (1 << i))	//if actual fan is enabled
		{
			if(i == REAR_FAN_IDX) {
				// Rear fan has dedicated RPM control (as it is 4pin fan)
				const uint16_t rpm = 60 / 2 * sla_fan[i].tacho_counter; // RPS -> RPM, 2 pulses per revolution
				sla_fan[i].rpm = 100 * (rpm / 100);

				// TODO: Maybe replacing this with PID controller might increase RPM stability.
				int16_t error = sla_fan[i].target_rpm - rpm;
				int16_t new_duty = sla_spwm_dev[i].duty + error / REAR_FAN_RPM_TO_DUTY_DIV;
				if(new_duty < 0) {
					sla_spwm_dev[i].duty = 0;
				} else if (new_duty > REAR_FAN_DUTY_MAX) {
					sla_spwm_dev[i].duty = REAR_FAN_DUTY_MAX;
					if (!sla_fans.fane_delay && (sla_fan[i].rpm < sla_fan[i].rpm_tolerance || sla_fan[i].tacho_counter == 0))
						sla_fans.fane |= (1 << i);
				} else {
					sla_spwm_dev[i].duty = new_duty;
				}
				sla_fan[i].tacho_counter = 0;
			} else {
				if (sla_fan[i].tacho_counter > 0 && (~sla_fans.fane & (1 << i))) //compute tacho only if we counted any in previous period
				{
					sla_fan[i].tacho = ((sla_fan[i].tacho + ((sla_fan[i].tacho_buffer << 1) / sla_fan[i].tacho_counter)) >> 1);
					sla_fan[i].rpm = 30000 / sla_fan[i].tacho;

					// Adjust spwm period to spin fan at target rpm
					if (sla_spwm_dev[i].duty > sla_fan[i].tacho)
						sla_spwm_dev[i].period += (sla_spwm_dev[i].duty - sla_fan[i].tacho);
					else if (sla_spwm_dev[i].duty < sla_fan[i].tacho)
						sla_spwm_dev[i].period -= (sla_fan[i].tacho - sla_spwm_dev[i].duty);

					sla_fan[i].prev_edge_index = 0;
					sla_fan[i].tacho_counter = 0;
					sla_fan[i].tacho_buffer = 0;
				} else {
					sla_spwm_dev[i].period -= sla_spwm_dev[i].period >> 1; //when got no tacho, increase duty cycle
				}

				if (sla_spwm_dev[i].period < sla_spwm_dev[i].duty) //100% spwm and fan still lower rpm, results in fane
				{
					if (!sla_fans.fane_delay && (sla_fan[i].tacho > sla_fan[i].rpm_tolerance || sla_fan[i].tacho_counter == 0))
						sla_fans.fane |= (1 << i);
					sla_spwm_dev[i].period = sla_spwm_dev[i].duty;
				}
			}
		}
	}
	if (sla_fans.fane_delay) {
		sla_fans.fane_delay--;
	}
}

_INLINE void isr_error(void)
{
	morse_cycle();
}

void sla_critical_overtemp(void)
{
	sla_gpio[0] = 0b11111111;	// disable motors, enable fans
	mcp23s17_out_a(0x00, sla_gpio[0]);
	sla_gpio[1] = 0b00000000;	// disable UV LED
	mcp23s17_out_b(0x00, sla_gpio[1]);
	PORTF &= ~0x80;				// set pin for FAN3
	sla_set_error(SLA_ERROR_OVTU);
}

// Timer 0 is shared with millies
ISR(TIMER0_COMPA_vect)
{
	int16_t uvtemp = sla_temp[0] > sla_temp[2] ? sla_temp[0] : sla_temp[2];

	if (sla_mode == SLA_MODE_ERRO)
	{
		isr_error();
		return; //in error mode skip all pwm and spi processing
	}

	if (sla_ms++ >= 1000) // runs once per second
	{
		sla_ms = 0;
		if (uvtemp > 700 && sla_mode != SLA_MODE_ERRO)	// shutdown everything if LED has over 70 °C
		{
			sla_critical_overtemp();
			return;
		}
		sei();
		if (sla_shdn_timer)
			if (--sla_shdn_timer == 0)
				sla_set_power(0);
		if (sla_mode == SLA_MODE_LIVE)
			if (sla_live_timer)
			{
				if (--sla_live_timer == 0)
					sla_set_mode(SLA_MODE_IDLE);
			}
		if (sla_get_start_button())
		{
			sla_shdn_timer2++;
			if (sla_shdn_timer2 > 5)
			{
				sla_beep(220, 100);
				sla_set_power(0);
			}
		}
		else
			sla_shdn_timer2 = 0;
		sla_spi_async(SLA_SPI_REQ_GPIO_IN_B);
		if (sla_leds & SLA_MSK_ULED)	// if uv led is on
			sla_uled.uvled_counter++;
		if (sla_uled.display_counter_mask || sla_leds & SLA_MSK_ULED) // if UV LED is on, or display_counter_mask is set (usually set on print start)
			sla_uled.display_counter++;
#ifndef _SIMULATOR
		isr_fan_adjust_rpm();
#endif //_SIMULATOR
	}
	else
		sei();

	sys_sp_check_min();

	if (sla_uled.ms)
	{
		if (--sla_uled.ms == 0)
		{
			sla_leds &= ~SLA_MSK_ULED;
			sla_gpio[1] &= ~0x03;
			sla_spi_async(SLA_SPI_REQ_GPIO_OUT_B);
		}
	}

	if (sla_wdel_ms)
	{
		sla_wdel_ms--;
	}

#ifdef SLA_TISE
	if (sla_tise_ms)
		--sla_tise_ms;
#endif //SLA_TISE

	if (sla_beep_ms)
	{
		if (--sla_beep_ms == 0)
			snd_off();
	}
	isr_tmc();

#ifndef _SIMULATOR
	isr_fan_rpm();	//measure fans rpm
#endif //_SIMULATOR
	isr_pwm();		//soft PWM

	sla_spi_cycle();

	if (sla_adc_timer)
		sla_adc_timer--;
	else
	{
		sla_adc_timer = ADC_DELAY;
		adc_cycle();
	}
	sla_rsens_cycle();
}

void sla_set_uled_pwm(uint8_t upwm)
{
	sla_uled.pwm = upwm;
	if ((sla_uled.pwm == 0) || (sla_uled.pwm == SLA_UPWM_MAX))
	{
		if (sla_uled.pwm == 0)
			PORTD &= ~0x01; //uv led pwm off
		else
			PORTD |= 0x01; //uv led pwm on
		TCCR0A &= ~(3 << COM0B0); //disable OCR0B output
	}
	else
	{
		OCR0B = (256 - TIMER0_CYC_1MS) + sla_uled.pwm; //set OCR0B register
		TCCR0A |= (2 << COM0B0); //enable OCR0B output. Maybe this should be done in OVF isr
	}
}
