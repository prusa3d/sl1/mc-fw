/*
 * uart functions used for communication with A64
 */
#include "uart.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "rbuf.h"

#define UART_BAUD_SELECT(baudRate,xtalCpu) ((uint8_t)(((float)(xtalCpu))/(((float)(baudRate))*8.0)-1.0+0.5))

#define uart1_rxcomplete (UCSR1A & (1 << RXC1))
#define uart1_txcomplete (UCSR1A & (1 << TXC1))
#define uart1_txready    (UCSR1A & (1 << UDRE1))

#ifdef UART1_IBUF
uint8_t uart1_ibuf[UART1_IBUF];
#endif //UART1_IBUF

#ifdef UART1_OBUF
uint8_t uart1_obuf[UART1_OBUF];
#endif //UART1_OBUF

#ifdef UART1_FILE
FILE _uart1io = {0};
FILE* uart1io = &_uart1io;
int uart1_putchar(char c, FILE *stream)
{
	return uart1_tx(c);
}
int uart1_getchar(FILE *stream)
{
	return uart1_rx();
}
#endif //UART1_FILE

void uart1_init(void)
{
	UCSR1A |= (1 << U2X1); // baudrate multiplier
	UBRR1L = UART_BAUD_SELECT(UART1_BAUD, F_CPU); // select baudrate
	UCSR1B = (1 << RXEN1) | (1 << TXEN1); // enable receiver and transmitter
#ifdef UART1_IBUF
	UCSR1B |= (1 << RXCIE1); // enable rx interrupt
#endif //UART1_IBUF
#ifdef UART1_OBUF
//	UCSR1B |= (1 << TXCIE1);
#endif //UART1_OBUF	

#ifdef UART1_IBUF
	rbuf_ini(uart1_ibuf, UART1_IBUF - 4);
#endif //UART0_IBUF

#ifdef UART1_OBUF
	rbuf_ini(uart1_obuf, UART1_OBUF - 4);
#endif //UART1_OBUF

#ifdef UART1_FILE
	fdev_setup_stream(uart1io, uart1_putchar, uart1_getchar, _FDEV_SETUP_WRITE | _FDEV_SETUP_READ); //setup uart1 i/o stream
#endif //UART1_FILE

}


int uart1_rx(void)
{
#ifdef UART1_IBUF
#ifdef UART1_INBL
	if (rbuf_empty(uart1_ibuf)) return -1; // for non blocking mode return -1
#else //UART1_INBL
	while (rbuf_empty(uart1_ibuf)); // wait until byte available
#endif //UART1_INBL
	return rbuf_get(uart1_ibuf);
#else //UART1_IBUF
#ifdef UART1_INBL
	if (!uart1_rxcomplete) return -1; // for non blocking mode return -1
#else //UART1_INBL
	while (!uart1_rxcomplete); // wait until byte available
#endif //UART1_INBL
	UCSR1A |= (1 << RXC1); // delete RXCflag
	return UDR1; // receive byte
#endif //UART1_IBUF
}

int uart1_tx(uint8_t c)
{
#ifdef UART1_OBUF
	if (UCSR1B & (1 << TXCIE1))
	{
#ifdef UART1_ONBL
		if (rbuf_put(uart1_obuf, c) < 0) return -1;
#else //UART1_ONBL
		while (rbuf_put(uart1_obuf, c) < 0);
#endif //UART1_ONBL
	}
	else
	{
		UCSR1B |= (1 << TXCIE1); //enable tx interrupt
		UDR1 = c; //transmit the byte
	}
#else //UART1_OBUF
#ifdef UART1_ONBL
	if (!uart1_txready) return -1; // for non blocking mode return -1
	UDR1 = c; // transmit byte
#else //UART1_ONBL
	UDR1 = c; // transmit byte
	while (!uart1_txcomplete); // wait until byte sent
	UCSR1A |= (1 << TXC1); // delete TXCflag
#endif //UART1_ONBL
#endif //UART1_OBUF
	return 0;
}


#ifdef UART1_IBUF

#ifdef UART1_RFUL
const char rx_full_msg[] PROGMEM = "ERROR: rx buffer overflow\n";
void uart1_report_rx_full(void)
{
	uint8_t i;
	cli();
	for (i = 0; i < sizeof(rx_full_msg); i++)
	{
		while (!uart1_txready); // wait for ready to next char
		UDR1 = pgm_read_byte(rx_full_msg + i); //transmit the byte
		while (!uart1_txcomplete); // wait until byte sent
	}
	sei();
}
#endif //UART1_RFUL

ISR(USART1_RX_vect)
{
//	uart1_tx(UDR1);
	if (rbuf_put(uart1_ibuf, UDR1) < 0) // put received byte to buffer
	{ //rx buffer full
#ifdef UART1_RFUL //report rx full
		uart1_report_rx_full();
#endif //UART1_RFUL
		//clear buffer
		uart1_ibuf[1] = 0;
		uart1_ibuf[2] = 0;
	}
}
#endif //UART1_IBUF

#ifdef UART1_OBUF
ISR(USART1_TX_vect)
{
	int c = rbuf_get(uart1_obuf);
	if (c >= 0) 
		UDR1 = c; // transmit next byte from buffer
	else
		UCSR1B &= ~(1 << TXCIE1); // disable tx interrupt (used as tx_inprogress flag)
}
#endif //UART1_OBUF
