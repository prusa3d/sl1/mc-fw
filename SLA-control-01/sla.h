#ifndef _SLA_H
#define _SLA_H
#include <inttypes.h>
#include <stdio.h>
#include "config.h"
#include "st4.h"

#define SLA_MSK_PLED 0x01 // power led (start led)
#define SLA_MSK_ULED 0x02 // uvled
#define SLA_MSK_RLED 0x10 // rxled led (usb rx led)
#define SLA_MSK_TLED 0x20 // txled led (usb tx led)
#define SLA_MSK_GPIO0 0x8F

#define SLA_FANS_MSK	0x03 // [1] blower fan, [0] UV led fan

#define SLA_MODE_ERRO 0xff //error (fatal error mode)
#define SLA_MODE_BOOT 0x00 //boot (inside setup function)
#define SLA_MODE_DIAG 0x01 //diag (self diagnostic in progress)
#define SLA_MODE_IDLE 0x02 //idle (no requests from master)
#define SLA_MODE_LIVE 0x03 //live (communication is live)
#define SLA_MODE_SERV 0x04 //service (service mode for debugging and production)

#define SLA_LIFE_TIME    5 //timeout [s] to switch from LIVE to IDLE

#define SLA_STATE_TWMO 0x0001 //tower is moving
#define SLA_STATE_TIMO 0x0002 //tilt is moving
#define SLA_STATE_SBUT 0x0040 //start button
#define SLA_STATE_COVS 0x0080 //cover switch
#define SLA_STATE_END3 0x0100 //END3
#define SLA_STATE_FRST 0x2000 //any flag set in MCUSR (use '?rst' command to read and reset mcusr)
#define SLA_STATE_FANE 0x4000 //fan error (no feedback)
#define SLA_STATE_FERR 0x8000 //fatal error (fatal error mode)

#define SLA_ERROR_NONE 0x00 //(0) no error - internal diagnostic successfull
#define SLA_ERROR_CHSA 0x01 //(1) application flash checksum error
#define SLA_ERROR_CHSB 0x02 //(2) bootloader flash checksum error
#define SLA_ERROR_SNNS 0x03 //(3) serial number not set
#define SLA_ERROR_FUSE 0x04 //(4) invalid fuses settings
#define SLA_ERROR_LOCK 0x05 //(5) bootsection not locked
#define SLA_ERROR_GSPI 0x06 //(6) GPIO SPI communication error
#define SLA_ERROR_TSPI 0x07 //(7) TMC SPI communication error
#define SLA_ERROR_TSIG 0x08 //(8) TMC wirring error (step, dir, ena)
#define SLA_ERROR_OVTU 0x09 //(9) UVled over temperature error
#define SLA_ERROR_CNT  0x10	// number of errors
//#define SLA_ERROR_ADCV 0x09 //(9) ADC voltage error - out of range
//#define SLA_ERROR_THER 0x0a //(10) Thermistor error - shortcut/disconnected
//#define SLA_ERROR_MOTO 0x0b //(11) Motor error - shortcut/disconnected
//#define SLA_ERROR_FANS 0x0c //(12) Fan error - no feedback
//#define SLA_ERROR_ULED 0x0d //(13) UVled error - not connected
//#define SLA_ERROR_OVTT 0x0e //(14) TMC over temperature error

#define SLA_SN_WRONG   0xFF //serial number in wrong format (maybe not written in EEPROM at all)

//power led modes
#define SLA_PLED_DISA  0x00   //disabled - off
#define SLA_PLED_CONT  0x01   //continuous light @ppwm
#define SLA_PLED_PULS  0x02   //triangle pulsing
#define SLA_PLED_BLNK  0x03   //blinking

//SLA tmc2130 flags
#define SLA_TMC_ERR    0x80   //error
#define SLA_TMC_OTE    0x40   //over-temperature error
#define SLA_TMC_OTW    0x20   //over-temperature warning
#define SLA_TMC_OLI    0x10   //open-load indicated

static const uint8_t REAR_FAN_IDX = 2;
static const uint8_t REAR_FAN_DUTY_MAX = 128;
static const uint8_t REAR_FAN_HW_DUTY_MAX = 8;
static const uint8_t REAR_FAN_DUTY_SCALE = 16;
static const uint8_t REAR_FAN_RPM_TO_DUTY_DIV = 120;

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern uint8_t sla_time_s;
extern uint8_t sla_time_m;
extern uint8_t sla_time_h;
extern uint16_t sla_ms;
extern uint8_t sla_shdn_timer;
extern uint8_t sla_shdn_timer2;
extern uint8_t sla_leds;
extern uint8_t sla_pled;
extern uint8_t sla_pled_pwm;
extern uint16_t sla_pled_pha;
extern uint8_t sla_pled_spd;
extern uint8_t sla_log;
extern uint8_t sla_sgbuff[256];
extern uint8_t sla_sgbufw;
extern uint8_t sla_sgbufc;
extern uint8_t sla_olicnt_tower;
extern uint8_t sla_olicnt_tilt;
extern volatile uint16_t sla_wdel_ms;
extern uint8_t sla_power_panic_status;
extern uint8_t sla_gpio[2];
extern volatile uint8_t sla_mode;
extern volatile uint8_t sla_live_timer;
extern volatile uint16_t sla_states;
extern uint8_t sla_sn[16];
extern int8_t sla_twcs_index;
extern int8_t sla_tics_index;
extern uint8_t sla_mstep_resolution[TMC2130_NUMAXES];
extern uint16_t sla_beep_ms;
extern uint8_t sla_tach;
extern uint16_t sla_tach_time;
extern uint8_t sla_tach_edge[SLA_FAN_NUM];
extern int16_t sla_twho_phase;
extern int16_t sla_twho_offset;
extern int8_t sla_twho_state;
extern int16_t sla_tiho_phase;
extern int16_t sla_tiho_offset;
extern int8_t sla_tiho_state;
extern uint8_t sla_tiho_comp_sgp;
extern uint8_t sla_tiho_comp_sgn;
extern uint8_t sla_gpio_r[2];

//structure for tmc axis configuration
#pragma pack(push)
#pragma pack(1)
typedef struct
{
	uint8_t  cur;              //current (1..63)
	uint8_t  sgt;              //stallguard threshold (-128..127)
	uint16_t cst;              //coolstep threshold
} tmc_axis_config_t;
#pragma pack(pop)

//structure for sla axis configuration
#pragma pack(push)
#pragma pack(1)
typedef struct
{
	st4_axis_config_t st4;     //st4 configuration (speed, accel ...)
	tmc_axis_config_t tmc;     //trinamic configuration (current, stallguard ...)
	uint32_t reserved;
} sla_axis_config_t;
#pragma pack(pop)

typedef struct
{
	uint16_t tacho_counter;				// counts tacho pulses between rpm adjustments
	uint16_t tacho;						// holds computed tacho period
	uint16_t rpm;						// used to report rpm by ?frpm
	uint16_t target_rpm;
	uint16_t tacho_buffer;						// holds computed tacho period. Used to report rpm by ?frpm
	uint16_t prev_edge_index;			// sla_spwm.index when previous edge was detected
	uint8_t rpm_tolerance;				// allow fan to have ~10 % lower RPM without throwing fane
} sla_fan_t;
extern sla_fan_t sla_fan[SLA_FAN_NUM];

typedef struct
{
	uint8_t fane;						// fan error status
	uint8_t fane_delay;					// delay before setting fane when duty is 100%
	uint8_t fmsk;						// fan error mask (not used)
	uint8_t prev_tach;					// stores raw tacho input from last timeslot
} sla_fans_state_t;
extern sla_fans_state_t sla_fans;

typedef struct
{
	uint16_t index;						// determines actual timeslot
	uint8_t cache;						// [7:4] reserved, [3] START-LED, [2] FAN3 rear, [1] FAN2 blower, [0] FAN1 uv
	uint8_t enable;						// [7:4] reserved, [3] START-LED, [2] FAN3 rear, [1] FAN2 blower, [0] FAN1 uv
	uint8_t on_flag;					// flag is set everytime spwm goes high
} sla_spwm_state_t;
extern sla_spwm_state_t sla_spwm;

typedef struct
{
	uint16_t period;					// pwm period. this parameter is changed to get proper duty cycle
	uint16_t on_index;					// timeslot when spwm output goes high
	uint16_t off_index;					// timeslot when spwm output goes low
	uint8_t duty;						// this parameter needs to have fixed width for fans to get full tacho period
	uint8_t duty_error;					// used on rear fan for temporal dithering
} sla_spwm_dev_t;
extern sla_spwm_dev_t sla_spwm_dev[SLA_SPWM_DEV];	//fans and power led

typedef struct
{
	uint8_t pwm;					// pwm to UV led driver which defines current
	uint32_t ms;		 			// time to hold UV led on
	uint32_t uvled_counter;			// uptime counter
	uint32_t display_counter;		// exposure display uptime counter
	uint8_t display_counter_mask;	// mask for start/stop incrementing exposure display uptime counter
} sla_uled_t;
extern sla_uled_t sla_uled;

extern void sla_init(void);
extern void sla_set_mode(uint8_t mode);
extern const char* sla_error_str_P(uint8_t error);
extern void sla_set_error(uint8_t error);
extern void sla_shdn(uint8_t delay);
extern void sla_beep(uint16_t freq, uint16_t durr);
extern void sla_motr(void);
extern int8_t sla_twup(uint16_t steps);
extern int8_t sla_twdn(uint16_t steps);
extern int8_t sla_twma(int32_t steps);
extern int8_t sla_tima(int16_t steps);
extern int8_t sla_twho(void);
extern int8_t sla_twhc(void);
extern int8_t sla_tiho(void);
extern int8_t sla_tihc(void);
extern void sla_cycle(void);
extern int8_t sla_init_gpio(void);
extern void sla_init_spwm(void);
extern uint8_t sla_get_power(void);
extern void sla_set_power(uint8_t power);
extern uint8_t sla_get_end3(void);
extern uint8_t sla_get_start_button(void);
extern uint8_t sla_get_cover_switch(void);
extern void sla_init_leds(void);
extern void sla_start_led_on(void);
extern void sla_start_led_off(void);
extern void sla_set_led(uint8_t mask);
extern uint8_t sla_get_tach(void);
extern void sla_set_fan(uint8_t mask);
extern int8_t sla_twcs(int8_t index);
extern int8_t sla_tics(int8_t index);
extern void sla_set_uled_pwm(uint8_t upwm);
extern void sla_set_pled(uint8_t pled);
extern uint16_t sla_get_phase(uint8_t axis);
extern int8_t sla_set_phase(uint8_t axis, uint16_t phase);
extern int8_t sla_do_steps(uint8_t axis, int16_t steps);
void sla_goto_fullstep(uint16_t actual_phase, uint8_t axis, uint8_t go_up);
extern void sla_set_power_panic_status(uint8_t val);
extern void sla_get_axis_config(uint8_t axis, sla_axis_config_t* pcfg);
extern void sla_set_axis_config(uint8_t axis, sla_axis_config_t* pcfg);
extern void sla_save_axis_config(uint8_t axis);
extern uint8_t sla_get_tmc_flags(uint8_t axis);

#ifdef SLA_TISE
extern int8_t sla_tise_state;
extern int16_t sla_tise_tpos;
extern uint16_t sla_tise_tdel;
extern int16_t sla_tise_fpos;
extern uint16_t sla_tise_fdel;
extern uint16_t sla_tise_ms;
extern int8_t sla_tise(void);
#endif //SLA_TISE

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_H
