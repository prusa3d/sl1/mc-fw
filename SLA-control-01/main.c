/*
 * main
 */
#include "main.h"
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include "sys.h"
#include "uart.h"
#include "cmd.h"
#include "sla.h"
#include "timer0.h"

#if defined(__unix__) && defined(_SIMULATOR)
#include <termios.h>
#include <string.h>
#endif

//initialization after reset
void setup(void)
{
	sys_init();
	sei();
	sla_set_power(1);
	uart1_init(); //uart1
	PORTD |= 0x04; //enable RX pin pullup (PD2)

#ifdef _SIMULATOR
	cmd_in = stdin;
	cmd_out = stdout;
#ifdef __unix__
	struct termios terminal;
	memset(&terminal, 0, sizeof(struct termios));
	tcgetattr(0, &terminal);
	terminal.c_lflag &= ~ICANON;
	tcsetattr(0, TCSANOW, &terminal);
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);
#endif
	cmd_err = stderr;
#else //_SIMULATOR
	stdin = uart1io; // stdin = uart1
	stdout = uart1io; // stdout = uart1
	cmd_in = uart1io;
	cmd_out = uart1io;
	cmd_err = uart1io;
#endif //_SIMULATOR

#ifndef SLA_2ND_BOARD_TEST
	fflush(cmd_out);
#endif //SLA_2ND_BOARD_TEST

	sla_init();

#ifdef SLA_2ND_BOARD_TEST
	_delay_ms(200);
	sla_twho_state = 1;
#endif //SLA_2ND_BOARD_TEST

	printf_P(PSTR("ready\n")); //startup message
}

//main loop
void loop(void)
{
	wdt_reset();
	sys_sp_check_top();
	cmd_process();
	sla_cycle();
}

#ifndef _SIMULATOR
int main(void)
{
	setup();
	while (1)
		loop();
}
#endif //_SIMULATOR
