#!/bin/bash
#
# chsum.sh - calculate checksum of binary file (sum of all bytes)
#  $1 - input binary file
#  $2 - output binary file (chsum will be appended at end of file if it exists)

bin_i=$1
bin_o=$2
if [ -z "$bin_i" ]; then echo 'No input file specified!' >&2; exit 1; fi
if [ -z "$bin_o" ]; then bin_o=$bin_i; fi

# print file as hex using xxd, cut hexadecimal data,
# replace spaces with line-feeds (awk need single byte per line)
# calculate sum using awk and output in hexadecimal format
chsum=$(\
xxd -g 1 $bin_i | cut -c11-57 | tr ' ' "\n" | sed '/^$/d' |\
 awk 'BEGIN { sum = 0; } { sum += strtonum("0x"$1); } END { printf("%08x\n", sum); }'\
)

# insert '\x' before every two chars
chsum=$(sed 's/.\{2\}/\\x&/g' <<< $chsum)

# swap bytes
chsum=${chsum:12:4}${chsum:8:4}${chsum:4:4}${chsum:0:4}

# append binary data using echo
command echo -n -e "$chsum" >>$bin_o

exit 0
