#ifndef _SLA_RSENS_H
#define _SLA_RSENS_H

#include <inttypes.h>
#include "config.h"

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern uint8_t sla_rsens;

extern void sla_rsens_enable(void);
extern void sla_rsens_disable(void);
extern uint8_t sla_rsens_get_state(void);
extern int8_t sla_rsens_meassure(uint32_t steps);
extern void sla_rsens_cycle(void);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_RSENS_H

