/*
 * system functions for watchdog, flash , etc.
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#include "sys.h"
#include "spi.h"

volatile uint8_t sys_mcusr = 0;
int8_t sys_state = 0;
uint8_t sys_signals = 0;

extern inline void sys_sp_check_fail(void);

void sys_init(void)
{
	sys_mcusr = MCUSR;
	MCUSR = 0;

	sys_sp_init();
	sys_init_wdt();
	sys_disable_usb();
	sys_setup_osc();
}

void sys_reset(void)
{
	wdt_enable(WDTO_15MS);
	cli();
#ifndef _SIMULATOR
	while(1);
#endif //_SIMULATOR
}

void sys_init_wdt(void)
{
	uint8_t _sreg = SREG;
	cli();
	wdt_reset();
	MCUSR &= ~(1 << WDRF);
	WDTCSR = (1 << WDCE) | (1 << WDE);
	WDTCSR = 0x00;
//	wdt_disable();
	SREG = _sreg;
	wdt_enable(WDTO_2S);
}

void sys_setup_osc(void)
{
	CLKSEL0 = 0x15;    //Choose Crystal oscillator with BOD
	CLKSEL1 = 0x0f;    //CLKSEL1.EXCKSEL0..3 = 1;
	CLKPR = 0x80;      //Change the clock prescaler, first change bit CLKPCE
	CLKPR = 0x00;      //
}

void sys_disable_usb(void)
{
	UHWCON = 0;
	USBCON = 0;
}

#define bootKey 0x7777
volatile uint16_t *const bootKeyPtr = (volatile uint16_t *)0x0800;

void sys_bootloader(void)
{
	*bootKeyPtr = bootKey;
	wdt_enable(WDTO_15MS);
	cli();
#ifndef _SIMULATOR
	while(1);
#endif //_SIMULATOR
}

#include <stdio.h>
extern FILE* cmd_err;

uint32_t sys_flash_chsum(uint16_t addr, uint16_t size)
{
	uint32_t chsum = 0;
	while (size--)
	{
//		cmd_err_printf_P(PSTR("%04x %02x "), addr, pgm_read_byte(addr));
		chsum += pgm_read_byte(addr++);
//		cmd_err_printf_P(PSTR("%06lx\n"), chsum);
	}
	return chsum;
}


volatile uint16_t *const sys_sp_min_ptr = (volatile uint16_t *)SYS_SP_MIN;
volatile uint16_t sys_sp_top = 0;

void sys_sp_init(void)
{
	if ((sys_mcusr & SYS_RESET_WDRF) && (*sys_sp_min_ptr == SYS_SP_OVF))
	{
		sys_mcusr &= ~SYS_RESET_WDRF;
		sys_mcusr |= SYS_RESET_SORF;
	}
	*sys_sp_min_ptr = SYS_SP_MAG;
}

_INLINE void sys_sp_check_fail(void)
{
	cli();
	wdt_enable(WDTO_15MS);
	*sys_sp_min_ptr = SYS_SP_OVF;
	while(1);
}

void sys_sp_check_min(void)
{
	if (*sys_sp_min_ptr == SYS_SP_MAG) return;
	sys_sp_check_fail();
}

void sys_sp_check_top(void)
{
	if (sys_sp_top == 0)
		sys_sp_top = SP;
	else
		if (sys_sp_top != SP)
			sys_sp_check_fail();
}



