/*
 * SL1 diagnostics of imporant data and printer state
 * runs at the bootup of the printer
 */
#include "sla_diag.h"
#include <stdio.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/boot.h>
#include "sys.h"
#include "sla.h"
#include "sla_sign.h"

#define BOOTLOADER_START       0x7000 //last 4kb of flash
#define BOOTLOADER_SIZE        0x0ff0 //4096 - 16 bytes
#define BOOTLOADER_V1_CHSUM    0x00079d66

extern FILE* cmd_err;
uint8_t sla_diag_state = 0;

//application flash checksum test
_INLINE uint8_t sla_diag_appl_flash_chsum(void)
{
	extern int __data_load_end; //linker symbol - end of flash
	uint16_t size = (uint16_t)(&__data_load_end); //size of flash
	//checksum is appended at end (dword == 4 bytes)
#ifdef _SIMULATOR
	return 1;
#endif
	return (pgm_read_dword(size) == sys_flash_chsum(0x0000, size))?1:0;
}

//bootloader flash checksum test
_INLINE uint8_t sla_diag_boot_flash_chsum(void)
{
	uint32_t chsum = sys_flash_chsum(BOOTLOADER_START, BOOTLOADER_SIZE);
	if (chsum == BOOTLOADER_V1_CHSUM) return 1;
#ifdef _SIMULATOR
	return 1;
#endif
	return 0;
}

_INLINE uint8_t sla_diag_serial_empty(void)
{
	uint8_t i;
	sla_read_serial(sla_sn);
	for (i = 0; i < 16; i++)
	{
		if (sla_sn[i] == SLA_SN_WRONG)
		{
			sla_sn[0] = SLA_SN_WRONG; //undefined SN
			break;
		}
	}
	return 0;
}

_INLINE uint8_t sla_diag_fuse_L5e_Hd8_Ec8(void)
{
	if (boot_lock_fuse_bits_get(GET_LOW_FUSE_BITS)      != 0x5e) return 0;
	if (boot_lock_fuse_bits_get(GET_HIGH_FUSE_BITS)     != 0xd8) return 0;
	if (boot_lock_fuse_bits_get(GET_EXTENDED_FUSE_BITS) != 0xc8) return 0;
	return 1;
}

_INLINE uint8_t sla_diag_bootflash_locked(void)
{
	if ((boot_lock_fuse_bits_get(GET_LOCK_BITS) & 0x3f) != 0x2f) return 0;
	return 1;
}

uint8_t sla_diag_sync_1_5(void)
{
	if (!sla_diag_appl_flash_chsum()) return SLA_ERROR_CHSA;
#ifndef SLA_DIAG_SKIP_CHSB //skip bootloader flash checksum test
	if (!sla_diag_boot_flash_chsum()) return SLA_ERROR_CHSB;
#endif //SLA_DIAG_SKIP_CHSB
#ifndef SLA_DIAG_SKIP_SNNS //skip serial number validity test
	if (sla_diag_serial_empty()) return SLA_ERROR_SNNS;
#endif //SLA_DIAG_SKIP_SNNS
#ifndef SLA_DIAG_SKIP_FUSE //skip fuses settings test
	if (!sla_diag_fuse_L5e_Hd8_Ec8()) return SLA_ERROR_FUSE;
#endif //SLA_DIAG_SKIP_FUSE
#ifndef SLA_DIAG_SKIP_LOCK //skip bootsection lock test
	if (!sla_diag_bootflash_locked()) return SLA_ERROR_LOCK;
#endif //SLA_DIAG_SKIP_LOCK
	return SLA_ERROR_NONE;
}

_INLINE void sla_diag_cycle_TSIG(void)
{
	sla_diag_state = SLA_DIAG_ADCV;
}

_INLINE void sla_diag_cycle_ADCV(void)
{
#ifndef SLA_DIAG_SKIP_ADCV
	uint8_t n;
	uint32_t v_sum[4] = {0, 0, 0, 0};
	cmd_err_printf_P(PSTR("DIAG_ADCV\n"));
	for (n = 0; n < 10; n++)
	{
		while (!sla_adc_changed)
		{
			cmd_process();
		}
		if (sla_adc_changed & 0x10) v_sum[0] += sla_volt_raw[0];
		if (sla_adc_changed & 0x20) v_sum[1] += sla_volt_raw[1];
		if (sla_adc_changed & 0x40) v_sum[2] += sla_volt_raw[2];
		if (sla_adc_changed & 0x80) v_sum[3] += sla_volt_raw[3];
	}
	cmd_err_printf_P(PSTR(" v_sum %d %d %d %d\n"), v_sum[0], v_sum[1], v_sum[2], v_sum[3]);
	sla_diag_state = SLA_DIAG_NONE;
	sla_set_mode(SLA_MODE_IDLE);
#else //SLA_DIAG_SKIP_ADCV
	sla_diag_state = SLA_DIAG_ADCT;
#endif //SLA_DIAG_SKIP_ADCV
}

_INLINE void sla_diag_cycle_ADCT(void)
{
#ifndef SLA_DIAG_SKIP_ADCT
#else //SLA_DIAG_SKIP_ADCT
	sla_diag_state = SLA_DIAG_MOTC;
#endif //SLA_DIAG_SKIP_ADCT
}

_INLINE void sla_diag_cycle_MOTC(void)
{
	sla_diag_state = SLA_DIAG_FANF;
}

_INLINE void sla_diag_cycle_FANF(void)
{
	sla_diag_state = SLA_DIAG_ULED;
}

_INLINE void sla_diag_cycle_ULED(void)
{
	sla_diag_state = SLA_DIAG_NONE;
}

void sla_diag_cycle(void)
{
	switch (sla_diag_state)
	{
	case SLA_DIAG_TSIG:
		sla_diag_cycle_TSIG();
		break;
	case SLA_DIAG_ADCV:
		sla_diag_cycle_ADCV();
		break;
	case SLA_DIAG_ADCT:
		sla_diag_cycle_ADCV();
		break;
	case SLA_DIAG_MOTC:
		sla_diag_cycle_MOTC();
		break;
	case SLA_DIAG_FANF:
		sla_diag_cycle_FANF();
		break;
	case SLA_DIAG_ULED:
		sla_diag_cycle_ULED();
		break;
	}
}
