#ifndef _SLA_SIGN_H
#define _SLA_SIGN_H

#include <inttypes.h>
#include <stdio.h>
#include "config.h"

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern void sla_print_signature(FILE* out);
extern void sla_read_pseudo_unique(char* ppun);
extern void sla_read_serial(uint8_t* psn);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_SIGN_H
