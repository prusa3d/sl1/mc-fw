// sla_diag.h
#ifndef _SLA_DIAG_H
#define _SLA_DIAG_H

#include <inttypes.h>
#include "config.h"

#define SLA_DIAG_NONE  0x00 //
#define SLA_DIAG_GSPI  0x01 //GPIO SPI communication
#define SLA_DIAG_TSPI  0x02 //TMC SPI communication
#define SLA_DIAG_TSIG  0x03 //TMC control signals (step,dir,ena)
#define SLA_DIAG_ADCV  0x04 //AD input test - voltages (24V + leds off)
#define SLA_DIAG_ADCT  0x05 //AD input test - thermistors (shortcut/disconnected)
#define SLA_DIAG_MOTC  0x06 //Motors (shortcut/disconnected, turn on for a short time)
#define SLA_DIAG_FANF  0x07 //Fans (turn on for short time and check response)
#define SLA_DIAG_ULED  0x08 //UV-led (turn on for short time and measure voltage)

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern uint8_t sla_diag_state;
extern uint8_t sla_diag_err;

extern uint8_t sla_diag_sync_1_5(void);
extern void sla_diag_cycle(void);
extern uint8_t sla_diag_boot_flash_chsum(void);
extern uint8_t sla_diag_serial_empty(void);
extern uint8_t sla_diag_fuse_L5e_Hd8_Ec8(void);
extern uint8_t sla_diag_bootflash_locked(void);
extern uint8_t sla_diag_appl_flash_chsum(void);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_DIAG_H
