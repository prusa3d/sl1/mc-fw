/**
 * SL1 synchronized spi
 * takes care of communication with stepper motor drivers
 */
#include "sla_spi.h"
#include "avr/interrupt.h"
#include "sla.h"
#include "mcp23s17.h"
#include "tmc2130.h"

//async spi request bitmask
volatile uint16_t sla_spi_req_mask = 0;

uint8_t  sla_spi_tmc_cur[2];
int8_t   sla_spi_tmc_sgt[2];
uint16_t sla_spi_tmc_cst[2];
uint32_t sla_spi_tmc_drv[2];
uint16_t sla_spi_tmc_msc[2];

void sla_spi_async(uint16_t spi_req_mask)
{
//	cmd_err_printf_P(PSTR("#SPI %04x\n"), spi_req_mask);
	cli();
	sla_spi_req_mask |= spi_req_mask;
	sei();
}

void sla_spi_sync(uint16_t spi_req_mask)
{
//	cmd_err_printf_P(PSTR("#SPI %04x\n"), spi_req_mask);
	cli();
	sla_spi_req_mask |= spi_req_mask;
	sei();
	while (sla_spi_req_mask & spi_req_mask);
}

void sla_spi_cycle(void)
{
	if (sla_spi_req_mask)
	{
		if (sla_spi_req_mask & SLA_SPI_REQ_GPIO_OUT_A)
		{
			mcp23s17_out_a(0x00, sla_gpio[0]);
			sla_spi_req_mask &= ~SLA_SPI_REQ_GPIO_OUT_A;
		}
		if (sla_spi_req_mask & SLA_SPI_REQ_GPIO_OUT_B)
		{
			mcp23s17_out_b(0x00, sla_gpio[1]);
			sla_spi_req_mask &= ~SLA_SPI_REQ_GPIO_OUT_B;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_GPIO_IN_A)
		{
			sla_gpio_r[0] = mcp23s17_in_a(0x00);
			sla_spi_req_mask &= ~SLA_SPI_REQ_GPIO_IN_A;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_GPIO_IN_B)
		{
			sla_gpio_r[1] = mcp23s17_in_b(0x00);
			sla_spi_req_mask &= ~SLA_SPI_REQ_GPIO_IN_B;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_TMC0_SET_CUR)
		{
			tmc2130_set_cur(0, sla_spi_tmc_cur[0]);
			sla_spi_req_mask &= ~SLA_SPI_REQ_TMC0_SET_CUR;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_TMC0_SET_SGT)
		{
			tmc2130_set_sgt(0, sla_spi_tmc_sgt[0]);
			sla_spi_req_mask &= ~SLA_SPI_REQ_TMC0_SET_SGT;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_TMC0_SET_CST)
		{
			tmc2130_set_cst(0, sla_spi_tmc_cst[0]);
			sla_spi_req_mask &= ~SLA_SPI_REQ_TMC0_SET_CST;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_TMC0_RD_DRV_STATUS)
		{
			sla_spi_tmc_drv[0] = tmc2130_read_drv_status(0);
			sla_spi_req_mask &= ~SLA_SPI_REQ_TMC0_RD_DRV_STATUS;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_TMC1_SET_CUR)
		{
			tmc2130_set_cur(1, sla_spi_tmc_cur[1]);
			sla_spi_req_mask &= ~SLA_SPI_REQ_TMC1_SET_CUR;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_TMC1_SET_SGT)
		{
			tmc2130_set_sgt(1, sla_spi_tmc_sgt[1]);
			sla_spi_req_mask &= ~SLA_SPI_REQ_TMC1_SET_SGT;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_TMC1_SET_CST)
		{
			tmc2130_set_cst(1, sla_spi_tmc_cst[1]);
			sla_spi_req_mask &= ~SLA_SPI_REQ_TMC1_SET_CST;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_TMC1_RD_DRV_STATUS)
		{
			sla_spi_tmc_drv[1] = tmc2130_read_drv_status(1);
			sla_spi_req_mask &= ~SLA_SPI_REQ_TMC1_RD_DRV_STATUS;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_TMC0_RD_MSCNT)
		{
			sla_spi_tmc_msc[0] = tmc2130_read_mscnt(0);
			sla_spi_req_mask &= ~SLA_SPI_REQ_TMC0_RD_MSCNT;
		}
		else if (sla_spi_req_mask & SLA_SPI_REQ_TMC1_RD_MSCNT)
		{
			sla_spi_tmc_msc[1] = tmc2130_read_mscnt(1);
			sla_spi_req_mask &= ~SLA_SPI_REQ_TMC1_RD_MSCNT;
		}
	}
}
