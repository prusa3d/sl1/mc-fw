#ifndef _SLA_ADC_H
#define _SLA_ADC_H

#include <inttypes.h>
#include "config.h"

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern uint16_t sla_temp_raw[4];
extern uint16_t sla_volt_raw[4];
extern int16_t sla_temp[4];
extern int16_t sla_volt[4];
extern uint8_t sla_adc_timer;

extern void sla_set_mux(uint8_t mux);
extern void sla_adc_cycle(void);
extern int16_t sla_calc_volt_uvled(uint16_t raw);
extern int16_t sla_calc_volt_power(uint16_t raw);
extern int16_t sla_calc_temp_uvled(uint16_t raw);
extern int16_t sla_calc_temp_ambient(uint16_t raw);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_ADC_H
