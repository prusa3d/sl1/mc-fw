#ifndef _SLA_TMC_H
#define _SLA_TMC_H

#include <inttypes.h>
#include "config.h"

#define SLA_TMC_SIG_STP 0x01
#define SLA_TMC_SIG_DIR 0x02
#define SLA_TMC_SIG_ENA 0x10
#define SLA_TMC_SIG_ALL 0x13

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)


extern int16_t sla_tmc_tstep1000[TMC2130_NUMAXES];
extern uint8_t sla_tmc_end_aux;

extern void sla_tmc_setup_pins(void);
extern uint8_t sla_tmc_get_ena(void);
extern void sla_tmc_set_ena(uint8_t mask);
extern uint8_t sla_tmc_get_dir(void);
extern void sla_tmc_set_dir(uint8_t mask);
extern void sla_tmc_cs_low(uint8_t axis);
extern void sla_tmc_cs_high(uint8_t axis);
extern void sla_tmc_do_step(uint8_t mask);
extern uint8_t sla_tmc_get_diag(void);
extern uint8_t sla_tmc_test_signals(uint8_t axis);
extern void sla_tmc_wait_standstill(uint8_t axis);
extern int16_t sla_tmc_calc_steps_to_phase(uint16_t actual_phase, uint16_t target_phase, uint8_t microstep_resolution);
extern uint16_t sla_tmc_measure_tstep1000(uint8_t axis);
extern int16_t sla_tmc_calc_cents(float f0, float f1);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_TMC_H
