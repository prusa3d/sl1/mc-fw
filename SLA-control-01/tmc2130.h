#ifndef _TMC2130_H
#define _TMC2130_H

#include <inttypes.h>
#include "config.h"

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)


extern uint8_t tmc2130_spi_status[TMC2130_NUMAXES];

//structure for tmc2130 axis parameters (size = 9bytes)
typedef struct
{
	uint32_t chopconf;  //chopconf register as 32bit value
	uint8_t  ihold;     //holding current (0..63)
	uint8_t  irun;      //running current (0..63)
	int8_t   sg_thr;    //stallguard threshold (-128..+127)
	uint16_t tcoolthrs; //coolstep threshold (0..65535)
} tmc2130_axis_t;

extern int8_t tmc2130_init(void);
extern uint8_t tmc2130_get_cur(uint8_t axis);
extern void tmc2130_set_cur(uint8_t axis, uint8_t cur);
extern int8_t tmc2130_get_sgt(uint8_t axis);
extern void tmc2130_set_sgt(uint8_t axis, int8_t sgt);
extern uint16_t tmc2130_get_cst(uint8_t axis);
extern void tmc2130_set_cst(uint8_t axis, uint16_t cst);
extern void tmc2130_set_mres(uint8_t axis, uint8_t mres);
extern uint32_t tmc2130_read_drv_status(uint8_t axis);
extern uint16_t tmc2130_read_mscnt(uint8_t axis);
extern uint8_t tmc2130_read_ioin(uint8_t axis);
extern uint8_t tmc2130_read_gstat(uint8_t axis);
extern uint32_t tmc2130_read_gconf(uint8_t axis);
extern uint32_t tmc2130_read_tstep(uint8_t axis);

//rx/tx datagram
uint8_t tmc2130_tx(uint8_t axis, uint8_t addr, uint32_t wval);
uint8_t tmc2130_rx(uint8_t axis, uint8_t addr, uint32_t* rval);

//read/write register
#define tmc2130_rd(axis, addr, rval) tmc2130_rx(axis, addr, rval)
#define tmc2130_wr(axis, addr, wval) tmc2130_tx(axis, addr | 0x80, wval)

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)
#endif //_TMC2130_H
