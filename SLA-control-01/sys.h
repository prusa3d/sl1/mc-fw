#ifndef _SYS_H
#define _SYS_H

#include <inttypes.h>
#include "config.h"

#define SYS_RESET_PORF 0x01 //power-on reset flag
#define SYS_RESET_EXRF 0x02 //external reset flag
#define SYS_RESET_BORF 0x04 //brown-out reset flag
#define SYS_RESET_WDRF 0x08 //watchdog reset flag
#define SYS_RESET_JTRF 0x10 //jtag reset flag
#define SYS_RESET_SORF 0x80 //stack overflow reset flag

#define SYS_SP_MAG 0xa55a   //stack monitor magic word - OK
#define SYS_SP_OVF 0x5aa5   //stack monitor magic word - overflow

// get state of signal (main loop or interrupt)
#define SIG_GET(id) (sys_signals & (1 << id))
// set state of signal (interrupt only)
#define SIG_SET(id) (sys_signals |= (1 << id))
// get state of signal (main loop only)
#define SIG_CLR(id) (sys_signals &= ~(1 << id))

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

// reset flags
extern volatile uint8_t sys_mcusr;
// system state
extern int8_t sys_state;
// signals from interrupt to main loop
extern uint8_t sys_signals;
extern volatile uint16_t *const sys_sp_min_ptr;
extern volatile uint16_t sys_sp_top;


extern void sys_init(void);
extern void sys_reset(void);
extern void sys_init_wdt(void);
extern void sys_setup_osc(void);
extern void sys_disable_usb(void);
extern void sys_bootloader(void);
extern uint32_t sys_flash_chsum(uint16_t addr, uint16_t size);
extern void sys_sp_init(void);
extern void sys_sp_check_min(void);
extern void sys_sp_check_top(void);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)
#endif //_SYS_H
