/*
 * HW specific configuration for MC revision 6 and above
 */
#include "sla.h"

#if (BOARD == SLA_REV06)

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "sys.h"
#include "cmd.h"
#include "mcp23s17.h"
#include "tmc2130.h"
#include "snd.h"
#include "adc.h"
#include "intpol.h"
#include "morse.h"
#include "sla_tmc.h"
#include "sla_spi.h"

//MCU:
//pin  xfunc  sla signal         change from rev03
//-------------------------------------------------------------
//PB0         RX-LED             no change
//PB1  SCLK   SPI                no change
//PB2  MOSI   SPI                no change
//PB3  MISO   SPI                no change
//PB4         CS-MCP             no change
//PB5         STEP-1             no change
//PB6         STEP-2             no change
//PB7         START-LED          no change
//PC6  OC3A   BUZZ               no change
//PC7         PWR-ON             no change
//PD0  OC0B   UV-LED-PWM         UV-LED-PWM was PF7, PD0 was PANIC
//PD1         CSN-1              CSN-1 was PF4, PD1 was 1-Wire
//PD2  RXD    RXD                no change
//PD3  TXD    TXD                no change
//PD4  ADC8   CSN-2              CSN-2 was PF5, PD4 was FAN1-TACH
//PD5         TX-LED             no change
//PD6  ADC9   Diag-1             Diag-1 was PF0, PD6 was FAN2-TACH
//PD7  ADC10  Diag-2             Diag-2 was PF1, PD7 was FAN3-TACH
//PE2  HWB    Surface-detector   Surface-detector was PF6, PW2 was HWB
//PE6  INT6   PANIC              PANIC was PD0, PE6 was FAN4-TACH
//PF0  ADC0   TEMP-MULTIPLEX     new signal, PF0 was Diag-1
//PF1  ADC1   UV-LED-MULTIPLEX   new signal, PF1 was Diag-2
//PF4  ADC4   FAN1-TACH          FAN1-TACH was PD4, PF4 was CSN-1
//PF5  ADC5   FAN2-TACH          FAN2-TACH was PD6, PF5 was CSN-2
//PF6  ADC6   FAN3-TACH          FAN3-TACH was PD7, PF6 was Surface-detector
//PF7  ADC7   FAN3-PWM           UV-LED-PWM was PF7, PD0 was PANIC, FAN3 was GPB0
//Multiplexer:
//X0          UV-LED-1           new signal
//X1          UV-LED-2           new signal
//X2          UV-LED-3           new signal
//X3
//Y0          TEMP-LED           new signal
//Y1          TEMP-1             new signal
//Y2          TEMP-2             new signal
//Y3          TEMP-3             new signal
//Expander:
//GPA0        DIR-1              no change
//GPA1        EN-1               no change
//GPA2        DIR-2              no change
//GPA3        EN-2               no change
//GPA4        FAN-1              no change
//GPA5        FAN-2              no change
//GPA6        FAN-3(always ON)   FAN1 was GPA6
//GPA7        FAN-4              no change
//GPB0        UV-LED             no change
//GPB1        IR-LED             no change
//GPB2        detect-ON          no change
//GPB3        AnalogSwA          new signal, GPB3 was unused
//GPB4        AnalogSwB          new signal, GPB4 was unused
//GPB5        END2               no change
//GPB6        END1               no change
//GPB7        BUTT-DETECT        no change






uint8_t sla_get_power(void)
{
	return (PORTC & 0x80)?1:0;
}

void sla_set_power(uint8_t power)
{
	DDRC |= 0x80;
	if (power)
		PORTC |= 0x80;
	else
		PORTC &= ~0x80;
}

uint8_t sla_get_end3(void)
{
#ifndef _SIMULATOR
	return (sla_gpio_r[0] & 0x80)?0:1; //END3 - bit7
#else
        return (st4_axis[1].pos > 6000 || st4_axis[1].pos < 0) && (st4_end & 0x02);
#endif //_SIMULATOR

}

uint8_t sla_get_start_button(void)
{
	return (sla_gpio_r[1] & 0x80)?0:1; //BUTT-DETECT - bit7
}

uint8_t sla_get_cover_switch(void)
{
	return (sla_gpio_r[1] & 0x20)?0:1; //END2 - bit5
}

void sla_init_leds(void)
{
	DDRB |= 0x80; //start led pin
	DDRB |= 0x01; //rx led pin
	DDRD |= 0x20; //tx led pin
	DDRD |= 0x01; //uv led pwm pin
	PORTB |= 0x80; //start led on
	PORTB |= 0x01; //rx led off
	PORTD |= 0x20; //tx led off
	PORTD |= 0x01; //uv led pwm on
	sla_leds = SLA_MSK_PLED; //led status = pled on
	sla_pled = SLA_PLED_CONT; //pled mode = continuous lighting
}

void sla_start_led_on(void)
{
	PORTB |= 0x80;
	sla_leds |= SLA_MSK_PLED;
}

void sla_start_led_off(void)
{
	PORTB &= ~0x80;
	sla_leds &= ~SLA_MSK_PLED;
}

void sla_set_led(uint8_t mask)
{
	sla_leds = mask;
	if (sla_leds & SLA_MSK_RLED) PORTB &= ~0x01;
	else PORTB |= 0x01;
	if (sla_leds & SLA_MSK_TLED) PORTD &= ~0x20;
	else PORTD |= 0x20;
	if (sla_leds & SLA_MSK_ULED) sla_gpio[1] |= 0x03;
	else sla_gpio[1] &= ~0x03;
	sla_spi_async(SLA_SPI_REQ_GPIO_OUT_B);
}

uint8_t sla_get_tach(void)
{
	return PINF >> 4;
}

void sla_set_fan(uint8_t mask)
{
	cli();
	for(uint8_t i = 0; i < SLA_FAN_NUM; i++)
	{
		if ((mask ^ sla_spwm.enable) & (1 << i))
		{
			sla_spwm_dev[i].on_index = sla_spwm.index; //enable fan in this timeslot. Anyway it could take up to 65525 ms
			if(i == REAR_FAN_IDX) {
				sla_spwm_dev[i].duty = REAR_FAN_DUTY_MAX; //start fan at 100% duty cycle
			} else {
#ifndef _SIMULATOR
				sla_fan[i].tacho = sla_spwm_dev[i].duty; //set tacho to expected value to shorten time to fane
#endif //_SIMULATOR
				sla_spwm_dev[i].period = sla_spwm_dev[i].duty; //start fan at 100% duty cycle
			}
		}
	}
	sla_fans.fane = 0;
	sla_fans.fane_delay = SLA_FANS_FANE_DELAY;
	sla_spwm.enable = (sla_spwm.enable & (1 << 3)) | mask;	//do not change pled status
	sei();
}

#endif //BOARD_SLA_REV06
