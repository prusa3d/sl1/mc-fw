/**
 * functions for driving beeper
 */
#include "snd.h"
#include <avr/interrupt.h>
#include "io_atmega32u4.h"

#if (SND_TIMER == 1)
#define OCRnA OCR1A
#elif (SND_TIMER == 3)
#define OCRnA OCR3A
#endif

void snd_init(void)
{
// CTC mode 4, source = fclk/8
// COMA=00 COMB=00 COMC=00 WGM=0100 CS=010
#if (SND_TIMER == 1)
	TIMSK1 &= ~((7 << OCIE1A)); //disable interrupts
	TCCR1A = 0x00; //COM_A-C=00, WGM_0-1=00
 	TCCR1B = (1 << WGM12) | (2 << CS10); //WGM_2-3=01, CS_0-2=010 (CTC, source = fclk/8)
	OCR1A = 0;
	OCR1B = 0;
	OCR1C = 0;
	TCNT1 = 0;
#elif (SND_TIMER == 3)
	TIMSK3 &= ~((7 << OCIE3A)); //disable interrupts
	TCCR3A = 0x00; //COM_A-C=00, WGM_0-1=00
 	TCCR3B = (1 << WGM12) | (2 << CS10); //WGM_2-3=01, CS_0-2=010 (CTC, source = fclk/8)
	OCR3A = 0;
	OCR3B = 0;
	OCR3C = 0;
	TCNT3 = 0;
#endif
	PIN_OUT(SND_PIN);
	PIN_CLR(SND_PIN);
}

float snd_get_freq(void)
{
	if (!snd_is_on()) return 0;
	return (float)1000000 / (OCRnA + 1);
}

void snd_set_freq(float freq)
{
	uint16_t ocra;
	if (freq == 0)
		snd_off();
	else
	{
		ocra = (uint16_t)(1000000 / freq - 0.5);
		if (ocra < 200) ocra = 200;
		OCRnA = ocra;
		snd_on();
	}
}

#if !SND_OCR_OUT
#if (SND_TIMER == 1)
ISR(TIMER1_COMPA_vect)
#elif (SND_TIMER == 3)
ISR(TIMER3_COMPA_vect)
#endif
{
	PIN_TOG(SND_PIN);
}
#endif //!SND_OCR_OUT
