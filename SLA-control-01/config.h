#ifndef _CONFIG_H
#define _CONFIG_H

//version and buildnumber
#define FW_VERSION_MAJOR 1
#define FW_VERSION_MINOR 2
#define FW_VERSION_PATCH 0
#define FW_BUILDSX ""          //semantic version suffix (refer to semver.org)

//SYS configuration (system)
#define SYS_SP_MIN    0x0900   //minimum SP addres

//UART configuration
#define UART1_BAUD 115200      //baudrate 115200
#define UART1_IBUF 128         //input buffer size
//#define UART1_OBUF 16          //output buffer size
#define UART1_INBL             //input non blocking mode
#define UART1_FILE             //uart1io FILE stream
#define UART1_RFUL             //report rx full

//CMD configuration (file stream command processor)
#define CMD_MAX_LINE 42        //;-)

//board revisions
#define SLA_REV01  1           //rev01 not supported
#define SLA_REV02  2           //rev02 discontinued 2018/12/18 (last build 197)
#define SLA_REV03  3           //rev03 discontinued 2019/01/19 (last build 270)
#define SLA_REV04  4           //rev04 discontinued 2019/04/09 (last build 389)
#define SLA_REV05  5           //rev05 discontinued 2020/07/22 (it was never put in production)
#define SLA_REV06  6

#ifdef BOARD
#if (BOARD < SLA_REV06)
#error "BOARD not supported"
#endif //(BOARD < SLA_REV06)
#else
#error "BOARD not defined"
#endif //BOARD

//SLA
#define SLA_AUTOKILL
#define SLA_UPWM_MIN       0    //uled pwm min value
#define SLA_UPWM_MAX       250  //uled pwm max value
#define SLA_HCAL_CNT       16   //home calibration step count
#define SLA_TMC_DELAY      10   //stallguard buffer sample delay
#define SLA_TACH_TIME      800  //time period for fans tacho measurement [ms]
#define SLA_FAN_PWM_DELAY  5    //delay for fan pwm adjut after rpms are set SLA_FAN_PWM_DELAY*SLA_TACH_TIME [ms]
//#define SLA_2ND_BOARD_TEST
//#define SLA_2ND_BOARD_TEST2
//#define SLA_2ND_BOARD_TEST3
//#define SLA_DIAG               //diagnostics after power-up
//#define SLA_DIAG_SKIP_CHSB     //skip bootloader flash checksum test
//#define SLA_DIAG_SKIP_SNNS     //skip serial number validity test
//#define SLA_DIAG_SKIP_FUSE     //skip fuses settings test
//#define SLA_DIAG_SKIP_LOCK     //skip bootsection lock test
//#define SLA_DIAG_SKIP_GSPI     //skip gpio spi
//#define SLA_DIAG_SKIP_TSPI     //skip tmc spi
//#define SLA_DIAG_SKIP_TSIG     //skip tmc signal/wiring diagnostics
#define SLA_DIAG_SKIP_ADCV     //skip adc voltage diagnostics
#define SLA_DIAG_SKIP_ADCT     //skip adc temperature diagnostics
//#define ST4_DBG                //debug output from st4 (228 bytes)
//#define SLA_DBG_HOME_TOWER     //debug output durring tower homing (652 bytes)
//#define SLA_DBG_HOME_TILT      //debug output durring tilt homing (780 bytes)
//#define SLA_DBG_HOME_CALIB     //debug output from calibration (328 bytes)
//#define SLA_DBG_SEPA_TILT     //debug output from tilt separation
//#define SLA_DBG_TMCC            //enable debug function to read/write TMC registers
//#define SLA_DBG_TMC_TSTEP     //debug output from tstep measurement
#define SLA_TW_MRES    4       //tower microstep resolution (4=16usteps)
#define SLA_TI_MRES    2       //tilt microstep resolution (2=64usteps)
#define SLA_TW_CUR_DEF 24      //default tower current
#define SLA_TI_CUR_DEF 24      //default tilt current
#define SLA_TW_SGT_DEF 6       //default tower stallguard threshold
#define SLA_TI_SGT_DEF 8       //default tilt stallguard threshold
#define SLA_TW_RNG_HOM 4000000  //tower range for homing [steps]
#define SLA_TW_BST_HOM 2048    //tower back-step for homing [steps]
#define SLA_TW_BS1_HOM 1024    //tower final back-step for homing [steps]
//#define SLA_TISE               //tilt separate command
#ifndef _SIMULATOR
#define SLA_TW_AUX_END         //tower aux endstop function
#define SLA_TI_AUX_END         //tilt aux endstop function
#endif // _SIMULATOR
#define SLA_TI_OPT_END         //tilt optical endstop function
#define SLA_OLICNT_MAX 20      //open load indicator counter maximum value
#if (SLA_TI_MRES == 4)
#define SLA_TI_RNG_HOM 3200    //tilt range for homing [steps]
#define SLA_TI_BST_HOM 256     //tilt back-step for homing [steps]
#define SLA_TI_BS1_HOM 64      //tilt final back-step for homing [steps]
#elif (SLA_TI_MRES == 2)
#define SLA_TI_RNG_HOM 12800   //tilt range for homing [steps]
#define SLA_TI_BST_HOM 768    //tilt back-step for homing [steps]
#define SLA_TI_BS1_HOM 256     //tilt final back-step for homing [steps]
#endif

//Homing - back-step tolerance
//#define SLA_TWHO_DIST_MIN 704
//#define SLA_TWHO_DIST_MAX 832
//#define SLA_TIHO_DIST_MIN 256
//#define SLA_TIHO_DIST_MAX 512
//#define SLA_TWHO_DIST_MIN 1024
//#define SLA_TWHO_DIST_MAX 1344
//#define SLA_TIHO_DIST_MIN 224
//#define SLA_TIHO_DIST_MAX 320
#define SLA_TWHO_DIST_MIN 960
#define SLA_TWHO_DIST_MAX 2560
#if (SLA_TI_MRES == 4)
#define SLA_TIHO_DIST_MIN 192
#define SLA_TIHO_DIST_MAX 768
#define SLA_TISE_DIST_MIN 256
#elif (SLA_TI_MRES == 2)
#define SLA_TIHO_DIST_MIN (SLA_TI_BST_HOM-64)
#define SLA_TIHO_DIST_MAX (SLA_TI_BST_HOM+128)
#define SLA_TISE_DIST_MIN 1024
#endif
#define SLA_TISE_LOOP_MAX 6

//TIMER0
#define TIMER0_PRESCALER   (3 << CS00) //timer0 prescaler (fclk/64)
#define TIMER0_CYC_1MS     250 //timer0 cycles per 1ms

//MCP23S17 configuration
#define MCP23S17               //use MCP23S17
#define MCP23S17_SPI_RATE   0  // fosc/4 = 4MHz
#define MCP23S17_SPCR       SPI_SPCR(MCP23S17_SPI_RATE, 0, 0, 1, 0)
#define MCP23S17_SPSR       SPI_SPSR(MCP23S17_SPI_RATE)
#define MCP23S17_CS_PIN     8  //chipselect pin PB4
#define MCP23S17_DEF_DIR 0xe080 //0..6 out 7 in 8..12 out 13..15 in
#define MCP23S17_DEF_GPO 0xe0ca //1,3 high 6-7 high, 0,2,4..5 low 8..12 low 13..15 high

//ADC configuration
#define ADC_CHAN_MSK      0x0003     //used AD channels bit mask (0,1)
#define ADC_CHAN_CNT      2          //number of used channels
#define ADC_OVRSAMPL      4          //oversampling multiplier
#define ADC_READY         adc_ready  //callback function ()
#define ADC_VREF          5050       //reference voltage [mV]
#if (BOARD == SLA_REV06)
#define ADC_DELAY         20
#endif //(BOARD == SLA_REV06)

//TMC2130 - Trinamic stepper driver
#define TMC2130_NUMAXES    2
//chipselect control
#define TMC2130_CS_LOW     sla_tmc_cs_low
#define TMC2130_CS_HIGH    sla_tmc_cs_high
//SPI configuration
#define TMC2130_SPI_RATE   0 // fosc/4 = 4MHz
#define TMC2130_SPCR       SPI_SPCR(TMC2130_SPI_RATE, 1, 1, 1, 0)
#define TMC2130_SPSR       SPI_SPSR(TMC2130_SPI_RATE)
//default axis configuration (common for all axes)
#define TMC2130_DEF_CUR    16 // default current
#define TMC2130_DEF_SGT    3  // default stallguard threshold
#define TMC2130_DEF_CST    1500 //default coolstep threshold
#define TMC2130_SGFILTER   1  //stallguard filter

//ST4 - stepper motion control
#define ST4_TIMER      1
#define ST4_NUMAXES    2
//direction control
#define ST4_GET_DIR    sla_tmc_get_dir
#define ST4_SET_DIR    sla_tmc_set_dir
//endstop sampling function
#define ST4_GET_END    sla_tmc_get_diag
//step function
#define ST4_DO_STEP    sla_tmc_do_step

//SND - sound generating
#define SND_TIMER          3 //use timer3
#define SND_PIN            5 //output pin PC6

//MORSE - morse code
//morse player timing - on/off delays
#define MORSE_DL      48         //  base delay
#define MORSE_DL1_SPC 0          //  on-delay - space
#define MORSE_DL0_SPC MORSE_DL   // off-delay - space
#define MORSE_DL1_DOT MORSE_DL   //  on-delay - dot
#define MORSE_DL0_DOT MORSE_DL   // off-delay - dot
#define MORSE_DL1_DSH 3*MORSE_DL //  on-delay - dash
#define MORSE_DL0_DSH MORSE_DL   // off-delay - dash
#define MORSE_OUT_L morse_out_L  // output '0' function
#define MORSE_OUT_H morse_out_H  // output '1' function
extern void morse_out_L(void);
extern void morse_out_H(void);

//MSVC or MINGW simulator
#ifdef _SIMULATOR
#include <simulator.h>
#pragma warning(disable:4996)
#define asm(code)
#define _SIM_SPI
#define _INLINE __inline
#undef SYS_SP_MIN
#define SYS_SP_MIN    (&__sim_sp_min)
#define SLA_DIAG_SKIP_TSPI     //skip tmc spi
#define SLA_DIAG_SKIP_TSIG     //skip tmc signal/wiring diagnostics
#else //_SIMULATOR
#define _INLINE inline
#endif //_SIMULATOR

//FANS
#define SLA_FRPM_MIN        800
#define SLA_FRPM_UV_MAX     2800
#define SLA_FRPM_BLOWER_MAX 3300
#define SLA_FRPM_REAR_MAX   5000
#define SLA_FAN_NUM         3
#define SLA_FANS_FANE_DELAY 6

//softPWM
#define SLA_SPWM_DEV		SLA_FAN_NUM + 1	// START-LED + FANS
#define SLA_SPWM_DEFAULT	20				// software pwm default value [ms]
#define SLA_SPWM_TOLERANCE	24				// rpm tolerance [ms] -> min 1250 RPM

//Voltage meassurement
#define SLA_VOLT_MEAS_R111     4700  //voltage divider top resistor [x10 ohm]
#define SLA_VOLT_MEAS_R112     3300  //voltage divider bottom resistor [x10 ohm]
#define SLA_VOLT_MEAS_VCSR     51    //current sense resistor voltage [mV]

//voltage divider ratio (x1024)
#define SLA_VOLT_MEAS_DIVR     ((uint32_t)1024 * (SLA_VOLT_MEAS_R111 + SLA_VOLT_MEAS_R112) / SLA_VOLT_MEAS_R112)
#define SLA_VOLT_MEAS_R21     10000  //voltage divider top resistor [kohm]
#define SLA_VOLT_MEAS_R90      1800  //voltage divider bottom resistor [kohm]

//voltage divider ratio (x1024)
#define SLA_VOLT_MEAS_DIVR2    ((uint32_t)1024 * (SLA_VOLT_MEAS_R21 + SLA_VOLT_MEAS_R90) / SLA_VOLT_MEAS_R90)

#endif //_CONFIG_H
