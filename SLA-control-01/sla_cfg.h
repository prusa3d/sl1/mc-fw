#ifndef _SLA_CFG_H
#define _SLA_CFG_H

#include <inttypes.h>
#include <stdio.h>
#include "config.h"

#define SLA_CFG_SN             0 // 16 bytes, serial number
#define SLA_CFG_FWVER          1 // 4 bytes, firmware version
#define SLA_CFG_EEVER          2 // 2 bytes, eeprom version
#define SLA_CFG_USTA           3 // 4 bytes, UV led statistics (second counter overflows after 136 years)
#define SLA_CFG_USTA_DSP       4 // 4 bytes, display statistics (second counter overflows after 136 years)
#define SLA_CFG_TWCFGN         5 // 16 bytes, tower config (selected)
#define SLA_CFG_TICFGN         6 // 16 bytes, tilt config (selected)
#define SLA_CFG_TWCFGA         7 // 16 bytes, tower config (all)
#define SLA_CFG_TICFGA         8 // 16 bytes, tilt config (all)
#define SLA_CFG_TWHPHA         9 // 2 byte, tower home phase
#define SLA_CFG_TIHPHA        10 // 2 byte, tilt home phase
#define SLA_CFG_TWHOFS        11 // 2 byte, tower home offset
#define SLA_CFG_TIHOFS        12 // 2 byte, tilt home offset
#define SLA_CFG_TIHOSC        13 // 2 byte, tilt stallguard compensation
#define SLA_CFG_END           20 // 0 bytes, just pointer

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

//dirty flags for all cfg variables
extern volatile uint32_t sla_cfg_dirty;

extern uint16_t sla_cfg_addr(uint8_t cfg);
extern void sla_cfg_init(void);
extern int8_t sla_cfg_load(void);
extern void sla_cfg_load_XXcf(uint8_t axis, int8_t index);
extern void sla_cfg_save(void);
extern void sla_cfg_default(void);
extern void eeprom_erase(uint16_t addr, uint16_t size);
extern void eeprom_read_bytes(uint16_t addr, uint8_t* data, uint8_t size);
extern void eeprom_write_bytes(uint16_t addr, uint8_t* data, uint8_t size);
extern void eeprom_update_bytes(uint16_t addr, uint8_t* data, uint8_t size);
extern void sla_print_eeprom(FILE* out);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_CFG_H
