/*
 * configuration of motors, eeprom functions, ...
 */
#include "sla_cfg.h"
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include "sla.h"
#include "sla_sign.h"

volatile uint32_t sla_cfg_dirty = 0;
const sla_axis_config_t PROGMEM sla_def_cfg_tower[16] =
{
//     sr0    srm  acc  dec   cur sgt    cst
	{{2500,	15000, 250, 150}, {22,  4,  100}}, //homingFast
	{{2500,  7500, 250, 150}, {16,  1,  500}}, //homingSlow
	{{3200, 15000, 250, 250}, {10,  3, 2200}}, //moveFast
	{{ 100,   150,  50,  50}, {15,  0, 1500}}, //moveSlow
	{{2500,  7500, 350, 350}, {12,  2, 1400}}, //resinSensor
	{{600,    800, 200, 200}, {34,  6,  500}}, //layer1
	{{600,   1600, 200, 200}, {34, 12,  500}}, //layer2 former superSlow
	{{600,   2400, 200, 200}, {34,  6,  500}}, //layer3
	{{600,   3200, 200, 200}, {34,  6,  500}}, //layer4
	{{600,   4000, 200, 200}, {34,  6,  500}}, //layer5
	{{600,   6400, 200, 200}, {34,  6,  500}}, //layer8
	{{600,   8800, 200, 200}, {34,  6,  500}}, //layer11
	{{600,  11200, 200, 200}, {34,  6,  500}}, //layer14
	{{600,  14400, 200, 200}, {34,  6,  500}}, //layer18
	{{3200, 17600, 250, 250}, {34,  6,  500}}, //layer22 former layer and layerMove
	{{3200, 19200, 250, 250}, {34,  6,  500}}, //layer24
};
#if (SLA_TI_MRES == 4)
const sla_axis_config_t PROGMEM sla_def_cfg_tilt[8] =
{
//     sr0    srm  acc  dec   cur sgt   cst
	{{ 640,  1280,  60,  60}, {19,  6, 1200}}, //homingFast
	{{ 360,   720,  40,  40}, {16,  5, 1500}}, //homingSlow
	{{ 960,  1280,  20,  20}, {24,  8, 1500}}, //moveFast
	{{ 100,   150,  50,  50}, {20,  0, 1500}}, //moveSlow
	{{ 100,   400,  12,  12}, {31, 12, 3000}}, //layerMoveSlow
	{{ 100,   200,  12,  12}, {62, 34, 2000}}, //layerRelease
	{{ 500,   800,  12,  12}, {18,  7, 2000}}, //layerMoveFast
	{{ 960,  1300,  50,  50}, {15,  3, 1150}}, //<reserved2>
};
#elif (SLA_TI_MRES == 2)
const sla_axis_config_t PROGMEM sla_def_cfg_tilt[16] =
{
//     sr0    srm  acc  dec   cur sgt   cst
	{{2560,  5120, 240, 240}, {20,  7,  700}}, //homingFast
	{{1200,  1500, 160, 160}, {16,  7, 1100}}, //homingSlow
	{{ 100,   120, 200, 200}, {11,  0, 1500}}, //move120 former moveSlow SL1
	{{ 100,   300,  80,  80}, {20,  0, 1500}}, //move300 former moveSlow SL1S/M1
	{{3840,  5120,  80,  80}, {16,  6, 1500}}, //move5120 former moveFast SL1
	{{8000,  8000,   0,   0}, {26,  9,  700}}, //move8000 former moveFast SL1S/M1
	{{ 200,   200,   0,   0}, {44, 63,    0}}, //layer200
	{{ 400,   400,   0,   0}, {44, 63,    0}}, //layer400 former layerRelease
	{{ 300,   600, 150, 150}, {20,  0, 1500}}, //layer600 former superSlow SL1
	{{ 400,   800, 150, 150}, {20,  0, 1500}}, //layer800 former superSlow SL1S/M1
	{{ 500,  1000, 150, 150}, {20,  0, 1500}}, //layer1000
	{{ 500,  1250, 150, 150}, {20,  0, 1500}}, //layer1250
	{{1500,  1500,   0,   0}, {44, 40, 2100}}, //layer1500 former layerMoveSlow SL1
	{{1750,  1750,   0,   0}, {44, 40, 2000}}, //layer1750 former layerMoveFast for SL1 and layerMoveX for SL1S/M1
	{{1750,  2000,   0,   0}, {44, 40, 2000}}, //layer2000
	{{1750,  2250,   0,   0}, {44, 40, 2000}}, //layer2250
};
#endif

uint16_t sla_cfg_addr(uint8_t cfg)
{
	switch (cfg)
	{
	case SLA_CFG_SN:     return 0x0000; //16 bytes, serial number
	case SLA_CFG_FWVER:  return 0x0010; //4 bytes, firmware version
	case SLA_CFG_EEVER:  return 0x0014; //2 bytes, eeprom version
	case SLA_CFG_USTA:   return 0x0016; //4 bytes, UV led statistics (second counter)
	case SLA_CFG_USTA_DSP: return 0x0020; //4 bytes, UV led statistics (second counter)
	case SLA_CFG_TWCFGN: return 0x0100; //16 bytes, selected tower config
	case SLA_CFG_TICFGN: return 0x0200; //16 bytes, selected tilt config
	case SLA_CFG_TWCFGA: return 0x0100; //16 bytes x 16, all tower configs
	case SLA_CFG_TICFGA: return 0x0200; //16 bytes x 16, all tilt configs
	case SLA_CFG_TWHPHA: return 0x0300; //2 byte, tower home phase
	case SLA_CFG_TIHPHA: return 0x0310; //2 byte, tilt home phase
	case SLA_CFG_TWHOFS: return 0x0302; //2 byte, tower home offset
	case SLA_CFG_TIHOFS: return 0x0312; //2 byte, tilt home offset
	case SLA_CFG_TIHOSC: return 0x0314; //2 byte, tilt stallguard compensation

	case SLA_CFG_END:    return 0x0316;	//first address with no data in eeprom
	}
	return 0xffff;
}

void progmem_read_bytes(uint8_t* addr, uint8_t* data, uint8_t size)
{
	while (size--)
		*(data++) = pgm_read_byte(addr++);
}

void eeprom_erase(uint16_t addr, uint16_t size)
{
	while (size--)
	{
		eeprom_update_byte((uint8_t*)(addr++), 0xff);
	}
}

void eeprom_read_bytes(uint16_t addr, uint8_t* data, uint8_t size)
{
	while (size--)
		*(data++) = eeprom_read_byte((uint8_t*)(addr++));
}

void eeprom_write_bytes(uint16_t addr, uint8_t* data, uint8_t size)
{
	while (size--)
		eeprom_write_byte((uint8_t*)(addr++), *(data++));
}

void eeprom_update_bytes(uint16_t addr, uint8_t* data, uint8_t size)
{
	while (size--)
		eeprom_update_byte((uint8_t*)(addr++), *(data++));
}

void sla_cfg_init(void)
{
	if (sla_sn[0] == SLA_SN_WRONG)
		sla_cfg_default();
	else
		if (sla_cfg_load() != 0)
			sla_cfg_default();
}

int8_t sla_axis_cfg_check(sla_axis_config_t* pcfg)
{
	if (pcfg->st4.sr0 > 20000) return -1;
	if (pcfg->st4.srm > 20000) return -1;
	if (pcfg->st4.acc < 12) return -1;
	if (pcfg->st4.acc > 600) return -1;
	if (pcfg->st4.dec < 12) return -1;
	if (pcfg->st4.dec > 600) return -1;
	if (pcfg->tmc.cur > 63) return -1;
	if (pcfg->tmc.cst > 5000) return -1;
	return 0;
}

int8_t sla_cfg_load(void)
{
	int8_t index;
	uint16_t addr;
	sla_axis_config_t cfg;

	//load persistant data
	addr = sla_cfg_addr(SLA_CFG_USTA);
	eeprom_read_bytes(addr, (uint8_t*)&sla_uled.uvled_counter, 4);
	if (sla_uled.uvled_counter == -1)	// only if EEPROM empty, initialize to 0
	{
		sla_uled.uvled_counter = 0;
		eeprom_update_bytes(addr, (uint8_t*)&sla_uled.uvled_counter, 4);
	}

	addr = sla_cfg_addr(SLA_CFG_USTA_DSP);
	eeprom_read_bytes(addr, (uint8_t*)&sla_uled.display_counter, 4);
	if (sla_uled.display_counter == -1)	// only if EEPROM empty, initialize to 0
	{
		sla_uled.display_counter = 0;
		eeprom_update_bytes(addr, (uint8_t*)&sla_uled.display_counter, 4);
	}

	addr = sla_cfg_addr(SLA_CFG_TWCFGA);
	for (index = 0; index < 16; index++)
	{
		eeprom_read_bytes(addr + 16 * index, (uint8_t*)(&cfg), 16);
		if (sla_axis_cfg_check(&cfg) != 0)
			return -1;
	}

	sla_twho_phase = eeprom_read_word((uint16_t*)(sla_cfg_addr(SLA_CFG_TWHPHA)));
	sla_tiho_phase = eeprom_read_word((uint16_t*)(sla_cfg_addr(SLA_CFG_TIHPHA)));
	sla_twho_offset = eeprom_read_word((uint16_t*)(sla_cfg_addr(SLA_CFG_TWHOFS)));
	sla_tiho_offset = eeprom_read_word((uint16_t*)(sla_cfg_addr(SLA_CFG_TIHOFS)));
	sla_tiho_comp_sgn = eeprom_read_byte((uint8_t*)(sla_cfg_addr(SLA_CFG_TIHOSC)) + 0);
	sla_tiho_comp_sgp = eeprom_read_byte((uint8_t*)(sla_cfg_addr(SLA_CFG_TIHOSC)) + 1);
	return 0;
}

void sla_cfg_save_XXcf(uint8_t axis, int8_t index)
{
	uint16_t addr;
	sla_axis_config_t cfg;
	sla_axis_config_t* pcfg;
	sla_get_axis_config(axis, &cfg);
	if (axis == 0)
		addr = sla_cfg_addr((index < 0)?SLA_CFG_TWCFGA:SLA_CFG_TWCFGN);
	else if (axis == 1)
		addr = sla_cfg_addr((index < 0)?SLA_CFG_TICFGA:SLA_CFG_TICFGN);
	if (index >= 0)
		eeprom_update_bytes(addr + 16 * index, (uint8_t*)(&cfg), 16);
	else
	{
		for (index = 0; index < 16; index++)
		{
			if (axis == 0) pcfg = (sla_axis_config_t*)(sla_def_cfg_tower + index);
			else if (axis == 1) pcfg = (sla_axis_config_t*)(sla_def_cfg_tilt + index);
			progmem_read_bytes((uint8_t*)pcfg, (uint8_t*)(&cfg), 16);
			eeprom_update_bytes(addr + 16 * index, (uint8_t*)(&cfg), 16);
		}
	}
}

void sla_cfg_load_XXcf(uint8_t axis, int8_t index)
{
	uint16_t addr;
	sla_axis_config_t cfg;
	if (index >= 0)
	{
		if (axis == 0)
			addr = sla_cfg_addr(SLA_CFG_TWCFGN);
		else if (axis == 1)
			addr = sla_cfg_addr(SLA_CFG_TICFGN);
		eeprom_read_bytes(addr + 16 * index, (uint8_t*)(&cfg), 16);
		sla_set_axis_config(axis, &cfg);
	}
}

void sla_cfg_save(void)
{
	if ((sla_cfg_dirty & (1 << SLA_CFG_TWCFGN)) && (sla_twcs_index >= 0))
		sla_cfg_save_XXcf(0, sla_twcs_index);
	if ((sla_cfg_dirty & (1 << SLA_CFG_TICFGN)) && (sla_tics_index >= 0))
		sla_cfg_save_XXcf(1, sla_tics_index);
	if (sla_cfg_dirty & (1 << SLA_CFG_TWCFGA))
		sla_cfg_save_XXcf(0, -1);
	if (sla_cfg_dirty & (1 << SLA_CFG_TICFGA))
		sla_cfg_save_XXcf(1, -1);
	if (sla_cfg_dirty & (1 << SLA_CFG_TWHPHA))
	{
		eeprom_update_word((uint16_t*)sla_cfg_addr(SLA_CFG_TWHPHA), sla_twho_phase);
	}
	if (sla_cfg_dirty & (1 << SLA_CFG_TIHPHA))
	{
		eeprom_update_word((uint16_t*)sla_cfg_addr(SLA_CFG_TIHPHA), sla_tiho_phase);
	}
	if (sla_cfg_dirty & (1 << SLA_CFG_TWHOFS))
	{
		eeprom_update_word((uint16_t*)sla_cfg_addr(SLA_CFG_TWHOFS), sla_twho_offset);
	}
	if (sla_cfg_dirty & (1 << SLA_CFG_TIHOFS))
	{
		eeprom_update_word((uint16_t*)sla_cfg_addr(SLA_CFG_TIHOFS), sla_tiho_offset);
	}
	if (sla_cfg_dirty & (1 << SLA_CFG_TIHOSC))
	{
		eeprom_update_byte((uint8_t*)sla_cfg_addr(SLA_CFG_TIHOSC) + 0, sla_tiho_comp_sgn);
		eeprom_update_byte((uint8_t*)sla_cfg_addr(SLA_CFG_TIHOSC) + 1, sla_tiho_comp_sgp);
	}
	sla_cfg_dirty = 0;
}

void sla_cfg_default(void)
{
	sla_twho_phase = -1;
	sla_tiho_phase = -1;
	sla_cfg_dirty = 0xffffffff;
	sla_cfg_save();
}

void sla_print_eeprom(FILE* out)
{
	uint8_t rows = 64; //64 rows = 1024 bytes
	uint8_t r;
	uint8_t c;
	uint16_t addr = 0;
	fprintf_P(out, PSTR("EEPROM (0x%03x..0x%03x):\n"), 0, 16 * rows - 1);
	for (r = 0; r < rows; r++)
	{
		fprintf_P(out, PSTR("%04x"), addr);
		for (c = 0; c < 16; c++)
			fprintf_P(out, PSTR(" %02x"), eeprom_read_byte((uint8_t*)(addr++)));
		fprintf_P(out, PSTR("\n"));
	}
}
