#ifndef _SLA_SPI_H
#define _SLA_SPI_H

#include <inttypes.h>
#include "config.h"

//async spi requests enumeration
#define SLA_SPI_REQ_NOTHING                0x0000 //nothing to do
#define SLA_SPI_REQ_GPIO_IN_A              0x0001 //mcp23s17_in_a
#define SLA_SPI_REQ_GPIO_OUT_A             0x0002 //mcp23s17_out_a
#define SLA_SPI_REQ_GPIO_IN_B              0x0004 //mcp23s17_in_b
#define SLA_SPI_REQ_GPIO_OUT_B             0x0008 //mcp23s17_out_b
#define SLA_SPI_REQ_TMC0_SET_CUR           0x0010 //tmc2130_set_cur(0, 
#define SLA_SPI_REQ_TMC0_SET_SGT           0x0020 //tmc2130_set_sgt(0, 
#define SLA_SPI_REQ_TMC0_SET_CST           0x0040 //tmc2130_set_cst(0, 
#define SLA_SPI_REQ_TMC0_RD_DRV_STATUS     0x0080 //tmc2130_read_drv_status(0, 
#define SLA_SPI_REQ_TMC1_SET_CUR           0x0100 //tmc2130_set_cur(1, 
#define SLA_SPI_REQ_TMC1_SET_SGT           0x0200 //tmc2130_set_sgt(1, 
#define SLA_SPI_REQ_TMC1_SET_CST           0x0400 //tmc2130_set_cst(1, 
#define SLA_SPI_REQ_TMC1_RD_DRV_STATUS     0x0800 //tmc2130_read_drv_status(1, 
#define SLA_SPI_REQ_TMC0_RD_MSCNT          0x1000 //tmc2130_read_mscnt(0, 
#define SLA_SPI_REQ_TMC1_RD_MSCNT          0x2000 //tmc2130_read_mscnt(1, 

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern volatile uint16_t sla_spi_req_mask;
extern uint8_t  sla_spi_tmc_cur[2];
extern int8_t   sla_spi_tmc_sgt[2];
extern uint16_t sla_spi_tmc_cst[2];
extern uint32_t sla_spi_tmc_drv[2];
extern uint16_t sla_spi_tmc_msc[2];

extern void sla_spi_async(uint16_t spi_req_mask);
extern void sla_spi_sync(uint16_t spi_req_mask);
extern void sla_spi_cycle(void);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_SPI_H
