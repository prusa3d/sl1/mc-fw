#ifndef _SND_H
#define _SND_H
#include <inttypes.h>
#include <avr/io.h>
#include "config.h"
#include "io_atmega32u4.h"

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern void snd_init(void);
extern float snd_get_freq(void);
extern void snd_set_freq(float freq);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

//used TIMER1
#if (SND_TIMER == 1)
//output pin is hardware controled (OCR1A/B/C)
#define SND_OCR_OUT ((SND_PIN == PIN_OCR1A) || (SND_PIN == PIN_OCR1B) || (SND_PIN == PIN_OCR1C))
//inlined 'on' function
#if (SND_PIN == PIN_OCR1A)
 __inline void snd_on(void) { TCCR1A = (1 << COM1A0); }
#elif (SND_PIN == PIN_OCR1B)
 __inline void snd_on(void) { TCCR1A = (1 << COM1B0); }
#elif (SND_PIN == PIN_OCR1C)
 __inline void snd_on(void) { TCCR1A = (1 << COM1C0); }
#else
 __inline void snd_on(void) { TIMSK1 |= (1 << OCIE1A); }
#endif
//inlined 'off' function
#if SND_OCR_OUT
 __inline void snd_off(void) { TCCR1A = 0x00; }
#else
 __inline void snd_off(void)
 { TIMSK1 &= ~(1 << OCIE1A); PIN_CLR(SND_PIN); }
#endif
//inlined 'is_on' function
#if SND_OCR_OUT
 __inline uint8_t snd_is_on(void) { return TCCR1A?1:0; }
#else
 __inline uint8_t snd_is_on(void) { return (TIMSK1 & (1 << OCIE1A))?1:0; }
#endif
#endif //(SND_TIMER == 1)

//used TIMER3
#if (SND_TIMER == 3)
//output pin is hardware controled (OCR3A/B/C)
#define SND_OCR_OUT ((SND_PIN == PIN_OCR3A) || (SND_PIN == PIN_OCR3B) || (SND_PIN == PIN_OCR3C))
//inlined 'on' function
#if (SND_PIN == PIN_OCR3A)
 __inline void snd_on(void) { TCCR3A = (1 << COM3A0); }
#elif (SND_PIN == PIN_OCR3B)
 __inline void snd_on(void) { TCCR3A = (1 << COM3B0); }
#elif (SND_PIN == PIN_OCR3C)
 __inline void snd_on(void) { TCCR3A = (1 << COM3C0); }
#else
 __inline void snd_on(void) { TIMSK3 |= (1 << OCIE3A); }
#endif
//inlined 'off' function
#if SND_OCR_OUT
 __inline void snd_off(void) { TCCR3A = 0x00; }
#else
 __inline void snd_off(void)
 { TIMSK3 &= ~(1 << OCIE3A); PIN_CLR(SND_PIN); }
#endif
//inlined 'is_on' function
#if SND_OCR_OUT
 __inline uint8_t snd_is_on(void) { return TCCR3A?1:0; }
#else
 __inline uint8_t snd_is_on(void) { return (TIMSK3 & (1 << OCIE3A))?1:0; }
#endif
#endif //(SND_TIMER == 1)

#endif //_SND_H
