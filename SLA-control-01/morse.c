/*
 * morse error codes interpreter
 */
#include "morse.h"
#include <inttypes.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

uint8_t morse_dat[4];  //buffer
uint8_t morse_cnt = 0; //total number of pairs (bytes * 4)
uint8_t morse_pos = 0; //position in buffer first two bits is index of bit pair (from msb)
uint8_t morse_dl1 = 0; //delay on
uint8_t morse_dl0 = 0; //delay off
uint8_t morse_out = 0; //output state

const uint8_t morse_numbers[10] PROGMEM = {
MORSE_0 >> 8,
MORSE_1 >> 8,
MORSE_2 >> 8,
MORSE_3 >> 8,
MORSE_4 >> 8,
MORSE_5 >> 8,
MORSE_6 >> 8,
MORSE_7 >> 8,
MORSE_8 >> 8,
MORSE_9 >> 8,
};

void morse_num(uint8_t num)
{
	uint16_t morse = (uint16_t)pgm_read_byte(morse_numbers + num) << 8;
	if (num >= 5) morse |= 0x40;
	else morse |= 0x80;
	cli();
	morse_dat[0] = morse >> 8;
	morse_dat[1] = morse & 0xff;
	morse_pos = 0;
	morse_dl1 = 0;
	morse_dl0 = 0;
	morse_cnt = 8;
	sei();
}

void morse_next_pair(void)
{
	uint8_t bn = morse_pos >> 2; //byte number
	uint8_t pi = morse_pos & 3; //pair index
	uint8_t pair = (morse_dat[bn] >> (2 * (3 - pi))) & 0x03;
	switch (pair)
	{
	case MORSE_SPC: // space
		morse_dl1 = MORSE_DL1_SPC;
		morse_dl0 = MORSE_DL0_SPC;
		break;
	case MORSE_DOT: // dot
		morse_dl1 = MORSE_DL1_DOT;
		morse_dl0 = MORSE_DL0_DOT;
		break;
	case MORSE_DSH: // dash
		morse_dl1 = MORSE_DL1_DSH;
		morse_dl0 = MORSE_DL0_DSH;
		break;
	case MORSE_END: // end
		morse_pos = ((morse_pos + 4) && 0xfc) - 1;
		break;
	}
	morse_pos++;
	if (morse_pos >= morse_cnt)
		morse_pos = 0;
}

void morse_cycle(void)
{
	if (morse_cnt == 0) return;
	if (morse_dl1)
	{
		morse_dl1--;
		if (!morse_out)
		{
			MORSE_OUT_H();
			morse_out = 1;
		}
	}
	else
	{
		if (morse_out)
		{
			MORSE_OUT_L();
			morse_out = 0;
		}
		if (morse_dl0)
			morse_dl0--;
		else
			morse_next_pair();
	}
}
