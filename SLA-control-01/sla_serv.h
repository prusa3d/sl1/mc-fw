#ifndef _SLA_SERV_H
#define _SLA_SERV_H

#include <inttypes.h>
#include "config.h"

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern uint8_t sla_get_POR(uint8_t port);
extern void sla_set_POR(uint8_t port, uint8_t val);
extern uint8_t sla_get_PIN(uint8_t port);
extern void sla_set_PIN(uint8_t port, uint8_t val);
extern uint8_t sla_get_DDR(uint8_t port);
extern void sla_set_DDR(uint8_t port, uint8_t val);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_SERV_H
