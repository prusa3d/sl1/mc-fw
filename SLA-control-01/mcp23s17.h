#ifndef _MCP23S17_H
#define _MCP23S17_H

#include <inttypes.h>
#include "config.h"

#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)

extern uint8_t mcp23s17_init(uint8_t dev_addr, uint16_t iodir, uint16_t gpio);
extern void mcp23s17_dir_a(uint8_t dev_addr, uint8_t iodira);
extern void mcp23s17_dir_b(uint8_t dev_addr, uint8_t iodira);
extern void mcp23s17_out_a(uint8_t dev_addr, uint8_t gpioa);
extern void mcp23s17_out_b(uint8_t dev_addr, uint8_t gpioa);
extern uint8_t mcp23s17_in_a(uint8_t dev_addr);
extern uint8_t mcp23s17_in_b(uint8_t dev_addr);

#if defined(__cplusplus)
}
#endif //defined(__cplusplus)
#endif //_MCP23S17_H
