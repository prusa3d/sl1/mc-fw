/*
 * timer0
 */
#include "timer0.h"
#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint32_t timer0_ms; // 32bit milisecond counter (overflow in 49.71 days)
/*
 * timer0 is used for:
 * TIMER0_OVF_vect  - generating 1ms clock base
 * TIMER0_COMPA_vect - periodic sla routine (tmc, pwm, rpm, ...)
 * OCR0B generating pwm for UV led
 */
void timer0_init(void)
{
	timer0_ms = 0; // initialize milisecond counter to zero
	//fast pwm mode, source = fclk/64
	TCCR0A = (3 << WGM00); //COM_A-B=00, WGM_0-1=11
	TCCR0B = TIMER0_PRESCALER; //WGM_2=0, CS_0-2=011
	OCR0A = 125; //set OCR0A register
	TCNT0 = (256 - TIMER0_CYC_1MS); // initialize counter - overflow in 1ms
	TIMSK0 = (1 << TOIE0) | (1 << OCIE0A); // enable overflow interrupt and OCR0A interrupt
}

uint32_t timer0_us(void)
{
	uint8_t _sreg = SREG;
	uint8_t tcnt;
	uint32_t ms;
	cli();
	tcnt = TCNT0;
	ms = timer0_ms;
	SREG = _sreg;
	return (1000 * ms) + (4 * (tcnt - (256 - TIMER0_CYC_1MS)));
}

ISR(TIMER0_OVF_vect)
{
	TCNT0 += (256 - TIMER0_CYC_1MS);
	sei();
	timer0_ms++;
}
