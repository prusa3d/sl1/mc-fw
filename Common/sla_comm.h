// sla_comm.h
#ifndef _SLA_COMM_H
#define _SLA_COMM_H
#include <stdio.h>
#include <netinet/in.h> 


#if defined(__cplusplus)
extern "C" {
#endif //defined(__cplusplus)


extern int addr_in_to_str(struct sockaddr_in* paddr, char* str_ip);

extern int str_to_addr_in(char* str_ip, struct sockaddr_in* paddr);

extern FILE* fopen_tcp_server(char* str_ip);

extern FILE* fopen_tcp_client(char* str_ip);

extern FILE* fopen_tty(char* str_dev);


#if defined(__cplusplus)
}
#endif //defined(__cplusplus)

#endif //_SLA_COMM_H
