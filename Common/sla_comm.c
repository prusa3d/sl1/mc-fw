

#include "sla_comm.h"
//#include <sys/types.h>
//#include <netinet/in.h> 
//#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/fcntl.h>
#include <errno.h>
#include <termios.h>


int addr_in_to_str(struct sockaddr_in* paddr, char* str_ip)
{
	unsigned char* b = (unsigned char*)&paddr->sin_addr.s_addr;
	unsigned short port = ntohs(paddr->sin_port);
	if (paddr->sin_family != AF_INET) return -1;
	return sprintf(str_ip, "%hhu.%hhu.%hhu.%hhu:%hu", b[0], b[1], b[2], b[3], port);
}

int str_to_addr_in(char* str_ip, struct sockaddr_in* paddr)
{
	int ret = 0;
	unsigned char* b = (unsigned char*)&(paddr->sin_addr.s_addr);
	unsigned short port = 0;
	paddr->sin_family = AF_INET;
	paddr->sin_addr.s_addr = INADDR_ANY;
	ret = sscanf(str_ip, "%hhu.%hhu.%hhu.%hhu:%hu", b+0, b+1, b+2, b+3, &port);
	paddr->sin_port = htons(port);
	return ret;
}

FILE* fopen_tcp_server(char* str_ip)
{
	int size = 0;
	int server_fd = 0;
	int client_fd = 0;
	struct sockaddr_in server;
	struct sockaddr_in client;
	char client_ip[22];
	FILE* file;

	printf("fopen_tcp_server %s\n", str_ip);

	server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (server_fd < 0)
	{
		perror("fopen_tcp_server - error - could not create socket");
		return 0;
	}
	puts("fopen_tcp_server - socket created");

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(8888);

	addr_in_to_str((struct sockaddr_in*)&server, client_ip);
	printf("fopen_tcp_server - INADDR_ANY = %s\n", client_ip);

	str_to_addr_in(str_ip, (struct sockaddr_in*)&server);

	if (bind(server_fd,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("fopen_tcp_server - error - bind failed");
		return 0;
	}
	puts("fopen_tcp_server - bind OK");

	listen(server_fd, 3);

	puts("fopen_tcp_server - waiting for incoming connections...");

	size = sizeof(struct sockaddr_in);
	client_fd = accept(server_fd, (struct sockaddr *)&client, (socklen_t*)&size);
	if (client_fd < 0)
	{
		perror("fopen_tcp_server - error - accept failed.");
		return 0;
	}
	addr_in_to_str((struct sockaddr_in*)&client, client_ip);
	printf("fopen_tcp_server - accepting connection from %s\n", client_ip);
	file = fdopen(client_fd, "r+");
	if (file == 0)
	{
		perror("fopen_tcp_server - fdopen failed");
		return 0;
	}
	if (fcntl(client_fd, F_SETFL, fcntl(client_fd, F_GETFL, 0) | O_NONBLOCK))
	{
		perror("fopen_tcp_server - fcntl failed");
		return 0;
	}
	close(server_fd);
	return file;
}

FILE* fopen_tcp_client(char* str_ip)
{
	int size;
	int server_fd = 0;
	int client_fd = 0;
	struct sockaddr_in server;
	struct sockaddr_in client;
	char server_ip[22];
	FILE* file;

	printf("fopen_tcp_client %s\n", str_ip);

	server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (server_fd < 0)
	{
		perror("fopen_tcp_client - error - could not create socket");
		return 0;
	}
	puts("fopen_tcp_client - socket created");

	str_to_addr_in(str_ip, (struct sockaddr_in*)&server);

	if (connect(server_fd,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("fopen_tcp_client - error - connect failed");
		return 0;
	}
	printf("fopen_tcp_client - connected to %s\n", str_ip);
	file = fdopen(server_fd, "r+");
	if (file == 0)
	{
		perror("fopen_tcp_client - fdopen failed");
		return 0;
	}
	if (fcntl(server_fd, F_SETFL, fcntl(server_fd, F_GETFL, 0) | O_NONBLOCK))
	{
		perror("fopen_tcp_client - fcntl failed");
		return 0;
	}
	return file;
}

speed_t encode_baud(int baud)
{
	switch (baud)
	{
	case 300: return B300;
	case 600: return B600;
	case 1200: return B1200;
	case 2400: return B2400;
	case 4800: return B4800;
	case 9600: return B9600;
	case 19200: return B19200;
	case 38400: return B38400;
	case 57600: return B57600;
	case 115200: return B115200;
	}
	return 0;
}

int decode_baud(speed_t cbaud)
{
	switch (cbaud)
	{
	case B300: return 300;
	case B600: return 600;
	case B1200: return 1200;
	case B2400: return 2400;
	case B4800: return 4800;
	case B9600: return 9600;
	case B19200: return 19200;
	case B38400: return 38400;
	case B57600: return 57600;
	case B115200: return 115200;
	}
	return 0;
}

tcflag_t encode_size(int size)
{
	switch (size)
	{
	case 5: return CS5;
	case 6: return CS6;
	case 7: return CS7;
	case 8: return CS8;
	}
	return 0;
}

int decode_size(tcflag_t csize)
{
	switch (csize)
	{
	case CS5: return 5;
	case CS6: return 6;
	case CS7: return 7;
	case CS8: return 8;
	}
	return 0;
}

int setup_ts(struct termios* pts, int baud, int size, int parity, int stop, int timeout)
{
	speed_t cbaud = cfgetospeed(pts);
	if (baud >= 0)
	{
		//encode baud
		cbaud = encode_baud(baud);
		//check baud
		if (cbaud == 0) return errno = EINVAL?-1:0;
	}
	tcflag_t csize = pts->c_cflag & CSIZE;
	if (size >= 0)
	{
		//encode size
		csize = encode_size(size);
		//check size
		if (csize == 0) return errno = EINVAL?-1:0;
	}
	if (parity >= 0)
	{
		//check parity
		if ((parity < 0) || (parity > 2)) return errno = EINVAL?-1:0;
	}
	else
		parity = (pts->c_cflag & PARENB)?((pts->c_cflag & PARODD)?1:2):0;
	if (stop >= 0)
	{
		//check stop
		if ((stop < 1) || (stop > 2)) return errno = EINVAL?-1:0;
	}
	else
		stop = (pts->c_cflag & CSTOPB)?2:1;
	if (timeout >= 0)
	{
		//check timeout
		if (timeout < 0) return errno = EINVAL?-1:0;
	}
	//cleanup
	bzero(pts, sizeof(struct termios));
	//default settings
//	cfmakeraw(pts);
	//reset input modes
	pts->c_iflag = 0;
	//reset output modes
	pts->c_oflag = 0;
	//set input baudrate
	cfsetispeed(pts, cbaud);
	//set output baudrate
	cfsetospeed(pts, cbaud);
	//local mode
	pts->c_cflag |= CLOCAL;
	//enable the receiver
	pts->c_cflag |= CREAD;
	//disable hardware flow control
	pts->c_cflag &= ~CRTSCTS;
	//set parity
	pts->c_cflag &= ~(PARENB | PARODD);
	pts->c_cflag |= parity?PARENB:0;
	pts->c_cflag |= ((parity % 2) != 0)?PARODD:0;
	//set stopbit
	pts->c_cflag &= ~CSTOPB;
	pts->c_cflag |= (stop == 2)?CSTOPB:0;
	//mask datasize
	pts->c_cflag &= ~CSIZE;
	//datasize 5,6,7,8 bit
	pts->c_cflag |= csize;
	//data processed as raw input (noncanonical)
	pts->c_lflag &= ~ICANON;
	//no echo
	pts->c_lflag &= ~ECHO;
	//no signal
	pts->c_lflag &= ~ISIG;
	//minimum number of characters
	pts->c_cc[VMIN] = timeout?0:1;
	//timeout in 1/10s
	pts->c_cc[VTIME] = timeout?(timeout / 100):0;
	return 0;
}

FILE* fopen_tty(char* str_dev)
{
	FILE* file = fopen(str_dev, "r+");

	if (file == 0)
	{
		perror("fopen_tty - fopen failed");
		return 0;
	}
	puts("fopen_tty - fopen OK");
	struct termios ts;
	if (tcgetattr(fileno(file), &ts) != 0) goto e0;
	if (setup_ts(&ts, 115200, 8, 0, 1, 100) != 0) goto e0;
	if (tcsetattr(fileno(file), TCSANOW, &ts) != 0) goto e0;
	usleep(100000); //wait 100ms - if omited, some usb converters randomly fail
	return file;
e0:
	fclose(file);
	return 0;
}
