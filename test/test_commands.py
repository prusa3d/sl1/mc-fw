import logging
import sys
import unittest
import threading
from queue import Queue, Empty
from subprocess import Popen, PIPE
from time import sleep

class TestCommands(unittest.TestCase):
    def setUp(self) -> None:
        self.stream_handler = logging.StreamHandler(sys.stdout)
        self.stream_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(name)s - %(message)s"))
        self.logger = logging.getLogger()
        self.logger.addHandler(self.stream_handler)
        self.logger.setLevel(logging.DEBUG)
        self.logger = logging.getLogger(__name__)
        self.process = Popen(["./SLA-control_rev06.elf"], stdin=PIPE, stdout=PIPE, encoding="ascii")
        self.read_queue = Queue()
        self.thread = threading.Thread(target=self._thread)
        self.thread.start()

        self.assertEqual(self._read(), "MCUSR=0x00")
        self.assertEqual(self._read(), "ready")

    def tearDown(self) -> None:
        self.process.kill()
        self.process.wait(timeout=3)
        self.thread.join()

    def _thread(self) -> None:
        while self.process.poll() is None:
            line = self.process.stdout.readline().strip()
            if not line.startswith("#"):
                self.read_queue.put(line)
        self.process.stdin.close()
        self.process.stdout.close()

    def _write(self, command: str) -> None:
        self.process.stdin.write(command)
        self.process.stdin.flush()

    def _read(self) -> str:
        try:
            data = self.read_queue.get(timeout=3)
            self.logger.debug("< %s", data)
            return data
        except Empty:
            return ""

    def _command(self, command) -> str:
        self._write(f"{command}\r\n")
        self.logger.debug("> %s", command)
        return self._read()

    def test_test(self) -> None:
        pass

    def test_ver(self) -> None:
        self.assertRegex(self._command("?ver"), ".* ok")

    def test_rev(self) -> None:
        self.assertEqual(self._command("?rev"), "6 70 ok")

    def test_ser(self) -> None:
        self.assertEqual(self._command("?ser"), "CZPX0619X678XC12345 ok")

    # TODO
    # def test_reset(self) -> None:
    #     pass

    def test_ena(self):
        self.assertEqual(self._command("?ena"), "0 ok")
        self.assertEqual(self._command("!ena 1"), "ok")
        self.assertEqual(self._command("?ena"), "1 ok")
        self.assertEqual(self._command("!ena 0"), "ok")
        self.assertEqual(self._command("?ena"), "0 ok")

    def test_end(self):
        self.assertEqual(self._command("?end"), "3 ok")
        self.assertEqual(self._command("!end 0"), "ok")
        self.assertEqual(self._command("?end"), "0 ok")
        self.assertEqual(self._command("!end 2"), "ok")
        self.assertEqual(self._command("?end"), "2 ok")

    def test_led(self):
        self.assertEqual(self._command("?led"), "1 ok")
        self.assertEqual(self._command("!led 0"), "ok")
        self.assertEqual(self._command("?led"), "0 ok")
        self.assertEqual(self._command("!led 1"), "ok")
        self.assertEqual(self._command("?led"), "1 ok")

    def test_status(self):
        self.assertEqual(self._command("?"), "0 ok")

    def test_beep(self):
        self.assertEqual(self._command("!beep 1000 100"), "ok")
        self.assertEqual(self._command("!beep 1 100"), "e4")

    def test_pled(self):
        self.assertEqual(self._command("?pled"), "2 ok")
        self.assertEqual(self._command("!pled 0"), "ok")
        self.assertEqual(self._command("?pled"), "0 ok")
        self.assertEqual(self._command("!pled 1"), "ok")
        self.assertEqual(self._command("?pled"), "1 ok")

    # TODO
    # def test_shdn(self) -> None:
    #     pass

    def test_uled(self):
        self.assertEqual(self._command("?uled"), "0 ok")
        self.assertEqual(self._command("!uled 1"), "ok")
        self.assertEqual(self._command("?uled"), "1 ok")
        self.assertEqual(self._command("!uled 0"), "ok")
        self.assertEqual(self._command("?uled"), "0 ok")
        self.assertEqual(self._command("!uled 1 1000"), "ok")
        self.assertRegex(self._command("?uled"), '1 ([1-9]|[1-9][0-9]|[1-9][0-9][0-9]) ok')
        sleep(1)
        self.assertEqual(self._command("?uled"), "0 ok")
        # TODO: Test exposure

    def test_fans(self):
        self.assertEqual(self._command("?fans"), "0 ok")
        self.assertEqual(self._command("!fans 3"), "ok")
        self.assertEqual(self._command("?fans"), "3 ok")

    def test_temps(self):
        self.assertEqual(self._command("?temp"), "400 200 200 200 ok")

    def test_volt(self):
        self.assertEqual(self._command("?volt"), "0 0 0 24000 ok")
        self._command("!uled 1")
        self.assertEqual(self._command("?volt"), "0 0 0 24000 ok")

    def test_motr(self):
        self.assertEqual(self._command("!motr"), "ok")

    def is_moving(self):
        return not self._command("?mot") == "0 ok"

    def check_tower_pos(self, microsteps: int) -> None:
        while self.is_moving():
            sleep(0.1)
        pos = self._command("?twpo")
        self.assertEqual(f"{microsteps} ok", pos)

    def test_twup(self):
        microsteps = 1000
        initial = 10000
        self.assertEqual(self._command(f"!twma {initial}"), "ok")
        self.wait_for("?mot", "0 ok")
        self.assertEqual(self._command(f"!twup {microsteps}"), "ok")
        self.check_tower_pos(initial + microsteps)

    def test_twdn(self):
        microsteps = 1000
        initial = 10000
        self.assertEqual(self._command(f"!twma {initial}"), "ok")
        self.wait_for("?mot", "0 ok")
        self.assertEqual(self._command(f"!twdn {microsteps}"), "ok")
        self.check_tower_pos(initial - microsteps)

    def test_twcu(self):  # deprecated API function
        self.assertEqual(self._command("!twcu 32"), "ok")
        self.assertEqual(self._command("?twcu"), "32 ok")

    def test_ticu(self):  # deprecated API function
        self.assertEqual(self._command("!ticu 32"), "ok")
        self.assertEqual(self._command("?ticu"), "32 ok")

    # TODO: This fails, not implemented, deprecated
    # def test_twsq(self):
    #     self.assertEqual(self._command("!twsq 0"), "ok")
    #     self.assertEqual(self._command("?twsg"), "64 ok")

    # TODO: This fails, not implemented, deprecated
    # def test_tisq(self):
    #     self.assertEqual(self._command("!tisq 64"), "ok")
    #     self.assertEqual(self._command("?tisg"), "64 ok")

    def wait_for(self, command, response_ok:str, wait_sec:int=30):
        for i in range(wait_sec * 10):
            if self._command(command) == response_ok:
                return
            else:
                sleep(0.1)
        self.assertFalse(True, "Timeout")

    def test_twho(self):
        self.assertEqual(self._command("?twho"), "-1 ok")
        self.assertEqual(self._command("!twho"), "ok")
        self.wait_for("?twho", "0 ok")

    def test_tiho_success(self):
        self.assertTrue(self._command("?end"), "3 ok")  # endstops are active
        self.assertEqual(self._command("?tiho"), "-1 ok")
        self.assertEqual(self._command("!tiho"), "ok")
        self.wait_for("?tiho", "0 ok")     # homing finished OK

    def test_tiho_endstop_not_reached(self):
        self.assertTrue(self._command("!end 0"), "ok")  # disable endstops
        self.assertEqual(self._command("?tiho"), "-1 ok")
        self.assertEqual(self._command("!tiho"), "ok")
        self.wait_for("?tiho", "-2 ok")     # endstop not reached

    def test_tiho_stuck_at_endstop(self):
        self.assertEqual(self._command("?tiho"), "-1 ok")
        self.assertEqual(self._command("!tiho"), "ok")
        self.wait_for("?tiho", "5 ok")     # tilt is going up
        self.assertTrue(self._command("!tipo -1000"), "ok")  # hack the position so the endstop is still active
        self.wait_for("?tiho", "-3 ok")     # axis is stuck at endstop

    def test_twma(self):
        position = 10000
        self.assertEqual(self._command(f"!twma {position}"), "ok")
        self.wait_for("?mot", "0 ok")
        self.assertEqual(self._command("?twpo"), f"{position} ok")

    def test_tima(self):
        position = 10000
        self.assertEqual(self._command(f"!tima {position}"), "ok")
        self.wait_for("?mot", "0 ok")
        self.assertEqual(self._command("?tipo"), f"{position} ok")

    def test_twpo(self):
        position = 10000
        self.assertEqual(self._command(f"!twpo {position}"), "ok")
        self.assertEqual(self._command("?twpo"), f"{position} ok")

    def test_tipo(self):
        position = 10000
        self.assertEqual(self._command(f"!tipo {position}"), "ok")
        self.assertEqual(self._command("?tipo"), f"{position} ok")

    def test_frpm(self):
        rpmMin = 800
        uvRpmMax = 2800
        blowerRpmMax = 3300
        rearRpmMax = 5000
        self.assertEqual(self._command(f"!frpm {rpmMin - 1} 2000 2000"), "e4")
        self.assertEqual(self._command(f"!frpm 2000 {rpmMin - 1} 2000"), "e4")
        self.assertEqual(self._command(f"!frpm 2000 2000 {rpmMin - 1}"), "e4")
        self.assertEqual(self._command(f"!frpm {uvRpmMax + 1} 2000 2000"), "e4")
        self.assertEqual(self._command(f"!frpm 2000 {blowerRpmMax + 1} 2000"), "e4")
        self.assertEqual(self._command(f"!frpm 2000 2000 {rearRpmMax + 1}"), "e4")
        self.assertEqual(self._command("!frpm 2000 2000 2000"), "ok")
        self.assertEqual(self._command("?frpm"), "0 0 0 ok")
        self._command("!fans 7")
        self.assertEqual(self._command("?frpm"), "2000 2000 2000 ok")
        self.assertEqual(self._command("!frpm 1500 1500 1500"), "ok")
        self.assertEqual(self._command("?frpm"), "1500 1500 1500 ok")

    def test_twcf(self):
        self.assertEqual(self._command("?twcf"), "3200 17600 250 250 24 6 1500 ok")
        self.assertEqual(self._command("!twcf 3100 17500 240 230 20 5 1600"), "ok")
        self.assertEqual(self._command("?twcf"), "3100 17500 240 230 20 5 1600 ok")

    def test_ticf(self):
        self.assertEqual(self._command("?ticf"), "960 1280 12 12 24 8 1500 ok")
        self.assertEqual(self._command("!ticf 3100 17500 240 230 20 5 1600"), "ok")
        self.assertEqual(self._command("?ticf"), "3100 17500 240 230 20 5 1600 ok")

    def test_twcs(self):
        self.assertEqual(self._command("?twcs"), "-1 ok")
        for i in range(8):
            self.assertEqual(self._command(f"!twcs {i}"), "ok")
            self.assertEqual(self._command("?twcs"), f"{i} ok")

    def test_tics(self):
        self.assertEqual(self._command("?tics"), "-1 ok")
        for i in range(8):
            self.assertEqual(self._command(f"!tics {i}"), "ok")
            self.assertEqual(self._command("?tics"), f"{i} ok")

    def test_eecl(self):
        self.assertEqual(self._command("!eecl"), "ok")

    def test_echo(self):
        self.assertEqual(self._command("!echo 1"), "ok")
        self._write("?echo\r\n")
        self.assertEqual(self._read(), "?echo")
        self.assertEqual(self._read(), "1 ok")

    def test_rsen(self):
        self.assertEqual(self._command("!rsen 1"), "ok")
        self.assertEqual(self._command("?rsen"), "1 ok")

    def test_rsme(self):
        position = 1000
        initial = 10000
        self.assertEqual(self._command(f"!twma {initial}"), "ok")
        self.wait_for("?mot", "0 ok")
        self.assertEqual(self._command(f"!rsme {position}"), "ok")
        self.wait_for("?mot", "0 ok")
        self.assertEqual(self._command("?twpo"), f"{initial - position} ok")

    def test_fane(self):
        self.assertEqual(self._command("?fane"), "0 ok")

    def test_ppwm(self):
        pwm = 10
        self.assertEqual(self._command(f"!ppwm {pwm}"), "ok")
        self.assertEqual(self._command("?ppwm"), f"{pwm} ok")

    def test_upwm(self):
        pwm = 128
        self.assertEqual(self._command(f"!upwm {pwm}"), "ok")
        self.assertEqual(self._command("?upwm"), f"{pwm} ok")

    def test_pspd(self):
        speed = 32
        self.assertEqual(self._command(f"!pspd {speed}"), "ok")
        self.assertEqual(self._command("?pspd"), f"{speed} ok")

    def test_sgbc(self):
        self.assertEqual(self._command("?sgbc"), "0 ok")

    def test_sgbd(self):
        # TODO: Why this does not return anything
        self.assertEqual(self._command("?sgbd"), "ok")

    # TODO: widl not tested

    # TODO: wdel not tested

    # TODO: ppst not tested

    def test_twhc(self):
        phase = 32
        self.assertEqual(self._command(f"!twhc {phase}"), "ok")
        self.assertEqual(self._command("?twhc"), f"{phase} ok")

    def test_tihc(self):
        phase = 32
        self.assertEqual(self._command(f"!tihc {phase}"), "ok")
        self.assertEqual(self._command("?tihc"), f"{phase} ok")

    def test_twst(self):
        self.assertEqual(self._command(f"!twst 0"), "ok")

    def test_tist(self):
        self.assertEqual(self._command(f"!twst 0"), "ok")

    def test_twph(self):
        self.assertEqual(self._command(f"!twph 512"), "ok")

    def test_tiph(self):
        self.assertEqual(self._command(f"!tiph 512"), "ok")

    def test_twgf(self):
        self.assertEqual(self._command(f"!twgf 0"), "ok")
        self.assertEqual(self._command(f"!twgf 1"), "ok")

    def test_tigf(self):
        self.assertEqual(self._command(f"!tigf 0"), "ok")
        self.assertEqual(self._command(f"!tigf 1"), "ok")

    def test_fmsk(self):
        mask = 3
        self.assertEqual(self._command(f"!fmsk {mask}"), "ok")
        self.assertEqual(self._command("?fmsk"), f"{mask} ok")

    # TODO: !gose not tested

    # TODO: _por not tested

    # TODO: _ddr not tested

    # TODO: _pin not tested

    def test_rst(self):
        self.assertEqual(self._command("?rst"), "0 ok")

if __name__ == '__main__':
    unittest.main()
