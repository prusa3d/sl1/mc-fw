FROM alpine:edge

RUN apk add --no-cache build-base binutils-avr gcc-avr avr-libc vim gawk bash python3
