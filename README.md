# SLA-control-01
SLA printer controler firmware for board rev 0.2b

## Build with Arduino IDE (recomended version 1.8.5)
1. start Arduino IDE
2. open file SLA-control-01.ino
3. select board "Leonardo"
4. compile

## Build with AVR-GCC (recomended version 4.8.2)
1. start terminal
2. cd to source folder
3. type "make build"
